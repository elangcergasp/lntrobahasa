#TSV=`find . '(' -name '*[*.tsv' ')' | xargs -L1`

files=`find . '(' -name '*[*.tsv' ')'`
IFS=$'\n' read -d '' -r -a TSV <<< "${files}"

tmpNewlineFile="tmpNewlineFile.txt"
echo -e "\n" > "${tmpNewlineFile}"

for i in "${TSV[@]}"
do
    echo $i
    tmp=()
    for j in "${TSV[@]}"
    do
        if [ "$i" != "$j" ]
        then
            echo "    "$j
            tmp+=("$j")
            tmp+=("$tmpNewlineFile")
        fi
    done
    unset 'tmp[${#tmp[@]}-1]'
    cat "${tmp[@]}" > "${i}_NOT"
done

rm "$tmpNewlineFile"
