package com.elangcergasp.lntrobahasa.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomSearchResultRepositoryImplTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomSearchResultRepositoryImplTests.class);

    @Autowired
    SearchResultRepository searchResultRepository;

    @Test
    public void testDeleteDuplicates() {
        searchResultRepository.deleteDuplicatesByUrlAndMeta()
                .collectList().block()
                .forEach(searchResultItem -> {
                    assertNotNull(searchResultItem);
                    LOGGER.debug("Tested have duplicates : " + searchResultItem);
                });
    }

}
