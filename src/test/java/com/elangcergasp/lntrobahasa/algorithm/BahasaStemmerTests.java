package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaActivePassiveForm;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaStemStructure;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaWordTokens;
import com.elangcergasp.lntrobahasa.services.cacher.KBBIMongoCacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BahasaStemmerTests {

    @Autowired
    KBBIMongoCacher KBBI;

    @Autowired
    BahasaStemmer stemmer;

    public List<Double> getOnlyNumbers(String text) {
        Matcher matcher = Pattern.compile("\\d+(?:[,\\.]\\d+)*").matcher(text);
        List<Double> doubleList = new ArrayList<>();
        while (matcher.find()) {
            doubleList.add(Double.valueOf(matcher.group()
                    .replaceAll("[,.](\\d{3})", "$1")
                    .replaceAll("[,.](\\d{1,2}|\\d{4,})", ".$1")));
        }
        return doubleList;
    }

    @Test
    public void a() {
        String text = "About 1 results<nobr> (0.30 seconds)&nbsp;</nobr>";
        System.out.println(getOnlyNumbers(text));
        assertTrue(1L == getOnlyNumbers(text).get(0).longValue());
    }

    @Test
    public void stemStructure() {
        BahasaStemStructure s = stemmer.stemStructure("memperlakukannyalah");
        assertEquals("mem", s.getPrefix_1());
        assertEquals("per", s.getPrefix_2());
        assertEquals("laku", s.getStem());
        assertEquals("kan", s.getSuffix_2());
        assertEquals("nya", s.getPossessive());
        assertEquals("lah", s.getParticle());
    }

    @Test
    public void splitSyllables() {
        BahasaWordTokens s = stemmer.splitSyllables("memperlakukannyalah");
        List<String> ss = s.getTokens();
        assertEquals("mem", ss.get(0));
        assertEquals("per", ss.get(1));
        assertEquals("la", ss.get(2));
        assertEquals("ku", ss.get(3));
        assertEquals("kan", ss.get(4));
        assertEquals("nya", ss.get(5));
        assertEquals("lah", ss.get(6));
    }

    @Test
    public void stemFormula() {
        BahasaStemStructure s = stemmer.stemFormula("mengadilinyalah");
        assertEquals("me", s.getPrefix_1());
        assertEquals("adil", s.getStem());
        assertEquals("i", s.getSuffix_2());
        assertEquals("nya", s.getPossessive());
        assertEquals("lah", s.getParticle());

        s = stemmer.stemFormula("menyesuaikan");
        assertEquals("me", s.getPrefix_1());
        assertEquals("sesuai", s.getStem());
        assertEquals("kan", s.getSuffix_2());
    }

    @Test
    public void getActivePassiveForm() {
        BahasaActivePassiveForm forms1 = stemmer.getActivePassiveForm("mengobati");
        assertEquals("mengobati", forms1.getActiveForm());
        assertEquals("diobati", forms1.getPassiveForm());

        BahasaActivePassiveForm forms2 = stemmer.getActivePassiveForm("berada");
        assertEquals("ada", forms2.getActiveForm());
        assertEquals("berada", forms2.getPassiveForm());

        BahasaActivePassiveForm forms3 = stemmer.getActivePassiveForm("dipukul");
        assertEquals("memukul", forms3.getActiveForm());
        assertEquals("dipukul", forms3.getPassiveForm());

        BahasaActivePassiveForm forms4 = stemmer.getActivePassiveForm("makan");
        assertEquals("memakan", forms4.getActiveForm());
        assertEquals("dimakan", forms4.getPassiveForm());

        BahasaActivePassiveForm forms5 = stemmer.getActivePassiveForm("telan");
        assertEquals("menelan", forms5.getActiveForm());
        assertEquals("ditelan", forms5.getPassiveForm());

        BahasaActivePassiveForm forms7 = stemmer.getActivePassiveForm("menamai");
        assertEquals("menamai", forms7.getActiveForm());
        assertEquals("dinamai", forms7.getPassiveForm());

        BahasaActivePassiveForm forms8 = stemmer.getActivePassiveForm("menghargai");
        assertEquals("menghargai", forms8.getActiveForm());
        assertEquals("dihargai", forms8.getPassiveForm());

        BahasaActivePassiveForm forms9 = stemmer.getActivePassiveForm("mencabut");
        assertEquals("mencabut", forms9.getActiveForm());
        assertEquals("dicabut", forms9.getPassiveForm());

        BahasaActivePassiveForm forms11 = stemmer.getActivePassiveForm("mengecat");
        assertEquals("mengecat", forms11.getActiveForm());
        assertEquals("dicat", forms11.getPassiveForm());

        BahasaActivePassiveForm forms10 = stemmer.getActivePassiveForm("mendaki");
        assertEquals("mendaki", forms10.getActiveForm());
        assertEquals("didaki", forms10.getPassiveForm());

        BahasaActivePassiveForm forms6 = stemmer.getActivePassiveForm("menjadi");
        assertEquals("menjadi", forms6.getActiveForm());
        assertEquals("dijadi", forms6.getPassiveForm());
    }

}
