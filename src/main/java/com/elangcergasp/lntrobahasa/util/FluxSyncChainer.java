/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.util;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Deque;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * @param <T>
 * @author elangcergasp
 */
public class FluxSyncChainer<T> {

    private final Deque<Flux<T>> syncDeque = new ConcurrentLinkedDeque<>();

    public synchronized Flux<T> enqueue(Flux<T> last) {
        Flux<T> wrapper;

        if (syncDeque.isEmpty()) {
            wrapper = Flux.create(emitter -> {
                last.doFinally(t -> {
                    emitter.complete();
                    dequeue();
                }).subscribe(t -> {
                    emitter.next(t);
                });
            });
        } else {
            Flux<T> prev = tail();
            wrapper = Flux.create(emitter -> {
                Mono<List<T>> prevList = prev.collectList();

                prevList.doOnError(signalType -> emitter.complete()).subscribe(l -> {
                    last.doFinally(t -> {
                        emitter.complete();
                        dequeue();
                    }).subscribe(t -> {
                        emitter.next(t);
                    });
                });
            });
        }

        Flux<T> result = wrapper.share();

        syncDeque.addLast(result);

        return result;
    }

    public synchronized Flux<T> head() {
        return syncDeque.peekFirst();
    }

    public synchronized Flux<T> tail() {
        return syncDeque.peekLast();
    }

    public synchronized Flux<T> dequeue() {
        return syncDeque.pollFirst();
    }

    public synchronized void clear() {
        syncDeque.clear();
    }

}
