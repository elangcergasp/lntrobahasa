/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.util;

import reactor.core.publisher.Flux;
import tech.tablesaw.api.Table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class MyArray {

    public static String STRING_MAP_SEPARATOR = "\t";
    public static String STRING_LIST_SEPARATOR = "\n";
    public static String STRING_ELEMENT_SEPARATOR = "\t";

    public static List<List<String>> tableToRows(Table data) {
        int rowSize = data.rowCount();
        int colSize = data.columnCount();
        List<List<String>> rows = new ArrayList<>();
        for (int i = 0; i < rowSize; i++) {
            List<String> row = new ArrayList<>();
            for (int j = 0; j < colSize; j++) {
                row.add(data.get(i, j));
            }
            rows.add(row);
        }
        return rows;
    }

    public static Flux<List<String>> tableToRowsFlux(Table data) {
        return Flux.create(emitter -> {
            try {
                int rowSize = data.rowCount();
                int colSize = data.columnCount();

                for (int i = 0; i < rowSize; i++) {
                    List<String> row = new ArrayList<>();
                    for (int j = 0; j < colSize; j++) {
                        row.add(data.get(i, j));
                    }
                    emitter.next(row);
                }
            } catch (Exception e) {}
            emitter.complete();
        });
    }

    public static String[] toStringArray(Object[] objs) {

        String[] stringArray = new String[objs.length];
        for (int i = 0; i < objs.length; i++) {
            stringArray[i] = objs[i].toString();
        }
        return stringArray;

        //return Arrays.asList(objs).toArray(new String[objs.length]);
    }

    public static void print(Object... objs) {
        for (Object obj : objs) {
            if (obj instanceof Object[]) {
                print(obj);
            } else {
                System.out.println(obj.toString());
            }
        }
    }

    public static <T> void print(Collection<T> objs) {
        for (Object obj : objs) {
            if (obj instanceof Collection) {
                print(obj);
            } else {
                System.out.println(obj.toString());
            }
        }
        System.out.println("========================================");
        System.out.println();
    }

    public static String toString(Object obj) {
        StringBuilder str = new StringBuilder();
        str.append(STRING_LIST_SEPARATOR);
        if (obj instanceof Map) {
            str.append(toString((Map) obj));
            str.append(STRING_LIST_SEPARATOR);
        } else if (obj instanceof Collection) {
            str.append(toString((Collection) obj));
            str.append(STRING_LIST_SEPARATOR);
        } else {
            str.append(obj.toString());
        }
        return str.toString();
    }

    public static String toString(Object obj, String separator) {
        StringBuilder str = new StringBuilder();
        if (obj instanceof Map) {
            str.append(toString((Map) obj));
        } else if (obj instanceof Collection) {
            str.append(toString((Collection) obj, separator));
        } else {
            str.append(obj.toString());
        }
        return str.toString();
    }

    public static <K, V> String toString(Map<K, V> map) {
        StringBuilder str = new StringBuilder();
        Collection<String> mapStr = map.entrySet().stream().parallel()
                .map(e -> "[" + toString(e.getKey(), STRING_ELEMENT_SEPARATOR) + "]" + STRING_MAP_SEPARATOR + toString(e.getValue(), STRING_ELEMENT_SEPARATOR))
                .collect(Collectors.toList());
        return toString(mapStr);
    }

    public static <T> String toString(Collection<T> objs, String separator) {
        StringBuilder str = new StringBuilder();
        for (Object obj : objs) {
            if (obj instanceof Map) {
                str.append(toString((Map) obj));
            } else if (obj instanceof Collection) {
                str.append(toString((Collection) obj, STRING_ELEMENT_SEPARATOR));
            } else {
                str.append(toString(obj));
            }
            str.append(separator);
        }
        return str.toString();
    }

    public static <T> String toString(Collection<T> objs) {
        return toString(objs, STRING_LIST_SEPARATOR);
    }

}
