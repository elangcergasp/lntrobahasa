package com.elangcergasp.lntrobahasa.util;

import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;

public class OpenMono<T> {

    private final Mono<T> mono;
    private MonoSink<T> emitter;

    public OpenMono() {
        emitter = null;
        mono = Mono.create(e -> {
            emitter = e;
        });
    }

    public Mono<T> getMono() {
        return mono;
    }

    public MonoSink<T> getEmitter() {
        while (emitter == null) {}
        return emitter;
    }
}
