package com.elangcergasp.lntrobahasa.util;

import reactor.core.publisher.Flux;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MultipleFluxSyncChainer<T> {

    private ConcurrentMap<String, FluxSyncChainer> fluxSyncChainerMap;

    public MultipleFluxSyncChainer() {
        fluxSyncChainerMap = new ConcurrentHashMap<>();
    }

    private synchronized FluxSyncChainer getChainer(String ID) {
        return fluxSyncChainerMap.compute(ID, (k, v) -> (v == null) ? new FluxSyncChainer() : v);
    }

    public synchronized Flux<T> enqueue(String ID, Flux<T> last) {
        return getChainer(ID).enqueue(last);
    }

    public synchronized Flux<T> head(String ID) {
        return getChainer(ID).head();
    }

    public synchronized Flux<T> tail(String ID) {
        return getChainer(ID).tail();
    }

    public synchronized Flux<T> dequeue(String ID) {
        return getChainer(ID).dequeue();
    }

    public synchronized void clearAll() {
        fluxSyncChainerMap.forEach((s, fluxSyncChainer) -> fluxSyncChainer.clear());
    }

}