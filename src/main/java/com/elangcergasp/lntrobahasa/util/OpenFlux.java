/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.util;

import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

/**
 *
 * @author elangcergasp
 */
public class OpenFlux<T> {

    private final Flux<T> flux;
    private FluxSink<T> emitter;

    public OpenFlux() {
        emitter = null;
        flux = Flux.create(e -> {
            emitter = e;
        });
    }

    public Flux<T> getFlux() {
        return flux;
    }

    public FluxSink<T> getEmitter() {
        while (emitter == null) {}
        return emitter;
    }
}
