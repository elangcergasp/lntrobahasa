/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.util.exporter;

import java.io.*;
import java.util.List;

/**
 *
 * @author elangcergasp
 */
public class CustomMapExporter {
    
    private CustomMapExporterFormatter formatter;

    public CustomMapExporter(CustomMapExporterFormatter formatter) {
        this.formatter = formatter;
    }
    
    public <T extends CustomMapExporterFormatter> CustomMapExporter(Class<T> formatterClass) throws InstantiationException, IllegalAccessException {
        this.formatter = formatterClass.newInstance();
    }

    public CustomMapExporterFormatter getFormatter() {
        return formatter;
    }

    public void setFormatter(CustomMapExporterFormatter formatter) {
        this.formatter = formatter;
    }
    
    public <T extends CustomMapExporterFormatter> void setFormatter(Class<T> formatterClass) throws InstantiationException, IllegalAccessException {
        this.formatter = formatterClass.newInstance();
    }
    
    public void export(CustomMapExportable exported, OutputStream outputStream) throws IOException {
        String exportedAsString = formatter.format(exported);
        outputStream.write(exportedAsString.getBytes());
    }
    
    public void export(List<CustomMapExportable> exported, OutputStream outputStream) throws IOException {
        String exportedAsString = formatter.format(exported);
        outputStream.write(exportedAsString.getBytes());
    }
    
    public void exportToConsole(CustomMapExportable exported) throws IOException {
        export(exported, System.out);
    }
    
    public void exportToConsole(List<CustomMapExportable> exported) throws IOException {
        export(exported, System.out);
    }
    
    public void exportToFile(CustomMapExportable exported, File outputFile) throws IOException, FileNotFoundException {
        export(exported, new FileOutputStream(outputFile));
    }
    
    public void exportToFile(List<CustomMapExportable> exported, File outputFile) throws IOException, FileNotFoundException {
        export(exported, new FileOutputStream(outputFile));
    }
    
    public void exportToFile(CustomMapExportable exported, String outputFilename) throws IOException, FileNotFoundException {
        export(exported, new FileOutputStream(outputFilename));
    }
    
    public void exportToFile(List<CustomMapExportable> exported, String outputFilename) throws IOException, FileNotFoundException {
        export(exported, new FileOutputStream(outputFilename));
    }
    
}
