/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.util.exporter;

import java.util.List;
import java.util.Map;

/**
 *
 * @author elangcergasp
 */
public interface CustomMapExportable {
    
    public List<String> exportColumnList();
    public Map<String, Object> exportDataMap();
    
}
