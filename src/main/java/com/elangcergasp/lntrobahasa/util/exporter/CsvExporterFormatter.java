/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.util.exporter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author elangcergasp
 */
public class CsvExporterFormatter implements CustomMapExporterFormatter {

    public static final String LIST_ELEMENT_SEPARATOR = "\n";
    public static final String DEFAULT_SEPARATOR = ";";
    public static final boolean DEFAULT_USE_HEADER = true;
    public static final boolean DEFAULT_ENCAPSULATE_STRING = true;
    public static final String DEFAULT_ENCAPSULATION_WRAPPER = "\"";

    private String separator = DEFAULT_SEPARATOR;
    private boolean useHeader = DEFAULT_USE_HEADER;
    private boolean encapsulateString = DEFAULT_ENCAPSULATE_STRING;
    private String encapsulationWrapper = DEFAULT_ENCAPSULATION_WRAPPER;

    public CsvExporterFormatter() {
    }

    public CsvExporterFormatter(String separator) {
        this.separator = separator;
    }

    public CsvExporterFormatter(String separator, boolean useHeader) {
        this.separator = separator;
        this.useHeader = useHeader;
    }

    public CsvExporterFormatter(String separator, boolean useHeader, boolean encapsulateString) {
        this.separator = separator;
        this.useHeader = useHeader;
        this.encapsulateString = encapsulateString;
    }

    public String formatHeader(CustomMapExportable data) {
        List<String> columns = data.exportColumnList();
        return String.join(separator, columns.toArray(new String[columns.size()]));
    }

    public String formatData(CustomMapExportable data, List<String> columns) {
        Map<String, Object> dataMap = data.exportDataMap();
        String w = encapsulationWrapper;
        return String.join(separator, columns.stream()
                .parallel()
                .map(c -> dataMap.getOrDefault(c, ""))
                .map(s -> !(s instanceof Number) ? s.toString() : s)
                .map(s -> encapsulateString && s instanceof String && s.toString().length() > 0 ? w + s + w : s)
                .collect(Collectors.toList()).toArray(new String[columns.size()]));
    }

    public String formatData(CustomMapExportable data) {
        List<String> columns = data.exportColumnList();
        return formatData(data, columns);
    }

    @Override
    public String format(CustomMapExportable data) {
        String dataAsString = formatData(data);
        if (useHeader) {
            String dataHeader = formatHeader(data);
            return dataHeader + LIST_ELEMENT_SEPARATOR + dataAsString;
        } else {
            return dataAsString;
        }
    }

    @Override
    public String format(List<CustomMapExportable> data) {
        int dataCount = data.size();
        if (dataCount > 0) {
            CustomMapExportable firstData = data.get(0);
            List<String> columns = firstData.exportColumnList();
            String allDataAsString = String.join(LIST_ELEMENT_SEPARATOR,
                    data.stream()
                            .parallel()
                            .map(d -> formatData(d, columns))
                            .collect(Collectors.toList()).toArray(new String[dataCount]));
            if (useHeader) {
                String dataHeader = formatHeader(firstData);
                return dataHeader + LIST_ELEMENT_SEPARATOR + allDataAsString;
            } else {
                return allDataAsString;
            }
        } else {
            return "";
        }
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public boolean isUsingHeader() {
        return useHeader;
    }

    public void setUseHeader(boolean useHeader) {
        this.useHeader = useHeader;
    }

    public boolean isEncapsulatingString() {
        return encapsulateString;
    }

    public void setEncapsulateString(boolean encapsulateString) {
        this.encapsulateString = encapsulateString;
    }

    public String getEncapsulationWrapper() {
        return encapsulationWrapper;
    }

    public void setEncapsulationWrapper(String encapsulationWrapper) {
        this.encapsulationWrapper = encapsulationWrapper;
    }

}
