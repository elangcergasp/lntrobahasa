package com.elangcergasp.lntrobahasa.controllers.api;

import com.elangcergasp.lntrobahasa.algorithm.CorpusConstruction;
import com.elangcergasp.lntrobahasa.algorithm.LNTROExtraction;
import com.elangcergasp.lntrobahasa.constants.URLs;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.properties.POSTaggerProperties;
import com.elangcergasp.lntrobahasa.properties.SearchEngineProperties;
import com.elangcergasp.lntrobahasa.services.InputDataService;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class PropertiesController implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesController.class);

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Autowired
    private LNTROProperties lntroProperties;

    @Autowired
    private SearchEngineProperties searchEngineProperties;

    @Autowired
    private POSTaggerProperties posTaggerProperties;

    @Autowired
    private InputDataService inputDataService;

    @CrossOrigin
    @GetMapping(value = URLs.PropertiesController.VALID_CSV_SEPARATORS, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<AvailableProperties> availableCSVSeparators() {
        return Flux.fromStream(
                InputDataService.VALID_SEPARATORS.stream()
                        .map(separator -> new AvailableProperties(
                                separator,
                                StringUtils.capitalize(separator.toLowerCase())
                        ))
        );
    }

    @CrossOrigin
    @GetMapping(value = URLs.PropertiesController.SEARCH_ENGINES, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<AvailableProperties> availableSearchEngines() {
        return Flux.fromStream(
                searchEngineProperties.getEngines().entrySet().stream()
                        .map(entry -> new AvailableProperties(
                                entry.getValue().getID(),
                                entry.getValue().getTitle(),
                                Stream.of((Map.Entry<String, Object>) new AbstractMap.SimpleEntry("countable", entry.getValue().getScrapper().isCountable()))
                                        .collect(Collectors.toMap(
                                                Map.Entry::getKey,
                                                Map.Entry::getValue
                                        ))
                        ))
        );
    }

    @CrossOrigin
    @GetMapping(value = URLs.PropertiesController.CONSTRUCTION_METHODS, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<AvailableProperties> availableConstructionMethods() {
        return Flux.fromStream(
                Stream.of(
                        CorpusConstruction.FROM_CACHE_SOURCE,
                        CorpusConstruction.FROM_CACHE,
                        CorpusConstruction.FROM_SOURCE
                ).map(method -> new AvailableProperties(
                        method,
                        String.join(", ",
                                Stream.of(method.split("_"))
                                        .map(String::toLowerCase)
                                        .map(StringUtils::capitalize)
                                        .collect(Collectors.toList())
                        )
                ))
        );
    }

    @CrossOrigin
    @GetMapping(value = URLs.PropertiesController.POS_TAGGERS, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<AvailableProperties> availablePOSTaggers() {
        return Flux.fromStream(
                posTaggerProperties.getModels().entrySet().stream()
                        .map(entry -> new AvailableProperties(
                                entry.getValue().getID(),
                                entry.getValue().getTitle()
                        ))
        );
    }

    @CrossOrigin
    @GetMapping(value = URLs.PropertiesController.EXTRACTION_METHODS, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<AvailableProperties> availableExtractionMethods() {
        return Flux.fromStream(
                applicationContext.getBeansOfType(LNTROExtraction.class)
                        .entrySet().stream()
                        .map(entry -> new AvailableProperties(
                                entry.getKey(),
                                entry.getValue().getDescription()
                        ))
        );
    }

    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private static class AvailableProperties {

        private String key;
        private String title;
        private Map<String, Object> meta;

        public AvailableProperties(String key, String title) {
            this.key = key;
            this.title = title;
        }
    }
}
