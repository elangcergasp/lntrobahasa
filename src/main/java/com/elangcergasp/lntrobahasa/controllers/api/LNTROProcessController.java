package com.elangcergasp.lntrobahasa.controllers.api;

import com.elangcergasp.lntrobahasa.algorithm.*;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROAlgorithmParams;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcess;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcessOutput;
import com.elangcergasp.lntrobahasa.constants.URLs;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.properties.StorageProperties;
import com.elangcergasp.lntrobahasa.repository.LNTROProcessRepository;
import com.elangcergasp.lntrobahasa.services.InputDataService;
import com.elangcergasp.lntrobahasa.services.cacher.SearchStatMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.services.storage.StorageService;
import com.elangcergasp.lntrobahasa.util.MyArray;
import lombok.*;
import org.apache.jena.ontology.OntModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.tablesaw.api.Table;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class LNTROProcessController implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(LNTROProcessController.class);

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Autowired
    private LNTROProperties lntroProperties;

    @Autowired
    private LNTROProcessRepository lntroProcessRepository;

    @Autowired
    private Preprocessor preprocessor;

    @Autowired
    private InputDataService inputDataService;

    @Autowired
    private BahasaStemmer stemmer;

    @Autowired
    private SearchStatMongoCacher searchStatMongoCacher;

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageProperties storageProperties;

    @Autowired
    private LNTROResultToOWLGenerator owlGeneratorService;

    @Autowired
    private BahasaLNTROProcessQueue lntroProcessQueue;

    // private Queue<String> processQueue = new ConcurrentLinkedQueue();

    @PostConstruct
    public void init() {

    }

    @CrossOrigin
    @PostMapping(value = URLs.LNTROController.SUBMIT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<OutputID> processSubmit(@RequestParam("title") String title,
                                        @RequestParam("description") String description,
                                        @RequestParam("data") MultipartFile dataFile,
                                        @RequestParam("separator") String separator,
                                        @RequestParam("corpusCount") Integer corpusCount,
                                        @RequestParam("corpusMethod") String corpusMethod,
                                        @RequestParam("constructionSearchEngineIDs") String[] constructionSearchEngineIDs,
                                        @RequestParam("posTaggerID") String posTaggerID,
                                        @RequestParam("extractionMethods") String[] extractionMethods,
                                        @RequestParam("evaluationSearchEngineIDs") String[] evaluationSearchEngineIDs) {

        LOGGER.debug("0 : " + title);
        LOGGER.debug("0 : " + description);
        LOGGER.debug("0 : " + dataFile.getOriginalFilename());
        LOGGER.debug("0 : " + separator);
        LOGGER.debug("0 : " + corpusCount);
        LOGGER.debug("0 : " + corpusMethod);
        LOGGER.debug("0 : " + String.join(",", constructionSearchEngineIDs));
        LOGGER.debug("0 : " + posTaggerID);
        LOGGER.debug("0 : " + String.join(",", extractionMethods));


        List<String> constructionSearchEngineIDList = Stream.of(constructionSearchEngineIDs).collect(Collectors.toList());
        List<String> extractionMethodList = Stream.of(extractionMethods).collect(Collectors.toList());
        List<String> evaluationSearchEngineIDList = Stream.of(evaluationSearchEngineIDs).collect(Collectors.toList());

        if (constructionSearchEngineIDList.size() == 0) {
            constructionSearchEngineIDList = new ArrayList<>(((Map<String, SearchEngineRunner>) this.applicationContext.getBean("getAllSearchEngineRunner")).keySet());
        }

        if (extractionMethodList.size() == 0) {
            extractionMethodList = Stream.of("MAANSExtraction", "MFLRExtraction").collect(Collectors.toList());
        }

        LNTROAlgorithmParams lntroAlgorithmParams = new LNTROAlgorithmParams();
        lntroAlgorithmParams.setCorpusConstructionMethod(corpusMethod);
        lntroAlgorithmParams.setConstructionSearchEngines(constructionSearchEngineIDList);
        lntroAlgorithmParams.setCorpusCount(corpusCount);
        lntroAlgorithmParams.setPosTagger(posTaggerID);
        lntroAlgorithmParams.setExtractionMethods(extractionMethodList);
        lntroAlgorithmParams.setEvaluationSearchEngines(evaluationSearchEngineIDList);
        LOGGER.debug("0 : " + lntroAlgorithmParams);

        Path dataBaseFilePath = storageService.load(storageService.safePath(storageProperties.getData()));
        Path dataPath = storageService.store(dataFile, dataBaseFilePath);
        LOGGER.debug("Stored Data File to : " + dataPath);

        Table data = inputDataService.getDataCSV(dataPath.toFile(), separator);
        List<String> columnNames = data.columnNames();
        Flux<List<String>> rows = MyArray.tableToRowsFlux(data);

        LNTROProcessOutput tmpOutput = new LNTROProcessOutput();
        tmpOutput.setData(rows);
        tmpOutput.setColumns(columnNames);

        String outputID = lntroProcessRepository.generateID().block();
        LNTROProcess lntroProcess = new LNTROProcess();
        lntroProcess.setId(outputID);
        lntroProcess.setTitle(title);
        lntroProcess.setDescription(description);
        lntroProcess.setAlgorithmParams(lntroAlgorithmParams);
        lntroProcess.setOutput(tmpOutput.block());

        lntroProcessQueue.doProcess(lntroProcess);

        return Mono.just(new OutputID(outputID));
    }

    @CrossOrigin
    @GetMapping(value = URLs.LNTROController.PROCESS_DETAIL + "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<LNTROProcess> processDetail(@PathVariable("id") String id) {
        return lntroProcessRepository.findById(id)
                .switchIfEmpty(Mono.fromCallable(() -> lntroProcessQueue.getProcess(id)).flatMap(p -> p));
    }

    @CrossOrigin
    @GetMapping(value = URLs.LNTROController.PROCESS_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<LNTROProcess> processList() {
        return lntroProcessRepository.findBy();
    }

    @CrossOrigin
    @GetMapping(value = URLs.LNTROController.PROCESS_QUEUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<LNTROProcess> processQueue() {
        return Flux.fromStream(Stream.concat(
                Stream.of(lntroProcessQueue.getCurrentProcessingItem().getValue()),
                lntroProcessQueue.getProcessQueueImmutable().stream().map(Map.Entry::getValue)
        ));
    }

    @CrossOrigin
    @GetMapping(value = URLs.DownloadController.ONTOLOGY + "/{id}/{term}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void ontology(
            @PathVariable("id") String id,
            @PathVariable("term") String term,
            HttpServletResponse response) {
        try {
            LNTROProcess lntroProcess = lntroProcessRepository.findById(id).block();
            OntModel ontologyModel = owlGeneratorService.createOntologyModel(
                    id+"--"+term,
                    lntroProcess.getOutput().getData(),
                    lntroProcess.getOutput().getColumns().get(0),
                    lntroProcess.getOutput().getColumns().get(1),
                    stemmer.getActivePassiveForm(term)
            );
            InputStream inputStream = new FileInputStream(owlGeneratorService.saveOntologyTemporary(ontologyModel, id+"--"+term));
            // copy it to response's OutputStream
            response.setContentType("application/xml");
            org.apache.commons.io.IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            LOGGER.error("Error writing file to temporary output stream.", ex);
            throw new RuntimeException("IOError writing file to output stream");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Error no such LNTRO Process with id "+id+" exists in database.", ex);
            throw new RuntimeException("No such LNTRO Process with id "+id+" exists in database");
        }
    }


    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private static class OutputID {

        private String ID;
    }
}
