package com.elangcergasp.lntrobahasa.controllers.api;

import com.elangcergasp.lntrobahasa.constants.URLs;
import com.elangcergasp.lntrobahasa.properties.StorageProperties;
import com.elangcergasp.lntrobahasa.services.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
public class DownloadController implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadController.class);

    private ApplicationContext applicationContext;

    @Autowired
    private StorageProperties storageProperties;

    @Autowired
    private StorageService storageService;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    public void init() {
        //
    }

    @CrossOrigin
    @GetMapping(value = URLs.DownloadController.ONTOLOGY + "/{filename}")
    public void ontology(
            @PathVariable("filename") String filename,
            HttpServletResponse response) {
        try {
            // get your file as InputStream
            String filepath = storageService.safePath(storageProperties.getOntology())+filename;
            InputStream inputStream = new FileInputStream(storageService.load(filepath).toFile());
            // copy it to response's OutputStream
            response.setContentType("application/xml");
            org.apache.commons.io.IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            LOGGER.error("Error writing file to output stream. Filename was '{}'", filename, ex);
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

}
