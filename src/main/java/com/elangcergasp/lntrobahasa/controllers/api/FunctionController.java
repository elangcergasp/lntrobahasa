package com.elangcergasp.lntrobahasa.controllers.api;

import com.elangcergasp.lntrobahasa.constants.URLs;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.services.cacher.SearchResultMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.MyKBBI;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;

@RestController
public class FunctionController implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionController.class);

    private ApplicationContext applicationContext;

    @Autowired
    private LNTROProperties prop;

    @Autowired
    private SearchResultMongoCacher searchResultMongoCacher;

    private MyKBBI KBBI = new MyKBBI();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    public void init() {
        //
    }

    @GetMapping(value = URLs.FunctionController.SEARCH + "/{engine}/{keyword}/{count}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<SearchResultItem> search(
            @PathVariable("engine") String engine,
            @PathVariable("keyword") String keyword,
            @PathVariable("count") Integer count) {
        SearchEngineRunner searchEngineRunner = applicationContext.getBean(SearchEngineRunner.class, engine);

        Flux<SearchResultItem> searchResult = searchEngineRunner.getSearchResult(keyword, count);

        return searchResult;
    }

    @GetMapping(value = URLs.FunctionController.SEARCH + "/{engine}/{keyword}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<SearchResultItem> search(
            @PathVariable("engine") String engine,
            @PathVariable("keyword") String keyword) {
        int count = prop.getConstruction().getCorpusCount();
        return search(engine, keyword, count);
    }

    @GetMapping(value = URLs.FunctionController.KBBI + "/document/{term}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public String kbbiDocument(@PathVariable("term") String term) {
        return KBBI.getDocument(term).outerHtml();
    }

    @GetMapping(value = URLs.FunctionController.KBBI + "/description/{term}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public String kbbiDescription(@PathVariable("term") String term) {
        return KBBI.getDescription(term).outerHtml();
    }

    @GetMapping(value = URLs.FunctionController.KBBI + "/highlighted/{term}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public String kbbiHighlighted(@PathVariable("term") String term) {
        return KBBI.getHighlightedElements(term).outerHtml();
    }

    @GetMapping(value = URLs.FunctionController.KBBI + "/postag/{term}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public String kbbiPOSTag(@PathVariable("term") String term) {
        return KBBI.getHighlightedPOSTag(term).toString();
    }
}
