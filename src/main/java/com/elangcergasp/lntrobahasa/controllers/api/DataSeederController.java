package com.elangcergasp.lntrobahasa.controllers.api;

import com.elangcergasp.lntrobahasa.constants.URLs;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.services.ErrorBufferService;
import com.elangcergasp.lntrobahasa.services.InputDataService;
import com.elangcergasp.lntrobahasa.services.cacher.KBBIMongoCacher;
import com.elangcergasp.lntrobahasa.services.cacher.SearchResultMongoCacher;
import com.elangcergasp.lntrobahasa.services.cacher.SearchStatMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineScrapper;
import com.elangcergasp.lntrobahasa.util.MyArray;
import lombok.*;
import org.apache.http.client.utils.URIBuilder;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.tablesaw.api.Table;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class DataSeederController implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSeederController.class);

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Autowired
    private LNTROProperties lntroProperties;

    @Autowired
    private SearchResultMongoCacher searchResultMongoCacher;

    @Autowired
    private SearchStatMongoCacher searchStatMongoCacher;

    @Autowired
    private KBBIMongoCacher kbbiMongoCacher;

    @Autowired
    private InputDataService inputDataService;

    private Map<String, SearchEngineScrapper> searchEngineScrapperMap;

    @PostConstruct
    public void init() {
        searchEngineScrapperMap = ((Map<String, SearchEngineRunner>) applicationContext.getBean("getAllSearchEngineRunner"))
                .entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> (SearchEngineScrapper) entry.getValue()
                ));
    }

    @CrossOrigin
    @PostMapping(value = URLs.DataSeederController.KBBI, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Boolean> kbbi(@RequestParam("term") String term,
                              @RequestParam("document") String document) {
        return kbbiMongoCacher.saveDocumentKBBI(term, document);
    }

    @CrossOrigin
    @PostMapping(value = URLs.DataSeederController.SEARCH_RESULT_ITEM, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Boolean> searchResultItem(@RequestParam("searchEngine") String searchEngine,
                                          @RequestParam("keyword") String keyword,
                                          @RequestParam("data") Collection<String> data,
                                          @RequestParam("document") String document) {
        SearchEngineScrapper scrapper = searchEngineScrapperMap.get(searchEngine);
        Collection<SearchResultItem> searchResultItems = scrapper.parseSearchResultItem(keyword, Jsoup.parse(document))
                .stream()
                .map(searchResultItem -> {
                    searchResultItem.setMeta(data);
                    return searchResultItem;
                })
                .map(sr -> {
                    searchResultMongoCacher.getSearchResultRepository().save(sr).subscribe(searchResultItem -> {
                        LOGGER.trace("[" + scrapper.getSearchEngine().getTitle() + "] Saved search result " + searchResultItem + "");
                    });
                    return sr;
                })
                .collect(Collectors.toList());

        return Mono.just(searchResultItems.size() > 0);
    }

    @CrossOrigin
    @PostMapping(value = URLs.DataSeederController.SEARCH_RESULT_STAT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Boolean> searchResultStat(@RequestParam("searchEngine") String searchEngine,
                                          @RequestParam("keyword") String keyword,
                                          @RequestParam("data") Collection<String> data,
                                          @RequestParam("document") String document) {
        SearchEngineScrapper scrapper = searchEngineScrapperMap.get(searchEngine);
        SearchResultStat searchResultStat = scrapper.parseSearchResultStat(keyword, Jsoup.parse(document));

        searchStatMongoCacher.getSearchStatRepository().save(searchResultStat).subscribe(stat -> {
            LOGGER.trace("[" + scrapper.getSearchEngine().getTitle() + "] Saved search stat " + stat + "");
        });
        return Mono.just(searchResultStat != null);
    }

    @CrossOrigin
    @PostMapping(value = URLs.DataSeederController.CREATE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Map<String, Map<Collection<String>, SearchEngineTarget>>> createUrl(
            @RequestParam("data") MultipartFile dataFile,
            @RequestParam("separator") String separator,
            @RequestParam("searchEngineIDs") String[] searchEngineIDs) {
        try {
            Table data = inputDataService.getDataCSV(dataFile.getInputStream(), separator);
            Flux<List<String>> rows = MyArray.tableToRowsFlux(data);
            // LOGGER.debug("Data : " + rows.collectList().block());
            List<String> searchEngineIDList = Stream.of(searchEngineIDs).collect(Collectors.toList());
            return Flux.fromIterable(searchEngineIDList)
                    .parallel()
                    // .runOn(Schedulers.parallel())
                    .map(se -> searchEngineScrapperMap.get(se))
                    .map(se -> new AbstractMap.SimpleEntry<>(
                            se.getSearchEngine().getID(),
                            rows.map(r -> {
                                String keyword = se.createKeyword(r);
                                String url = se.getSearchEngine().getHttp().getUrl();
                                try {
                                    URIBuilder u = new URIBuilder(url);
                                    se.makeQueryData(
                                            keyword,
                                            se.getNearestOffsetPerIncrement(0),
                                            se.getOffsetPerIncrement()
                                    ).forEach((k, v) -> {
                                        u.addParameter(k, v);
                                    });
                                    url = u.build().toString();
                                } catch (Exception e) {}
                                return new AbstractMap.SimpleEntry<>(
                                        r,
                                        new SearchEngineTarget(
                                                se.getSearchEngine().getID(),
                                                keyword,
                                                url
                                        )
                                );
                            }).collectList()
                    ))
                    .sequential()
                    .collectList()
                    .map(mapEntries -> mapEntries.stream().collect(Collectors.toMap(
                            AbstractMap.SimpleEntry::getKey,
                            e -> e.getValue().block().stream().collect(Collectors.toMap(
                                    Map.Entry::getKey,
                                    Map.Entry::getValue
                            ))
                    )));
        } catch (Exception e) {}
        return Mono.empty();
    }

    @CrossOrigin
    @GetMapping(value = URLs.DataSeederController.SEARCH_ERROR_BUFFER_URLS, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Map<String, Map<Collection<String>, SearchEngineTarget>>> searchErrorBufferUrls() {
        try {
            return Flux.fromIterable(ErrorBufferService.getInstance().getAllByClass(SearchEngineScrapper.class))
                    .parallel()
                    // .runOn(Schedulers.parallel())
                    .map(errorBuffer -> {
                        Map<String, Object> meta = errorBuffer.getMeta();
                        SearchEngineScrapper se = searchEngineScrapperMap.get(meta.get("searchEngine"));
                        String keyword = (String) meta.get("keyword");
                        String url = se.getSearchEngine().getHttp().getUrl();
                        try {
                            URIBuilder u = new URIBuilder(url);
                            se.makeQueryData(
                                    keyword,
                                    se.getNearestOffsetPerIncrement(0),
                                    se.getOffsetPerIncrement()
                            ).forEach((k, v) -> {
                                u.addParameter(k, v);
                            });
                            url = u.build().toString();
                        } catch (Exception e) {}
                        return new SearchEngineTarget(
                                se.getSearchEngine().getID(),
                                keyword,
                                url
                        );
                    })
                    .sequential()
                    .groupBy(SearchEngineTarget::getSearchEngine)
                    .map(e -> new AbstractMap.SimpleEntry<>(
                            e.key(),
                            e.collectList()
                    ))
                    .collectList()
                    .map(mapEntries -> mapEntries.stream().collect(Collectors.toMap(
                            AbstractMap.SimpleEntry::getKey,
                            e -> e.getValue().block().stream().collect(Collectors.toMap(
                                    ee -> Stream.of(UUID.randomUUID().toString()).collect(Collectors.toList()),
                                    ee -> ee
                            ))
                    )));
        } catch (Exception e) {}
        return Mono.empty();
    }

    @CrossOrigin
    @GetMapping(value = URLs.DataSeederController.SEARCH_ERROR_BUFFER_CLEAR, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<SearchEngineTarget> searchErrorBufferClear() {
        try {
            return Flux.fromIterable(ErrorBufferService.getInstance().clearClass(SearchEngineScrapper.class))
                    .parallel()
                    // .runOn(Schedulers.parallel())
                    .map(ErrorBufferService.ErrorBuffer::getMeta)
                    .map(meta -> {
                        SearchEngineScrapper se = searchEngineScrapperMap.get(meta.get("searchEngine"));
                        String keyword = (String) meta.get("keyword");
                        String url = se.getSearchEngine().getHttp().getUrl();
                        try {
                            URIBuilder u = new URIBuilder(url);
                            se.makeQueryData(
                                    keyword,
                                    se.getNearestOffsetPerIncrement(0),
                                    se.getOffsetPerIncrement()
                            ).forEach((k, v) -> {
                                u.addParameter(k, v);
                            });
                            url = u.build().toString();
                        } catch (Exception e) {}
                        return new SearchEngineTarget(
                                se.getSearchEngine().getID(),
                                keyword,
                                url
                        );
                    })
                    .sequential();
        } catch (Exception e) {}
        return Flux.empty();
    }

    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Document
    class SearchEngineTarget {
        String searchEngine;
        String keyword;
        String url;
    }

}
