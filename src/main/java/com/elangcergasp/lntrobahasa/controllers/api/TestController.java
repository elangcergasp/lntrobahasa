/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.controllers.api;

import com.elangcergasp.lntrobahasa.algorithm.*;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcessOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermScoreMap;
import com.elangcergasp.lntrobahasa.constants.URLs;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.services.cacher.KBBIMongoCacher;
import com.elangcergasp.lntrobahasa.util.MyArray;
import io.netty.util.internal.ConcurrentSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author elangcergasp
 */
@RestController
public class TestController implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private LNTROProperties prop;

    @Autowired
    private KBBIMongoCacher KBBI;

    private ApplicationContext applicationContext;

    private Table data = null;
    private Flux<List<String>> rows = null;

    private Set<String> extractionStopwords;

    private static Preprocessor preprocessor;
    private static CorpusAnnotation corpusAnnotation;

    private static LNTROProcessOutput prevCorpusCachedSearched = null;
    private static LNTROProcessOutput prevCorpusCached = null;
    private static LNTROProcessOutput prevCorpusSearched = null;

    private static LNTROProcessOutput prevAnnotatedCachedSearched = null;
    private static LNTROProcessOutput prevAnnotatedCached = null;
    private static LNTROProcessOutput prevAnnotatedSearched = null;

    @PostConstruct
    public void init() {
        preprocessor = applicationContext.getBean(Preprocessor.class);
        corpusAnnotation = applicationContext.getBean(CorpusAnnotation.class);
        KBBI = applicationContext.getBean(KBBIMongoCacher.class);

        String dataPath = prop.getData();
        if (dataPath != null && dataPath.length() > 0) {
            InputStream dataInput = null;
            try {
                dataInput = new FileInputStream(dataPath);

                data = Table.read().csv(CsvReadOptions
                        .builder(dataInput, "DATA")
                        .separator('\t')
                        .header(true)
                );
                rows = MyArray.tableToRowsFlux(data).doFinally((t) -> {
                    LOGGER.info("Fetched data from \"" + dataPath + "\"");
                }).cache(Integer.MAX_VALUE).share();

            } catch (FileNotFoundException ex) {
                LOGGER.error("Data file not found : " + ex.getMessage());
                return;
            } catch (IOException ex) {
                LOGGER.error("Input Data error : " + ex.getMessage());
                return;
            }
        }

        String extractionStopwordsPath = prop.getPreprocessing().getStopwords().get("extraction");
        if (extractionStopwordsPath != null && extractionStopwordsPath.length() > 0) {
            InputStream extractionStopwordsInput = null;
            try {
                extractionStopwordsInput = new FileInputStream(extractionStopwordsPath);

                extractionStopwords = new ConcurrentSet<>();

                MyArray.tableToRowsFlux(
                        Table.read().csv(CsvReadOptions
                                .builder(extractionStopwordsInput, "STOPWORDS")
                                .separator('\t')
                                .header(false)))
                        .doFinally(signalType -> {
                            LOGGER.info("Fetched Extraction Stopwords from \""+extractionStopwordsPath+"\" with "+(extractionStopwords == null ? 0 : extractionStopwords.size())+" words");
                        })
                        .subscribe(stopwordRow -> {
                            stopwordRow.forEach(stopword -> {
                                extractionStopwords.add(stopword);
                            });
                        });
            } catch (FileNotFoundException ex) {
                LOGGER.error("Stopword file not found : " + ex.getMessage());
                return;
            } catch (IOException ex) {
                LOGGER.error("Input Stopwords error : " + ex.getMessage());
                return;
            }
        }
    }

    @GetMapping(value = URLs.TestController.DATA)
    public String data() {
        return data.print();
    }

    @GetMapping(value = URLs.TestController.ROWS, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<List<String>> rows() {
        return rows;
    }

    @GetMapping(value = URLs.TestController.CONSTRUCT_CACHED_SEARCHED + "/{searchEngineID}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Flux<String>>> constructCachedSearched(@PathVariable String searchEngineID) {
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class, searchEngineID);

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);

        LNTROProcessOutput keywords = lntro.constructData(rows);
        LNTROProcessOutput corpus = lntro.constructCorpus(keywords);
        prevCorpusCachedSearched = corpus;
        return corpus.getCorpus();
    }

    @GetMapping(value = URLs.TestController.CONSTRUCT_CACHED_SEARCHED, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Flux<String>>> constructCachedSearched() {
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class);

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);

        LNTROProcessOutput data = lntro.constructData(rows);
        LNTROProcessOutput corpus = lntro.constructCorpus(data);
        prevCorpusCachedSearched = corpus;
        return corpus.getCorpus();
    }

    @GetMapping(value = URLs.TestController.CONSTRUCT_CACHED + "/{searchEngineID}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Flux<String>>> constructCached(@PathVariable String searchEngineID) {
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class, searchEngineID);

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);

        LNTROProcessOutput keywords = lntro.constructData(rows);
        LNTROProcessOutput corpus = lntro.constructCorpusCached(keywords);
        prevCorpusCached = corpus;
        return corpus.getCorpus();
    }

    @GetMapping(value = URLs.TestController.CONSTRUCT_CACHED, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Flux<String>>> constructCached() {
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class);

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);

        LNTROProcessOutput keywords = lntro.constructData(rows);
        LNTROProcessOutput corpus = lntro.constructCorpusCached(keywords);
        prevCorpusCached = corpus;
        return corpus.getCorpus();
    }

    @GetMapping(value = URLs.TestController.CONSTRUCT_SEARCHED + "/{searchEngineID}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Flux<String>>> constructSearched(@PathVariable String searchEngineID) {
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class, searchEngineID);

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);

        LNTROProcessOutput keywords = lntro.constructData(rows);
        LNTROProcessOutput corpus = lntro.constructCorpusSearched(keywords);
        prevCorpusSearched = corpus;
        return corpus.getCorpus();
    }

    @GetMapping(value = URLs.TestController.CONSTRUCT_SEARCHED, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Flux<String>>> constructSearched() {
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class);

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);

        LNTROProcessOutput keywords = lntro.constructData(rows);
        LNTROProcessOutput corpus = lntro.constructCorpusSearched(keywords);
        prevCorpusSearched = corpus;
        return corpus.getCorpus();
    }

    @GetMapping(value = URLs.TestController.ANNOTATE_CACHED_SEARCHED, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Mono<TermFrequencyMap>>> annotateCachedSearched() {
        if (prevCorpusCachedSearched == null) {
            return null;
            /*
            return String.format("Corpus has not been constructed yet; see <a href=\"%s\">%s</a>",
                    URLs.TestController.CONSTRUCT_CACHED_SEARCHED,
                    "Construct Cached Searched Corpus");
             */
        }

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusAnnotation(corpusAnnotation);

        LNTROProcessOutput annotated = lntro.annotateCorpus(prevCorpusCachedSearched);
        prevAnnotatedCachedSearched = annotated;
        return annotated.getTermFrequencyMaps();
    }

    @GetMapping(value = URLs.TestController.ANNOTATE_CACHED, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Mono<TermFrequencyMap>>> annotateCached() {
        if (prevCorpusCached == null) {
            return null;
            /*
            return String.format("Corpus has not been constructed yet; see <a href=\"%s\">%s</a>",
                    URLs.TestController.CONSTRUCT_CACHED,
                    "Construct Cached Corpus");
             */
        }

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusAnnotation(corpusAnnotation);

        LNTROProcessOutput annotated = lntro.annotateCorpus(prevCorpusCached);
        prevAnnotatedCached = annotated;
        return annotated.getTermFrequencyMaps();
    }

    @GetMapping(value = URLs.TestController.ANNOTATE_SEARCHED, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map.Entry<List<String>, Mono<TermFrequencyMap>>> annotateSearched() {
        if (prevCorpusSearched == null) {
            return null;
            /*
            return String.format("Corpus has not been constructed yet; see <a href=\"%s\">%s</a>",
                    URLs.TestController.CONSTRUCT_SEARCHED,
                    "Construct Searched Corpus");
             */
        }

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusAnnotation(corpusAnnotation);

        LNTROProcessOutput annotated = lntro.annotateCorpus(prevCorpusSearched);
        prevAnnotatedSearched = annotated;
        return annotated.getTermFrequencyMaps();
    }

    @GetMapping(value = URLs.TestController.EXTRACT_CACHED_SEARCHED + "/{extractionMethod}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<TermScoreMap> extractCachedSearched(@PathVariable String extractionMethod) {
        if (prevAnnotatedCachedSearched == null) {
            return null;
            /*
            return String.format("Corpus has not been annotated yet; see <a href=\"%s\">%s</a>",
                    URLs.TestController.ANNOTATE_CACHED_SEARCHED,
                    "Annotated Cached Searched Corpus");
             */
        }

        LNTROExtraction extraction;
        try {
            extraction = (LNTROExtraction) applicationContext.getBean(extractionMethod.toUpperCase() + "Extraction");
        } catch (NoSuchBeanDefinitionException e) {
            return null;
            /*
            return "Extraction method '" + extractionMethod + "' does not exist";
             */
        }

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.addLntroExtraction(extractionMethod, extraction);
        lntro.setKBBI(KBBI);
        lntro.setExtractionStopwords(extractionStopwords);

        LNTROProcessOutput extracted = lntro.extract(prevAnnotatedCachedSearched);
        return extracted.getExtractionScores()
                .filterWhen(entry -> Mono.just(entry.getKey().equals(extractionMethod)))
                .blockFirst().getValue();
    }

    @GetMapping(value = URLs.TestController.EXTRACT_CACHED + "/{extractionMethod}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<TermScoreMap> extractCached(@PathVariable String extractionMethod) {
        if (prevAnnotatedCached == null) {
            return null;
            /*
            return String.format("Corpus has not been annotated yet; see <a href=\"%s\">%s</a>",
                    URLs.TestController.ANNOTATE_CACHED,
                    "Annotated Cached Corpus");
             */
        }

        LNTROExtraction extraction;
        try {
            extraction = (LNTROExtraction) applicationContext.getBean(extractionMethod.toUpperCase() + "Extraction");
        } catch (NoSuchBeanDefinitionException e) {
            return null;
            /*
            return "Extraction method '" + extractionMethod + "' does not exist";
             */
        }

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.addLntroExtraction(extractionMethod, extraction);
        lntro.setKBBI(KBBI);
        lntro.setExtractionStopwords(extractionStopwords);

        LNTROProcessOutput extracted = lntro.extract(prevAnnotatedCached);
        return extracted.getExtractionScores()
                .filterWhen(entry -> Mono.just(entry.getKey().equals(extractionMethod)))
                .blockFirst().getValue();
    }

    @GetMapping(value = URLs.TestController.EXTRACT_SEARCHED + "/{extractionMethod}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<TermScoreMap> extractSearched(@PathVariable String extractionMethod) {
        if (prevAnnotatedSearched == null) {
            return null;
            /*
            return String.format("Corpus has not been annotated yet; see <a href=\"%s\">%s</a>",
                    URLs.TestController.ANNOTATE_SEARCHED,
                    "Annotated Searched Corpus");
             */
        }

        LNTROExtraction extraction;
        try {
            extraction = (LNTROExtraction) applicationContext.getBean(extractionMethod.toUpperCase() + "Extraction");
        } catch (NoSuchBeanDefinitionException e) {
            return null;
            /*
            return "Extraction method '" + extractionMethod + "' does not exist";
             */
        }

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.addLntroExtraction(extractionMethod, extraction);
        lntro.setKBBI(KBBI);
        lntro.setExtractionStopwords(extractionStopwords);

        LNTROProcessOutput extracted = lntro.extract(prevAnnotatedSearched);
        return extracted.getExtractionScores()
                .filterWhen(entry -> Mono.just(entry.getKey().equals(extractionMethod)))
                .blockFirst().getValue();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
