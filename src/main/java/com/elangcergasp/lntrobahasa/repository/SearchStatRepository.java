package com.elangcergasp.lntrobahasa.repository;

import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface SearchStatRepository extends ReactiveMongoRepository<SearchResultStat, String> {

    Mono<SearchResultStat> findBySearchEngine(String searchEngine);

    Mono<SearchResultStat> findByKeyword(String keyword);

    @Query("{ 'searchEngine': ?0, 'keyword': ?1}")
    Mono<SearchResultStat> findBySearchEngineAndKeyword(String searchEngine, String keyword);

    @Query("{ 'searchEngine': ?0, 'meta': ?1}")
    Mono<SearchResultStat> findBySearchEngineAndMeta(String searchEngine, Object meta);

    Mono<SearchResultStat> deleteByKeyword(String keyword);

    Mono<SearchResultStat> deleteByCount(Long count);

}
