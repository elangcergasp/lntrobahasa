package com.elangcergasp.lntrobahasa.repository;

import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface CustomSearchResultRepository {

    Flux<SearchResultItem> deleteDuplicatesByUrlAndMeta();
}
