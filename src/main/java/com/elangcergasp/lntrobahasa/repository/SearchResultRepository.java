/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.repository;

import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author Elang Cergas Pembrani
 */
@Repository
public interface SearchResultRepository extends ReactiveMongoRepository<SearchResultItem, String>, CustomSearchResultRepository {

    Flux<SearchResultItem> findBySearchEngine(String searchEngine);

    Flux<SearchResultItem> findByKeyword(String keyword);

    Flux<SearchResultItem> findByMeta(Object meta);

    @Query("{ 'searchEngine': ?0, 'keyword': ?1}")
    Flux<SearchResultItem> findBySearchEngineAndKeyword(String searchEngine, String keyword);

    @Query("{ 'searchEngine': ?0, 'meta': ?1}")
    Flux<SearchResultItem> findBySearchEngineAndMeta(String searchEngine, Object meta);

    @Query("{ 'keyword': ?0, 'searchEngine': { $in: ?1 } }")
    Flux<SearchResultItem> findByKeywordAndSearchEngines(String keyword, String... searchEngines);

    @Query("{ 'meta': ?0, 'searchEngine': { $in: ?1 } }")
    Flux<SearchResultItem> findByMetaAndSearchEngines(Object meta, String... searchEngines);

    Mono<SearchResultItem> deleteByKeyword(String keyword);

    Mono<SearchResultItem> deleteByMeta(Object keyword);
    
}
