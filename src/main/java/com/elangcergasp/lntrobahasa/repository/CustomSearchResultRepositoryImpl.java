package com.elangcergasp.lntrobahasa.repository;

import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.mongodb.AggregationOptions;
import com.mongodb.async.client.AggregateIterable;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.reactivestreams.client.MongoCollection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CustomSearchResultRepositoryImpl implements CustomSearchResultRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomSearchResultRepositoryImpl.class);

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @Autowired
    private MappingMongoConverter mappingMongoConverter;

    @Override
    public Flux<SearchResultItem> deleteDuplicatesByUrlAndMeta() {
        MongoCollection<Document> collection = reactiveMongoTemplate.getCollection(reactiveMongoTemplate.getCollectionName(SearchResultItem.class));
        return Flux.from(collection
                .aggregate(
                        Arrays.asList(
                                Aggregates.group(
                                        new Document()
                                                .append("contentUrl", "$contentUrl")
                                                .append("meta", "$meta"),
                                        Arrays.asList(
                                                Accumulators.addToSet("dups", "$_id"),
                                                Accumulators.sum("count", 1)
                                        )
                                ),
                                Aggregates.match(Filters.gt("count", 1))
                        )
                )
                .allowDiskUse(true))
                .flatMap(document -> {
                    try {
                        List<String> dups = ((Collection<String>) document.get("dups")).stream().collect(Collectors.toList());
                        if (dups.size() == 1) {
                            return Mono.empty();
                        }
                        String firstDocumentID = dups.remove(0);
                        return Mono.from(collection.deleteMany(Filters.in("_id", dups)))
                                .then(Mono.just(firstDocumentID));
                    } catch (Exception e) {
                        return Mono.empty();
                    }
                })
                .collectList()
                .flatMapMany(ids -> collection.find(Filters.in("_id", ids)))
                .map(document -> mappingMongoConverter.read(SearchResultItem.class, document));
    }
}
