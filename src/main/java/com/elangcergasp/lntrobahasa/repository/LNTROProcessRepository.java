package com.elangcergasp.lntrobahasa.repository;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROBlockOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcess;
import com.elangcergasp.lntrobahasa.controllers.api.LNTROProcessController;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.reactivestreams.client.gridfs.AsyncInputStream;
import com.mongodb.reactivestreams.client.gridfs.AsyncOutputStream;
import com.mongodb.reactivestreams.client.gridfs.GridFSBucket;
import com.mongodb.reactivestreams.client.gridfs.helpers.AsyncStreamHelper;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

@Repository
//@RepositoryRestResource(collectionResourceRel = "lntroProcess", path = "lntro/process")
public class LNTROProcessRepository {

    public static final String FILE_PREFIX = LNTROProcess.class.getSimpleName();
    public static final String FILE_EXT = ".json";

    private static final Logger LOGGER = LoggerFactory.getLogger(LNTROProcessController.class);

    @Autowired
    private GridFSBucket gridFSBucket;

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @Autowired
    private MappingMongoConverter mappingMongoConverter;

    public Mono<String> generateID() {
        return Flux.generate(synchronousSink -> synchronousSink.next(UUID.randomUUID().toString()))
                .map(Object::toString)
                .filterWhen(s -> reactiveMongoTemplate.findById(s, LNTROProcess.class)
                        .map(p -> false).defaultIfEmpty(true))
                .next();
    }

    public Flux<LNTROProcess> findBy() {
        return reactiveMongoTemplate.findAll(LNTROProcess.class)
                .map(lntroProcess -> {
                    if (lntroProcess.getOutput() != null) {
                        lntroProcess.setOutput(null);
                    }
                    return lntroProcess;
                });
    }

    public Mono<LNTROProcess> findById(String id) {
        return reactiveMongoTemplate.findById(id, LNTROProcess.class)
                .map(lntroProcess -> {
                    if (lntroProcess.getOutput() == null) {
                        lntroProcess.setOutput(getOutput(id).block());
                    }
                    return lntroProcess;
                });
    }

    public Mono<LNTROProcess> save(LNTROProcess lntroProcess) {
        String processID = lntroProcess.getId();

        LNTROProcess tmpProcess = new LNTROProcess();
        tmpProcess.setId(processID);
        tmpProcess.setFetchDate(lntroProcess.getFetchDate());
        tmpProcess.setTitle(lntroProcess.getTitle());
        tmpProcess.setDescription(lntroProcess.getDescription());
        tmpProcess.setAlgorithmParams(lntroProcess.getAlgorithmParams());
        tmpProcess.setOntologyOWLFile(lntroProcess.getOntologyOWLFile());

        Document tmpDocument = (Document) mappingMongoConverter.convertToMongoType(tmpProcess);

        Document lntroBlockOutput = (Document) mappingMongoConverter.convertToMongoType(lntroProcess.getOutput());
        AsyncInputStream inputStream = AsyncStreamHelper.toAsyncInputStream(
                new ByteArrayInputStream(lntroBlockOutput.toJson().getBytes(StandardCharsets.UTF_8))
        );

        GridFSUploadOptions options = new GridFSUploadOptions()
                .chunkSizeBytes(358400)
                .metadata(tmpDocument);

        return Mono.from(gridFSBucket.uploadFromStream(getFilename(processID), inputStream, options))
                .then(reactiveMongoTemplate.save(tmpProcess));
    }

    public String getFilename(String processID) {
        return FILE_PREFIX + "_" + processID + FILE_EXT;
    }

    public Mono<LNTROBlockOutput> getOutput(String processID) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        AsyncOutputStream aos = AsyncStreamHelper.toAsyncOutputStream(baos);

        return Mono.from(gridFSBucket.downloadToStream(getFilename(processID), aos))
                .map(count -> Document.parse(baos.toString()))
                .map(document -> mappingMongoConverter.read(LNTROBlockOutput.class, document));
    }
}

