package com.elangcergasp.lntrobahasa.repository;

import com.elangcergasp.lntrobahasa.model.DocumentKBBI;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface KBBIRepository extends ReactiveMongoRepository<DocumentKBBI, String> {

    Mono<DocumentKBBI> findByTerm(String term);

    Mono<DocumentKBBI> deleteByTerm(String term);

}
