/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.config;

import com.elangcergasp.lntrobahasa.algorithm.BahasaCorpusAnnotation;
import com.elangcergasp.lntrobahasa.algorithm.BahasaPOSTagger;
import com.elangcergasp.lntrobahasa.algorithm.CorpusAnnotation;
import com.elangcergasp.lntrobahasa.algorithm.POSTagger;
import com.elangcergasp.lntrobahasa.properties.POSTaggerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author Elang Cergas Pembrani
 */
@Configuration
@Import(PropertiesConfiguration.class)
public class CorpusAnnotationConfiguration {

    @Autowired
    private PropertiesConfiguration propertiesConfiguration;

    @Autowired
    private POSTaggerProperties posTaggerProperties;

    @Bean @Scope("prototype")
    public POSTagger posTagger(String modelFilePath) {
        return new BahasaPOSTagger(modelFilePath);
    }

    @Bean @Scope("prototype")
    public CorpusAnnotation corpusAnnotation(String modelFilePath) {
        return new BahasaCorpusAnnotation(posTagger(modelFilePath));
    }

    @Bean
    public POSTagger posTagger() {
        String modelID = propertiesConfiguration.lntroProperties().getAnnotation().getPosTagger();
        return new BahasaPOSTagger(posTaggerProperties.getModels().get(modelID).getModelFile());
    }

    @Bean
    public CorpusAnnotation corpusAnnotation() {
        return new BahasaCorpusAnnotation(posTagger());
    }

}
