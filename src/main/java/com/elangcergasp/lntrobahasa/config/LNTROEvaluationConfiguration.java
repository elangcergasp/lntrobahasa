/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.config;

import com.elangcergasp.lntrobahasa.algorithm.LNTROEvaluation;
import com.elangcergasp.lntrobahasa.algorithm.TopSearchEvaluation;
import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.services.cacher.SearchStatMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.util.MultipleFluxSyncChainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

import java.util.Map;

/**
 *
 * @author Elang Cergas Pembrani
 */
@Configuration
@Import(PropertiesConfiguration.class)
public class LNTROEvaluationConfiguration {

    @Autowired
    private LNTROProperties lntroProperties;

    @Autowired
    private SearchStatMongoCacher searchStatMongoCacher;

    @Autowired
    private MultipleFluxSyncChainer<SearchResultStat> syncChainer;

    @Bean @Scope("prototype")
    public LNTROEvaluation topSearchEvaluation(Map<String, SearchEngineRunner> searchEngineRunnerMap) {
        LNTROProperties.Evaluation.TopSearch topSearchProperties = lntroProperties.getEvaluation().getTopSearch();

        return new TopSearchEvaluation(
                searchStatMongoCacher,
                syncChainer,
                searchEngineRunnerMap,
                topSearchProperties.getCorpusCount()
        );
    }

}
