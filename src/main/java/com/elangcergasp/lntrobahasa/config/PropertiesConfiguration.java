/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.config;

import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.properties.POSTaggerProperties;
import com.elangcergasp.lntrobahasa.properties.SearchEngineProperties;
import com.elangcergasp.lntrobahasa.properties.StorageProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author elangcergasp
 */
@Configuration
public class PropertiesConfiguration {
    
    @Bean
    public LNTROProperties lntroProperties() {
        return new LNTROProperties();
    }
    
    @Bean
    public SearchEngineProperties searchEngineProperties() {
        return new SearchEngineProperties();
    }

    @Bean
    public POSTaggerProperties posTaggerProperties() {
        return new POSTaggerProperties();
    }

    @Bean
    public StorageProperties storageProperties() {
        return new StorageProperties();
    }

}
