package com.elangcergasp.lntrobahasa.config;

import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import com.elangcergasp.lntrobahasa.util.MultipleFluxSyncChainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServicesConfiguration {

    @Bean
    public MultipleFluxSyncChainer<SearchResultItem> syncChainerSearchResultItem() {
        return new MultipleFluxSyncChainer();
    }

    @Bean
    public MultipleFluxSyncChainer<SearchResultStat> syncChainerSearchResultStat() {
        return new MultipleFluxSyncChainer();
    }

}
