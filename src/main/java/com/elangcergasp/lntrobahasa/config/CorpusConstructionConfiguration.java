/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.config;

import com.elangcergasp.lntrobahasa.algorithm.BahasaCorpusConstruction;
import com.elangcergasp.lntrobahasa.algorithm.BahasaPreprocessor;
import com.elangcergasp.lntrobahasa.algorithm.CorpusConstruction;
import com.elangcergasp.lntrobahasa.algorithm.Preprocessor;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.properties.SearchEngineProperties;
import com.elangcergasp.lntrobahasa.services.cacher.SearchResultMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineScrapper;
import com.elangcergasp.lntrobahasa.util.MultipleFluxSyncChainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import javax.annotation.PostConstruct;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 *
 * @author Elang Cergas Pembrani
 */
@Configuration
@Import({PropertiesConfiguration.class})
public class CorpusConstructionConfiguration {

    private CorpusConstruction corpusConstruction;

    private Map<String, SearchEngineRunner> searchEngineRunnerMap;

    @Autowired
    private PropertiesConfiguration propertiesConfiguration;

    @Autowired
    private LNTROProperties lntroProperties;

    @Autowired
    private MultipleFluxSyncChainer<SearchResultItem> syncChainer;

    @Autowired
    private SearchEngineProperties searchEngineProperties;

    @Autowired
    private SearchResultMongoCacher searchResultMongoCacher;

    @PostConstruct
    public void init() {
        searchEngineRunnerMap = new ConcurrentHashMap<>();
        searchEngineProperties = propertiesConfiguration.searchEngineProperties();
        if (searchEngineProperties != null) {
            searchEngineProperties.getEngines().forEach((ID, se) -> {
                if (se != null) {
                    SearchEngineRunner searchEngineRunner = new SearchEngineScrapper(se);
                    searchEngineRunnerMap.put(se.getID(), searchEngineRunner);
                }
            });
        }

        lntroProperties = propertiesConfiguration.lntroProperties();
        if (lntroProperties != null) {
            BahasaCorpusConstruction corpusConstruction = new BahasaCorpusConstruction(
                    searchResultMongoCacher,
                    syncChainer
            );
            corpusConstruction.setCorpusCount(lntroProperties.getConstruction().getCorpusCount());
            lntroProperties.getConstruction().getSources().forEach((ID, se) -> {
                if (se != null) {
                    corpusConstruction.addSearchEngineScrapper(searchEngineRunnerMap.get(se.getSearchEngine()));
                }
            });
            this.corpusConstruction = corpusConstruction;
        }
    }

    @Bean @Scope("prototype")
    public SearchEngineRunner searchEngineRunner(String ID) {
        return searchEngineRunnerMap.get(ID);
    }

    @Bean @Scope("prototype")
    public Map<String, SearchEngineRunner> searchEngineRunners(List<String> IDs) {
        return IDs.stream()
                .map(ID -> new AbstractMap.SimpleEntry<>(ID, searchEngineRunnerMap.get(ID)))
                .collect(Collectors.toMap(
                        AbstractMap.SimpleEntry::getKey,
                        AbstractMap.SimpleEntry::getValue
                ));
    }

    @Bean @Scope("prototype")
    public Map<String, SearchEngineRunner> searchEngineRunnersStatCounter(List<String> IDs) {
        return IDs.stream()
                .map(ID -> searchEngineRunnerMap.get(ID))
                .filter(searchEngineRunner -> searchEngineRunner.getSearchEngine().getScrapper().isCountable())
                .map(searchEngineRunner -> new AbstractMap.SimpleEntry<>(searchEngineRunner.getSearchEngine().getID(), searchEngineRunner))
                .collect(Collectors.toMap(
                        AbstractMap.SimpleEntry::getKey,
                        AbstractMap.SimpleEntry::getValue
                ));
    }

    @Bean @Scope("prototype")
    public Map<String, SearchEngineRunner> searchEngineRunnersStatCounter(List<String> IDs, String fallbackID) {
        List<SearchEngineRunner> searchEngineRunnerList = IDs.stream()
                .map(ID -> searchEngineRunnerMap.get(ID))
                .filter(searchEngineRunner -> searchEngineRunner.getSearchEngine().getScrapper().isCountable())
                .collect(Collectors.toList());

        if (searchEngineRunnerList.size() == 0) {
            if (fallbackID != null) {
                searchEngineRunnerList.add(searchEngineRunnerMap.get(fallbackID));
            }
        }

        return searchEngineRunnerList.stream()
                .map(searchEngineRunner -> new AbstractMap.SimpleEntry<>(searchEngineRunner.getSearchEngine().getID(), searchEngineRunner))
                .collect(Collectors.toMap(
                        AbstractMap.SimpleEntry::getKey,
                        AbstractMap.SimpleEntry::getValue
                ));
    }

    @Bean @Scope("prototype")
    public CorpusConstruction corpusConstruction(Map<String, SearchEngineRunner> searchEngineRunners) {
        BahasaCorpusConstruction corpusConstruction = new BahasaCorpusConstruction(
                searchResultMongoCacher,
                syncChainer
        );
        lntroProperties = propertiesConfiguration.lntroProperties();
        for (SearchEngineRunner ser : searchEngineRunners.values()) {
            corpusConstruction.addSearchEngineScrapper(ser);
        }
        return corpusConstruction;
    }

    @Bean @Scope("prototype")
    public CorpusConstruction corpusConstruction(List<String> IDs) {
        BahasaCorpusConstruction corpusConstruction = new BahasaCorpusConstruction(
                searchResultMongoCacher,
                syncChainer
        );
        lntroProperties = propertiesConfiguration.lntroProperties();
        if (lntroProperties != null) {
            // int corpusCount = lntroProperties.getConstruction().getCorpusCount();
            for (String ID : IDs) {
                corpusConstruction.addSearchEngineScrapper(searchEngineRunnerMap.get(ID));
            }
        }
        return corpusConstruction;
    }

    @Bean @Scope("prototype")
    public CorpusConstruction corpusConstruction(List<String> IDs, int corpusCount) {
        BahasaCorpusConstruction corpusConstruction = new BahasaCorpusConstruction(
                searchResultMongoCacher,
                syncChainer
        );
        corpusConstruction.setCorpusCount(corpusCount);
        lntroProperties = propertiesConfiguration.lntroProperties();
        if (lntroProperties != null) {
            for (String ID : IDs) {
                corpusConstruction.addSearchEngineScrapper(searchEngineRunnerMap.get(ID));
            }
        }
        return corpusConstruction;
    }

    @Bean("getAllSearchEngineRunner")
    @Lazy
    public Map<String, SearchEngineRunner> getAllSearchEngineRunner() {
        return searchEngineRunnerMap;
    }
    
    @Bean
    @Lazy
    public CorpusConstruction defaultCorpusConstruction() {
        return corpusConstruction;
    }

    @Bean
    public Preprocessor preprocessor() {
        return new BahasaPreprocessor();
    }

}
