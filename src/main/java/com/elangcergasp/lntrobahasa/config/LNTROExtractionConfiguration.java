/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.config;

import com.elangcergasp.lntrobahasa.algorithm.LNTROExtraction;
import com.elangcergasp.lntrobahasa.algorithm.MAANSExtraction;
import com.elangcergasp.lntrobahasa.algorithm.MFLRExtraction;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author elangcergasp
 */
@Configuration
public class LNTROExtractionConfiguration {
    
    @Bean
    @Qualifier("MAANSExtraction")
    public LNTROExtraction MAANSExtraction() {
        return new MAANSExtraction();
    }
    
    @Bean
    @Qualifier("MFLRExtraction")
    public LNTROExtraction MFLRExtraction() {
        return new MFLRExtraction();
    }
    
}
