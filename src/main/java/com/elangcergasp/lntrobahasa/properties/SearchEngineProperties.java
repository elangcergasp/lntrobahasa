/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.properties;

import com.elangcergasp.lntrobahasa.model.SearchEngine;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 *
 * @author Elang Cergas Pembrani
 */
@ToString
@ConfigurationProperties(prefix = "search-engine")
public class SearchEngineProperties {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchEngineProperties.class);

    private Map<String, SearchEngine> engines;

    public Map<String, SearchEngine> getEngines() {
        return engines;
    }

    public void setEngines(Map<String, SearchEngine> engines) {
        this.engines = engines;
    }

}
