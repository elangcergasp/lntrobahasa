package com.elangcergasp.lntrobahasa.properties;

import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@ToString
@ConfigurationProperties(prefix = "pos-tagger")
public class POSTaggerProperties {

    private Map<String, Model> models;

    public Map<String, Model> getModels() {
        return models;
    }

    public void setModels(Map<String, Model> models) {
        this.models = models;
    }

    @ToString
    public static class Model {

        private String ID;
        private String title;
        private String modelFile;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getModelFile() {
            return modelFile;
        }

        public void setModelFile(String modelFile) {
            this.modelFile = modelFile;
        }
    }
}
