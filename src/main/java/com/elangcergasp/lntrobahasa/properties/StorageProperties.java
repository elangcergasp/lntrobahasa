package com.elangcergasp.lntrobahasa.properties;

import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ToString
@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder baseLocation for storing files
     */
    private String baseLocation = "uploads";

    private String data = "data";

    private String ontology = "ontology";

    public String getBaseLocation() {
        return baseLocation;
    }

    public void setBaseLocation(String baseLocation) {
        this.baseLocation = baseLocation;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOntology() {
        return ontology;
    }

    public void setOntology(String ontology) {
        this.ontology = ontology;
    }
}
