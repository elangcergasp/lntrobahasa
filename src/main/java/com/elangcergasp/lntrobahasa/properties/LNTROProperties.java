/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.properties;

import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 *
 * @author Elang Cergas Pembrani
 */
@ToString
@ConfigurationProperties(prefix = "lntro")
public class LNTROProperties {

    private String data;
    private Preprocessing preprocessing;
    private Construction construction;
    private Annotation annotation;
    private Evaluation evaluation;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Preprocessing getPreprocessing() {
        return preprocessing;
    }

    public void setPreprocessing(Preprocessing preprocessing) {
        this.preprocessing = preprocessing;
    }

    public Construction getConstruction() {
        return construction;
    }

    public void setConstruction(Construction construction) {
        this.construction = construction;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    @ToString
    public static class Preprocessing {

        private Map<String, String> stopwords;

        public Map<String, String> getStopwords() {
            return stopwords;
        }

        public void setStopwords(Map<String, String> stopwords) {
            this.stopwords = stopwords;
        }
    }
    
    @ToString
    public static class Construction {

        private Integer corpusCount;
        private Map<String, Source> sources;

        public Integer getCorpusCount() {
            return corpusCount;
        }

        public void setCorpusCount(Integer corpusCount) {
            this.corpusCount = corpusCount;
        }

        public Map<String, Source> getSources() {
            return sources;
        }

        public void setSources(Map<String, Source> sources) {
            this.sources = sources;
        }

        @ToString
        public static class Source {

            private String searchEngine;

            public String getSearchEngine() {
                return searchEngine;
            }

            public void setSearchEngine(String searchEngine) {
                this.searchEngine = searchEngine;
            }
        }
    }

    @ToString
    public static class Annotation {

        private String posTagger;

        public String getPosTagger() {
            return posTagger;
        }

        public void setPosTagger(String posTagger) {
            this.posTagger = posTagger;
        }
    }

    @ToString
    public static class Evaluation {

        private TopSearch topSearch;

        public TopSearch getTopSearch() {
            return topSearch;
        }

        public void setTopSearch(TopSearch topSearch) {
            this.topSearch = topSearch;
        }

        @ToString
        public static class TopSearch {

            private Integer topTermCount;
            private Integer randomDataSamplingCount;
            private Integer corpusCount;

            public Integer getTopTermCount() {
                return topTermCount;
            }

            public void setTopTermCount(Integer topTermCount) {
                this.topTermCount = topTermCount;
            }

            public Integer getRandomDataSamplingCount() {
                return randomDataSamplingCount;
            }

            public void setRandomDataSamplingCount(Integer randomDataSamplingCount) {
                this.randomDataSamplingCount = randomDataSamplingCount;
            }

            public Integer getCorpusCount() {
                return corpusCount;
            }

            public void setCorpusCount(Integer corpusCount) {
                this.corpusCount = corpusCount;
            }
        }
    }
}
