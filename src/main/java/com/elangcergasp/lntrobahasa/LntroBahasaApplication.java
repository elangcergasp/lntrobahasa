package com.elangcergasp.lntrobahasa;

import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.elangcergasp.lntrobahasa")
public class LntroBahasaApplication implements CommandLineRunner, ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(LntroBahasaApplication.class);

    @Autowired
    private LNTROProperties prop;

    private ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(LntroBahasaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        /*
        String dataPath = prop.getData();
        InputStream dataInput = new FileInputStream(dataPath);

        Table data = null;
        int rowLimit = 5;
        Collection<Collection<String>> rows = null;
        Collection<Collection<String>> rowsLimited = null;
        try {
            data = Table.read().csv(CsvReadOptions.builder(dataInput, "DATA").separator('\t').header(true));
            rows = MyArray.tableToRows(data);

            rowsLimited = ((List) rows).subList(0, rowLimit); // LIMIT DATA
        } catch (FileNotFoundException ex) {
            LOGGER.error("Data not found : " + ex.getMessage());
            return;
        } catch (IOException ex) {
            LOGGER.error("Input Data error : " + ex.getMessage());
            return;
        }
        LOGGER.info("Fetched data from \"" + dataPath + "\" with " + (rowsLimited == null ? 0 : rowsLimited.size()) + " rows");
        //LOGGER.info(MyArray.toString(rowsLimited));
        LOGGER.info(data.print());

        int i = 0;
        String searchEngineID = prop.getConstruction().getSources().get(i).getSearchEngine();
        Preprocessor preprocessor = applicationContext.getBean(Preprocessor.class);
        CorpusConstruction corpusConstruction = applicationContext.getBean(CorpusConstruction.class, searchEngineID);
        CorpusAnnotation corpusAnnotation = applicationContext.getBean(CorpusAnnotation.class);

        LNTROExtraction maansExtraction = (LNTROExtraction) applicationContext.getBean("MAANSExtraction");
        LNTROExtraction mflrExtraction = (LNTROExtraction) applicationContext.getBean("MFLRExtraction");

        LNTROAlgorithm lntro = new LNTROAlgorithm();
        lntro.setPreprocessor(preprocessor);
        lntro.setCorpusConstruction(corpusConstruction);
        lntro.setCorpusAnnotation(corpusAnnotation);

        LNTROAlgorithm lntroMAANS = new LNTROAlgorithm();
        lntroMAANS.setPreprocessor(preprocessor);
        lntroMAANS.setCorpusConstruction(corpusConstruction);
        lntroMAANS.setCorpusAnnotation(corpusAnnotation);
        lntroMAANS.setLntroExtraction(maansExtraction);

        LNTROAlgorithm lntroMFLR = new LNTROAlgorithm();
        lntroMFLR.setPreprocessor(preprocessor);
        lntroMFLR.setCorpusConstruction(corpusConstruction);
        lntroMFLR.setCorpusAnnotation(corpusAnnotation);
        lntroMFLR.setLntroExtraction(mflrExtraction);

        LNTROProcessOutput constructedCorpus = lntro.constructCorpus(rows);
        LNTROProcessOutput annotatedCorpus = lntro.annotateCorpus(constructedCorpus);

        LNTROProcessOutput maansExtracted = lntroMAANS.extract(annotatedCorpus);
        LNTROProcessOutput mflrExtracted = lntroMFLR.extract(annotatedCorpus);
        
        final int topValuesIndexThreshold = 3;
        List<AtomicTermScore> maansTopValues = maansExtracted.getLntroFinalScores().toAtomicTermList().subList(0, topValuesIndexThreshold);
        List<AtomicTermScore> mflrTopValues = mflrExtracted.getLntroFinalScores().toAtomicTermList().subList(0, topValuesIndexThreshold);

        LOGGER.info(maansExtracted.getLntroFinalScores().toString());
        LOGGER.info(mflrExtracted.getLntroFinalScores().toString());
        
        LOGGER.info(MyArray.toString(maansTopValues));
        LOGGER.info(MyArray.toString(mflrTopValues));
         */
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
