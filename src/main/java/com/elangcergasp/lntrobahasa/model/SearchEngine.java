/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

/**
 *
 * @author Elang Cergas Pembrani
 */
@EqualsAndHashCode
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class SearchEngine {

    @Id
    private String ID;
    private String title;
    private Http http;
    private Scrapper scrapper;
    private Params params;

    @ToString
    @Getter
    @Setter
    public static class Http {

        private String url;
        private String wildcard;
        private String quoteAll;
        private String quoteData;
        private String method;
        private String keywordField;
        private String offsetField;
        private String limitField;
        private Integer initialOffset;
        private Map<String, String> additionalFields;
    }

    @ToString
    @Getter
    @Setter
    public static class Scrapper {

        private String urlSelector;
        private String titleSelector;
        private String snippetSelector;
        private String statCountSelector;
        private String statElapsedSelector;
        private String negativeSelector;

        public Boolean isCountable() {
            return statCountSelector != null && statCountSelector.length() > 0;
        }
    }

    @ToString
    @Getter
    @Setter
    public static class Params {

        private Boolean threadSync;
        private Integer limitPerIncrement;
        private Integer offsetPerIncrement;
        private Integer retryLimit;
        private Long retryDelay;
        private Boolean useProxy;
        private Boolean usePrevOffsetReferrer;
    }
}
