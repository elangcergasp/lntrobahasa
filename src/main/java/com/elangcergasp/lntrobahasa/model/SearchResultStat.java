package com.elangcergasp.lntrobahasa.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class SearchResultStat {

    @Id
    private String id;

    @CreatedDate
    private Date fetchDate = defaultFetchDate();

    private String searchEngine;
    private String keyword;
    private Long count;
    private Double elapsed;
    private Object meta;

    public static Date defaultFetchDate() { return Calendar.getInstance().getTime(); }

}
