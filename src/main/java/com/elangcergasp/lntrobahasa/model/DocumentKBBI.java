package com.elangcergasp.lntrobahasa.model;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTag;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class DocumentKBBI {

    public static final String SEPARATOR = ". ";

    @Id
    private String id;

    @CreatedDate
    private Date fetchDate = defaultFetchDate();

    private String term;
    private String documentHTML;
    private DocumentKBBI reference;
    private POSTag posTag;

    public static Date defaultFetchDate() { return Calendar.getInstance().getTime(); }

}
