/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.model;

import com.elangcergasp.lntrobahasa.util.exporter.CustomMapExportable;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Elang Cergas Pembrani
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class SearchResultItem implements CustomMapExportable {

    public static final String SEPARATOR = ". ";

    @Id
    private String id;

    @CreatedDate
    private Date fetchDate = defaultFetchDate();

    private String searchEngine;
    private String keyword;
    private String contentUrl;
    private String contentTitle;
    private String contentSnippet;
    private Object meta;

    public static Date defaultFetchDate() { return Calendar.getInstance().getTime(); }

    public String getContent() {
        return contentTitle + SEPARATOR + contentSnippet;
    }

    public static String SEARCH_ENGINE_EXPORTABLE_FIELD = "Search Engine";
    public static String KEYWORD_EXPORTABLE_FIELD = "Keyword";
    public static String CONTENT_URL_EXPORTABLE_FIELD = "URL";
    public static String CONTENT_TITLE_EXPORTABLE_FIELD = "Title";
    public static String CONTENT_SNIPPET_EXPORTABLE_FIELD = "Snippet";

    @Override
    public List<String> exportColumnList() {
        return Stream.of(SEARCH_ENGINE_EXPORTABLE_FIELD,
                KEYWORD_EXPORTABLE_FIELD,
                CONTENT_URL_EXPORTABLE_FIELD,
                CONTENT_TITLE_EXPORTABLE_FIELD,
                CONTENT_SNIPPET_EXPORTABLE_FIELD)
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> exportDataMap() {
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put(SEARCH_ENGINE_EXPORTABLE_FIELD, searchEngine);
        map.put(KEYWORD_EXPORTABLE_FIELD, keyword);
        map.put(CONTENT_URL_EXPORTABLE_FIELD, contentUrl);
        map.put(CONTENT_TITLE_EXPORTABLE_FIELD, contentTitle);
        map.put(CONTENT_SNIPPET_EXPORTABLE_FIELD, contentSnippet);
        return map;
    }

    public boolean sourceEquals(SearchResultItem that) {
        if (this == that) return true;
        return Objects.equals(this.contentUrl, that.contentUrl) &&
                Objects.equals(this.meta, that.meta);
    }

    public int sourceHashCode() {
        return Objects.hash(contentUrl, meta);
    }
}
