/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.services.cacher.SearchResultMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.util.MultipleFluxSyncChainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Elang Cergas Pembrani
 */
public class BahasaCorpusConstruction implements CorpusConstruction {

    public static class SearchIncrement {

        private SearchEngineRunner searchEngineRunner;
        private int offset;
        private int count;

        public SearchIncrement() {
        }

        public SearchIncrement(SearchEngineRunner searchEngineRunner, int offset, int count) {
            this.searchEngineRunner = searchEngineRunner;
            this.offset = offset;
            this.count = count;
        }

        public SearchEngineRunner getSearchEngineRunner() {
            return searchEngineRunner;
        }

        public void setSearchEngineRunner(SearchEngineRunner searchEngineRunner) {
            this.searchEngineRunner = searchEngineRunner;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(BahasaCorpusConstruction.class);

    private final Map<String, SearchEngineRunner> searchEngineRunnerMap = new ConcurrentHashMap<>();
    private MultipleFluxSyncChainer<SearchResultItem> syncChainer;

    private SearchResultMongoCacher cacher;
    private int corpusCount = CORPUS_DEFAULT_COUNT;

    public BahasaCorpusConstruction() {
    }

    public BahasaCorpusConstruction(SearchResultMongoCacher cacher, MultipleFluxSyncChainer<SearchResultItem> syncChainer) {
        this.cacher = cacher;
        this.syncChainer = syncChainer;
    }

    @Override
    public Flux<CorpusItem> getCorpus(Collection<String> data) {
        return getCorpus(data, corpusCount);
    }

    @Override
    public Flux<CorpusItem> getCorpus(Collection<String> data, int count) {
        Flux<CorpusItem> corpus = getSearchResultFromCacheThenSource(data, count)
                .map((sri) -> new CorpusItem(sri.getSearchEngine(), sri.getContent()));
        return corpus.take(count);
    }

    @Override
    public Flux<CorpusItem> getCorpus(String keyword) {
        return getCorpus(keyword, corpusCount);
    }

    @Override
    public Flux<CorpusItem> getCorpus(String keyword, int count) {
        Flux<CorpusItem> corpus = getSearchResultFromCacheThenSource(keyword, count)
                .map((sri) -> new CorpusItem(sri.getSearchEngine(), sri.getContent()));
        return corpus.take(count);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromCache(Collection<String> data) {
        return getCorpusFromCache(data, corpusCount);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromCache(Collection<String> data, int count) {
        Flux<CorpusItem> corpus = getSearchResultFromCache(data, count)
                .map((sri) -> new CorpusItem(sri.getSearchEngine(), sri.getContent()));
        return corpus.take(count);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromCache(String keyword) {
        return getCorpusFromCache(keyword, corpusCount);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromCache(String keyword, int count) {
        Flux<CorpusItem> corpus = getSearchResultFromCache(keyword, count)
                .map((sri) -> new CorpusItem(sri.getSearchEngine(), sri.getContent()));
        return corpus.take(count);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromSource(Collection<String> data) {
        return getCorpusFromSource(data, corpusCount);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromSource(Collection<String> data, int count) {
        return getCorpusFromSource(data, 0, count);
    }

    public Flux<CorpusItem> getCorpusFromSource(Collection<String> data, int offset, int count) {
        Flux<CorpusItem> corpus = getSearchResultFromSource(data, offset, count)
                .map((sri) -> new CorpusItem(sri.getSearchEngine(), sri.getContent()));
        return corpus.take(count);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromSource(String keyword) {
        return getCorpusFromSource(keyword, corpusCount);
    }

    @Override
    public Flux<CorpusItem> getCorpusFromSource(String keyword, int count) {
        return getCorpusFromSource(keyword, 0, count);
    }

    public Flux<CorpusItem> getCorpusFromSource(String keyword, int offset, int count) {
        Flux<CorpusItem> corpus = getSearchResultFromSource(keyword, offset, count)
                .map((sri) -> new CorpusItem(sri.getSearchEngine(), sri.getContent()));
        return corpus.take(count);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(Collection<String> data, int count) {
        String[] searchEngineIDs = new ArrayList<>(searchEngineRunnerMap.keySet())
                .toArray(new String[searchEngineRunnerMap.size()]);
        return cacher.getSearchResultFromCache(searchEngineIDs, data, count);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String keyword, int count) {
        String[] searchEngineIDs = new ArrayList<>(searchEngineRunnerMap.keySet())
                .toArray(new String[searchEngineRunnerMap.size()]);
        return cacher.getSearchResultFromCache(searchEngineIDs, keyword, count);
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromCacheGrouped(Collection<String> data, int count) {
        return getSearchResultFromCache(data, count)
                .groupBy(SearchResultItem::getSearchEngine)
                .map(searchResultItemGroupedFlux -> new AbstractMap.SimpleEntry<>(
                        searchResultItemGroupedFlux.key(),
                        searchResultItemGroupedFlux.share()
                ));
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromCacheGrouped(String keyword, int count) {
        return getSearchResultFromCache(keyword, count)
                .groupBy(SearchResultItem::getSearchEngine)
                .map(searchResultItemGroupedFlux -> new AbstractMap.SimpleEntry<>(
                        searchResultItemGroupedFlux.key(),
                        searchResultItemGroupedFlux.share()
                ));
    }

    public Flux<SearchResultItem> getSearchResultFromSource(Collection<String> data, int offset, int count) {
        return getSearchResultFromSourceGrouped(data, offset, count)
                // .parallel()
                // .runOn(Schedulers.parallel())
                .flatMap(Map.Entry::getValue)
                // .sequential()
                .distinct(SearchResultItem::sourceHashCode);
    }

    public Flux<SearchResultItem> getSearchResultFromSource(String keyword, int offset, int count) {
        return getSearchResultFromSourceGrouped(keyword, offset, count)
                // .parallel()
                // .runOn(Schedulers.parallel())
                .flatMap(Map.Entry::getValue)
                // .sequential()
                .distinct(SearchResultItem::sourceHashCode);
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromSourceGrouped(Collection<String> data, int offset, int count) {
        int searchOffset = (int) Math.ceil((double) offset / (double) searchEngineRunnerMap.size());
        int searchCount = (int) Math.ceil((double) count / (double) searchEngineRunnerMap.size());
        Collection<SearchIncrement> searchIncrements = searchEngineRunnerMap
                .values().stream()
                // .parallel()
                .map(searchEngineRunner -> new SearchIncrement(searchEngineRunner, searchOffset, searchCount))
                .collect(Collectors.toList());
        return getSearchResultFromSourceGrouped(data, searchIncrements);
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromSourceGrouped(String keyword, int offset, int count) {
        int searchOffset = (int) Math.ceil((double) offset / (double) searchEngineRunnerMap.size());
        int searchCount = (int) Math.ceil((double) count / (double) searchEngineRunnerMap.size());
        Collection<SearchIncrement> searchIncrements = searchEngineRunnerMap
                .values().stream()
                // .parallel()
                .map(searchEngineRunner -> new SearchIncrement(searchEngineRunner, searchOffset, searchCount))
                .collect(Collectors.toList());
        return getSearchResultFromSourceGrouped(keyword, searchIncrements);
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromSourceGrouped(Collection<String> data, Collection<SearchIncrement> searchIncrements) {
        Flux<Map.Entry<String, Flux<SearchResultItem>>> searchResults = Flux.fromIterable(searchIncrements)
                // .parallel()
                // .runOn(Schedulers.parallel())
                .map(searchIncrement -> {
                    SearchEngineRunner searchEngineRunner = searchIncrement.getSearchEngineRunner();
                    int searchOffset = searchIncrement.getOffset();
                    int searchCount = searchIncrement.getCount();
                    Map.Entry<String, Flux<SearchResultItem>> searchResult;
                    if (searchEngineRunner.getSearchEngine().getParams().getThreadSync()) {
                        synchronized (this) {
                            searchResult = new AbstractMap.SimpleEntry<>(
                                    searchEngineRunner.getSearchEngine().getID(),
                                    syncChainer.enqueue(searchEngineRunner.getSearchEngine().getID(),
                                            cacher.getSearchResultFromSearchEngine(searchEngineRunner, data, searchOffset, searchCount))
                            );
                        }
                    } else {
                        searchResult = new AbstractMap.SimpleEntry<>(
                                searchEngineRunner.getSearchEngine().getID(),
                                cacher.getSearchResultFromSearchEngine(searchEngineRunner, data, searchOffset, searchCount)
                        );
                    }
                    return searchResult;
                });
                // .sequential();

        return searchResults.share();
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromSourceGrouped(String keyword, Collection<SearchIncrement> searchIncrements) {
        Flux<Map.Entry<String, Flux<SearchResultItem>>> searchResults = Flux.fromIterable(searchIncrements)
                // .parallel()
                // .runOn(Schedulers.parallel())
                .map(searchIncrement -> {
                    SearchEngineRunner searchEngineRunner = searchIncrement.getSearchEngineRunner();
                    int searchOffset = searchIncrement.getOffset();
                    int searchCount = searchIncrement.getCount();
                    Map.Entry<String, Flux<SearchResultItem>> searchResult;
                    if (searchEngineRunner.getSearchEngine().getParams().getThreadSync()) {
                        synchronized (this) {
                            searchResult = new AbstractMap.SimpleEntry<>(
                                    searchEngineRunner.getSearchEngine().getID(),
                                    syncChainer.enqueue(searchEngineRunner.getSearchEngine().getID(),
                                            cacher.getSearchResultFromSearchEngine(searchEngineRunner, keyword, searchOffset, searchCount))
                            );
                        }
                    } else {
                        searchResult = new AbstractMap.SimpleEntry<>(
                                searchEngineRunner.getSearchEngine().getID(),
                                cacher.getSearchResultFromSearchEngine(searchEngineRunner, keyword, searchOffset, searchCount)
                        );
                    }
                    return searchResult;
                });
                // .sequential();

        return searchResults.share();
    }

    public Flux<SearchResultItem> getSearchResultFromCacheThenSource(Collection<String> data, int count) {
        return getSearchResultFromCacheThenSourceGrouped(data, count)
                // .parallel()
                // .runOn(Schedulers.parallel())
                .flatMap(Map.Entry::getValue)
                // .sequential()
                .distinct(SearchResultItem::sourceHashCode);
    }

    public Flux<SearchResultItem> getSearchResultFromCacheThenSource(String keyword, int count) {
        return getSearchResultFromCacheThenSourceGrouped(keyword, count)
                // .parallel()
                // .runOn(Schedulers.parallel())
                .flatMap(Map.Entry::getValue)
                // .sequential()
                .distinct(SearchResultItem::sourceHashCode);
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromCacheThenSourceGrouped(Collection<String> data, int count) {
        Flux<Map.Entry<String, Flux<SearchResultItem>>> searchResults = Flux.create(masterEmitter -> {
            try {
                getSearchResultFromCache(data, count)
                        .collectList()
                        .subscribe(searchResultItemList -> {
                            Map<String, List<SearchResultItem>> cacheGrouped = searchEngineRunnerMap.entrySet().stream()
                                    .collect(Collectors.toMap(
                                            Map.Entry::getKey,
                                            entry -> new ArrayList<>()
                                    ));

                            searchResultItemList.forEach(searchResultItem -> {
                                cacheGrouped.compute(searchResultItem.getSearchEngine(), (k, v) -> {
                                    if (v == null) {
                                        v = new ArrayList<>();
                                    }
                                    v.add(searchResultItem);
                                    return v;
                                });
                            });

                            Map<String, Flux<SearchResultItem>> groupedMapFlux = cacheGrouped.entrySet().stream()
                                    .collect(Collectors.toMap(
                                            Map.Entry::getKey,
                                            entry -> Flux.fromIterable(entry.getValue())
                                    ));

                            int cacheTotal = searchResultItemList.size();
                            int searchCount = (int) Math.ceil(((double) (count - cacheTotal)) / ((double) searchEngineRunnerMap.size()));

                            if (searchCount > 0) {
                                Collection<SearchIncrement> searchIncrements = searchEngineRunnerMap.entrySet().stream()
                                        .map(entry -> new SearchIncrement(
                                                entry.getValue(),
                                                cacheGrouped.get(entry.getKey()).size(),
                                                searchCount
                                        ))
                                        .collect(Collectors.toList());

                                Flux<Map.Entry<String, Flux<SearchResultItem>>> searchGrouped = getSearchResultFromSourceGrouped(data, searchIncrements);

                                searchGrouped.doFinally(signalType -> {
                                    groupedMapFlux.entrySet().forEach(masterEmitter::next);
                                    masterEmitter.complete();
                                }).subscribe(searchResultsEntry -> {
                                    groupedMapFlux.compute(searchResultsEntry.getKey(), (k, v) -> v == null
                                            ? searchResultsEntry.getValue()
                                            : v.concatWith(searchResultsEntry.getValue()).distinct(SearchResultItem::sourceHashCode));
                                });
                            } else {
                                groupedMapFlux.entrySet().forEach(masterEmitter::next);
                                masterEmitter.complete();
                            }
                        });
            } catch (Exception e) {
                masterEmitter.complete();
            }
        });

        return searchResults.share();
    }

    public Flux<Map.Entry<String, Flux<SearchResultItem>>> getSearchResultFromCacheThenSourceGrouped(String keyword, int count) {
        Flux<Map.Entry<String, Flux<SearchResultItem>>> searchResults = Flux.create(masterEmitter -> {
            try {
                getSearchResultFromCache(keyword, count)
                        .collectList()
                        .subscribe(searchResultItemList -> {
                            Map<String, List<SearchResultItem>> cacheGrouped = searchEngineRunnerMap.entrySet().stream()
                                    .collect(Collectors.toMap(
                                            Map.Entry::getKey,
                                            entry -> new ArrayList<>()
                                    ));

                            searchResultItemList.forEach(searchResultItem -> {
                                cacheGrouped.compute(searchResultItem.getSearchEngine(), (k, v) -> {
                                    if (v == null) {
                                        v = new ArrayList<>();
                                    }
                                    v.add(searchResultItem);
                                    return v;
                                });
                            });

                            Map<String, Flux<SearchResultItem>> groupedMapFlux = cacheGrouped.entrySet().stream()
                                    .collect(Collectors.toMap(
                                            Map.Entry::getKey,
                                            entry -> Flux.fromIterable(entry.getValue())
                                    ));

                            int cacheTotal = searchResultItemList.size();
                            int searchCount = (int) Math.ceil(((double) (count - cacheTotal)) / ((double) searchEngineRunnerMap.size()));

                            if (searchCount > 0) {
                                Collection<SearchIncrement> searchIncrements = searchEngineRunnerMap.entrySet().stream()
                                        .map(entry -> new SearchIncrement(
                                                entry.getValue(),
                                                cacheGrouped.get(entry.getKey()).size(),
                                                searchCount
                                        ))
                                        .collect(Collectors.toList());

                                Flux<Map.Entry<String, Flux<SearchResultItem>>> searchGrouped = getSearchResultFromSourceGrouped(keyword, searchIncrements);

                                searchGrouped.doFinally(signalType -> {
                                    groupedMapFlux.entrySet().forEach(masterEmitter::next);
                                    masterEmitter.complete();
                                }).subscribe(searchResultsEntry -> {
                                    groupedMapFlux.compute(searchResultsEntry.getKey(), (k, v) -> v == null
                                            ? searchResultsEntry.getValue()
                                            : v.concatWith(searchResultsEntry.getValue()).distinct(SearchResultItem::sourceHashCode));
                                });
                            } else {
                                groupedMapFlux.entrySet().forEach(masterEmitter::next);
                                masterEmitter.complete();
                            }
                        });
            } catch (Exception e) {
                masterEmitter.complete();
            }
        });

        return searchResults.share();
    }

    public SearchResultMongoCacher getSearchResultCacher() {
        return cacher;
    }

    public void setSearchResultCacher(SearchResultMongoCacher searchResultCache) {
        this.cacher = searchResultCache;
    }

    public SearchEngineRunner getSearchEngineRunner(String ID) {
        return searchEngineRunnerMap.get(ID);
    }

    public Map<String, SearchEngineRunner> getAllSearchEngineRunner() {
        return searchEngineRunnerMap;
    }

    public void addSearchEngineScrapper(SearchEngineRunner searchEngineRunner) {
        searchEngineRunnerMap.put(searchEngineRunner.getSearchEngine().getID(), searchEngineRunner);
    }

    public int getCorpusCount() {
        return corpusCount;
    }

    public void setCorpusCount(int corpusCount) {
        this.corpusCount = corpusCount;
    }

    public MultipleFluxSyncChainer<SearchResultItem> getSyncChainer() {
        return syncChainer;
    }

    public void setSyncChainer(MultipleFluxSyncChainer<SearchResultItem> syncChainer) {
        this.syncChainer = syncChainer;
    }
}
