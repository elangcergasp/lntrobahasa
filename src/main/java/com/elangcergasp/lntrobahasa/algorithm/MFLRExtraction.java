/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermScoreMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Elang Cergas Pembrani
 */
public class MFLRExtraction implements LNTROExtraction {

    private static final Logger LOGGER = LoggerFactory.getLogger(MFLRExtraction.class);

    @Override
    public String getDescription() {
        return "[MFLR] Most Frequent Local Recommendation";
    }

    public TermScoreMap filterOnlyMax(TermScoreMap scores) {
        TermScoreMap result = new TermScoreMap(scores);
        if (scores.size() > 0) {
            double mapMax = scores.max();
            result.forEach((k, v) -> {
                if (v < mapMax) {
                    result.remove(k);
                }
            });
        }
        return result;
    }

    public Mono<TermScoreMap> countTermFrequence(Flux<TermScoreMap> scores) {
        return scores.reduce(new TermScoreMap(), (a, b) -> {
            b.termsSet().forEach(t -> a.put(t, 1d));
            return a;
        });
    }

    @Override
    public Mono<TermScoreMap> getFinalScores(Flux<TermFrequencyMap> tfmList) {
        Flux<TermScoreMap> scores = tfmList
                .parallel()
                .map(TermFrequencyMap::getAllPercentageScores)
                .sequential();

        Flux<TermScoreMap> onlyMax = scores
                .parallel()
                .map(this::filterOnlyMax)
                .sequential();

        return countTermFrequence(onlyMax);
    }

    @Override
    public Mono<String> getExtractionTerm(Flux<TermFrequencyMap> tfmList) {
        return getExtractionTerm(getFinalScores(tfmList));
    }

    @Override
    public Mono<String> getExtractionTerm(Mono<TermScoreMap> tm) {
        return tm.flatMap(t -> Mono.just(t.maxKey()));
    }

    @Override
    public String getExtractionTerm(TermScoreMap tm) {
        return tm.maxKey();
    }

    @Override
    public Mono<Double> getExtractionScore(Flux<TermFrequencyMap> tfmList) {
        return getExtractionScore(getFinalScores(tfmList));
    }

    @Override
    public Mono<Double> getExtractionScore(Mono<TermScoreMap> tm) {
        return tm.flatMap(t -> Mono.just(t.max()));
    }

    @Override
    public double getExtractionScore(TermScoreMap tm) {
        return tm.max();
    }

}
