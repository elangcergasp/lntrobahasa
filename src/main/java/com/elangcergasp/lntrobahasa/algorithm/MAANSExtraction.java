/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermScoreMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Elang Cergas Pembrani
 */
public class MAANSExtraction implements LNTROExtraction {

    private static final Logger LOGGER = LoggerFactory.getLogger(MAANSExtraction.class);

    public static double SCORE_THRESHOLD = 0.1;

    @Override
    public String getDescription() {
        return "[MAANS] Maximum of Absolute Average of Normalized Scores";
    }

    public Mono<TermScoreMap> sumScores(Flux<TermScoreMap> scores) {
        return scores.reduce(new TermScoreMap(), (a, b) -> TermScoreMap.join(a, b));
    }

    public Mono<TermScoreMap> averageScores(Flux<TermScoreMap> scores) {
        return Mono.create(emitter -> {
            scores.collectList().doFinally(signalType -> emitter.success()).subscribe(l -> {
                double length = l.size();
                sumScores(Flux.fromIterable(l)).doFinally(signalType -> emitter.success()).subscribe(tfm -> {
                    TermScoreMap result = new TermScoreMap(tfm);
                    result.forEach((k, v) -> {
                        result.compute(k, (kk, vv) -> vv / length);
                    });
                    emitter.success(result);
                });
            });
        });
    }

    public TermScoreMap filterScoreThreshold(TermScoreMap scores, double threshold) {
        TermScoreMap result = new TermScoreMap(scores);
        result.forEach((k, v) -> {
            if (v < threshold) {
                result.remove(k);
            }
        });
        return result;
    }

    @Override
    public Mono<TermScoreMap> getFinalScores(Flux<TermFrequencyMap> tfmList) {
        Flux<TermScoreMap> scores = tfmList
                .parallel()
                .map(TermFrequencyMap::getAllNormalizedScores)
                .sequential();

        Flux<TermScoreMap> thresholded = scores
                .parallel()
                .map(m -> filterScoreThreshold(m, SCORE_THRESHOLD))
                .sequential();

        return averageScores(scores);
    }

    @Override
    public Mono<String> getExtractionTerm(Flux<TermFrequencyMap> tfmList) {
        return getExtractionTerm(getFinalScores(tfmList));
    }

    @Override
    public Mono<String> getExtractionTerm(Mono<TermScoreMap> tm) {
        return tm.flatMap(t -> Mono.just(t.maxKey()));
    }

    @Override
    public String getExtractionTerm(TermScoreMap tm) {
        return tm.maxKey();
    }

    @Override
    public Mono<Double> getExtractionScore(Flux<TermFrequencyMap> tfmList) {
        return getExtractionScore(getFinalScores(tfmList));
    }

    @Override
    public Mono<Double> getExtractionScore(Mono<TermScoreMap> tm) {
        return tm.flatMap(t -> Mono.just(t.max()));
    }

    @Override
    public double getExtractionScore(TermScoreMap tm) {
        return tm.max();
    }

}
