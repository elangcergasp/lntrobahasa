/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface Preprocessor {
    
    public static final Set<String> PUNCTUATIONS = Stream.of(
        "\\[", "\\\\", "\\]", "\\^", "\\_", "\\`", "\\{", "\\|", "\\}", "\\~", "\\+", 
        // "\\,", "\\-", "\\.", "\\/", "\\:", "\\;", "\\<", "\\=", "\\>", "\\?", "\\@",
        "\\,", "\\.", "\\/", "\\:", "\\;", "\\<", "\\=", "\\>", "\\?", "\\@",
        "\\!", "\"", "\\#", "\\$", "\\%", "\\&", "\\'", "\\(", "\\)", "\\*"
    ).collect(Collectors.toSet());
    
    public String preprocess(String text, List<Function<String, String>> pipeline);
    public Flux<String> preprocess(Flux<String> text, List<Function<String, String>> pipeline);
    
    public Flux<String> sentenceSplit(String text);
    
    public Flux<String> tokenize(String text);
    
    public String caseFold(String text);
    public Flux<String> caseFold(Flux<String> text);
    
    public String removePattern(String regex, String text);
    public Flux<String> removePattern(String regex, Flux<String> text);
    
    public void setStopwords(Set<String> stopwords);
    public String removeStopwords(String text);
    public Flux<String> removeStopwords(Flux<String> text);
    public String removeStopwords(Set<String> stopwords, String text);
    public Flux<String> removeStopwords(Set<String> stopwords, Flux<String> text);
    
    public String removePunctuationToken(String text);
    public Flux<String> removePunctuationToken(Flux<String> text);
    
    public String removeNumberToken(String text);
    public Flux<String> removeNumberToken(Flux<String> text);
    
    public String removeExtraWhitespace(String text);
    public Flux<String> removeExtraWhitespace(Flux<String> text);

    public String removeAnyToken(Set<String> set, String text);
    public Flux<String> removeAnyToken(Set<String> set, Flux<String> text);

    public String removeWordToken(Set<String> set, String text);
    public Flux<String> removeWordToken(Set<String> set, Flux<String> text);

    public String removeTokenContaining(Set<String> set, String text);
    public Flux<String> removeTokenContaining(Set<String> set, Flux<String> text);
    
    public Flux<String> filterHighlight(List<String> mustContained, String text);
    public Flux<Flux<String>> filterHighlight(List<String> mustContained, Flux<String> text);
    
    public <T> Collection<T> flattenList(Collection<Collection<T>> corpus);
    public <T> Flux<T> flattenFlux(Flux<Flux<T>> corpus);
}
