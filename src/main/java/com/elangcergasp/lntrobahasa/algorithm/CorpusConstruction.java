/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import reactor.core.publisher.Flux;

import java.util.Collection;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface CorpusConstruction {
    
    public static final int CORPUS_DEFAULT_COUNT = 100;

    public static final String FROM_CACHE        = "CACHE";
    public static final String FROM_SOURCE       = "SOURCE";
    public static final String FROM_CACHE_SOURCE = "CACHE_SOURCE";

    public Flux<CorpusItem> getCorpus(Collection<String> data, int count);
    public Flux<CorpusItem> getCorpus(Collection<String> data);
    public Flux<CorpusItem> getCorpus(String keyword, int count);
    public Flux<CorpusItem> getCorpus(String keyword);


    public Flux<CorpusItem> getCorpusFromCache(Collection<String> data, int count);
    public Flux<CorpusItem> getCorpusFromCache(Collection<String> data);
    public Flux<CorpusItem> getCorpusFromCache(String keyword, int count);
    public Flux<CorpusItem> getCorpusFromCache(String keyword);

    public Flux<CorpusItem> getCorpusFromSource(Collection<String> data, int count);
    public Flux<CorpusItem> getCorpusFromSource(Collection<String> data);
    public Flux<CorpusItem> getCorpusFromSource(String keyword, int count);
    public Flux<CorpusItem> getCorpusFromSource(String keyword);

    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Document
    public class CorpusItem {

        private String searchEngine;
        private String content;
    }
}
