/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.EvaluationOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaActivePassiveForm;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface LNTROEvaluation {

    public Mono<EvaluationOutput> evaluate(Flux<List<String>> data, List<BahasaActivePassiveForm> terms);
    
}
