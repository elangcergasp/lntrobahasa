package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.*;
import com.elangcergasp.lntrobahasa.model.DocumentKBBI;
import com.elangcergasp.lntrobahasa.services.cacher.KBBIMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.MyKBBI;
import jregex.MatchIterator;
import jregex.MatchResult;
import jregex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BahasaStemmer {

    private static final Logger LOGGER = LoggerFactory.getLogger(BahasaStemmer.class);

    public static final List<String> HURUF_VOKAL_1    = Stream.of("aeiou".split("")).collect(Collectors.toList());
    public static final List<String> HURUF_VOKAL_2    = Stream.of("ai", "au", "oi").collect(Collectors.toList());
    public static final List<String> HURUF_DIFTONG    = HURUF_VOKAL_2;
    public static final List<String> HURUF_KONSONAN_1 = Stream.of("bcdfghjklmnpqrstvwxyz".split("")).collect(Collectors.toList());
    public static final List<String> HURUF_KONSONAN_2 = Stream.of("kh", "ng", "ny", "sy").collect(Collectors.toList());
    public static final List<String> HURUF_VOKAL      = Stream.concat(HURUF_VOKAL_2.stream(), HURUF_VOKAL_1.stream()).collect(Collectors.toList());
    public static final List<String> HURUF_KONSONAN   = Stream.concat(HURUF_KONSONAN_2.stream(), HURUF_KONSONAN_1.stream()).collect(Collectors.toList());
    public static final List<String> HURUF_ALL        = Stream.concat(HURUF_VOKAL.stream(), HURUF_KONSONAN.stream()).collect(Collectors.toList());

    public static final String PREFIKS_BE = "be";
    public static final String PREFIKS_DI = "di";
    public static final String PREFIKS_KE = "ke";
    public static final String PREFIKS_ME = "me";
    public static final String PREFIKS_PE = "pe";
    public static final String PREFIKS_SE = "se";
    public static final String PREFIKS_TE = "te";

    public static final String SUFFIKS_I   = "i";
    public static final String SUFFIKS_AN  = "an";
    public static final String SUFFIKS_KAN = "kan";

    public static final String KONFIKS_BE_AN  = PREFIKS_BE + "--" + SUFFIKS_AN;
    public static final String KONFIKS_BE_KAN = PREFIKS_BE + "--" + SUFFIKS_KAN;
    public static final String KONFIKS_DI_I   = PREFIKS_DI + "--" + SUFFIKS_I;
    public static final String KONFIKS_DI_KAN = PREFIKS_DI + "--" + SUFFIKS_KAN;
    public static final String KONFIKS_ME_I   = PREFIKS_ME + "--" + SUFFIKS_I;
    public static final String KONFIKS_ME_KAN = PREFIKS_ME + "--" + SUFFIKS_KAN;
    public static final String KONFIKS_TE_I   = PREFIKS_TE + "--" + SUFFIKS_I;
    public static final String KONFIKS_TE_KAN = PREFIKS_TE + "--" + SUFFIKS_KAN;
    public static final String KONFIKS_KE_AN  = PREFIKS_KE + "--" + SUFFIKS_AN;
    public static final String KONFIKS_SE_AN  = PREFIKS_SE + "--" + SUFFIKS_AN;
    public static final String KONFIKS_PE_AN  = PREFIKS_PE + "--" + SUFFIKS_AN;

    private static final Pattern SPLIT_LETTERS_REGEX = new Pattern("({letter}(?:au|ai|oi|kh|ng|ny|sy|\\w))");

    private static final Pattern SPLIT_SYLLABLE_REGEX = new Pattern("" +
            "({syllable}(?>\n" +
            "      (?:\\W)\n" +
            "    | (?:(?<=\\wai|\\wau|\\woi)(?>au|ai|oi|[aiueo]))\n" +
            "    | (?:(?<=\\w[iue])(?>au|ai|oi|[aiueo]))\n" +
            "    | (?:(?<=\\wa)[aeo])\n" +
            "    | (?:(?<=\\wo)[aueo])\n" +
            "    | (?:(?<=[aiueo])(?>kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?=au|ai|oi|[aiueo]))\n" +
            "    | (?:(?<=\\w[^aiueonks\\s])(?>kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])+)\n" +
            "    | (?:(?<=\\w(?:ng|ny|kh|sy))[^aiueo\\s]+)\n" +
            "    | (?:(?<=\\wn)[^aiueogy\\s]+)\n" +
            "    | (?:(?<=\\wk)[^aiueoh\\s]+)\n" +
            "    | (?:(?<=\\ws)[^aiueoy\\s]+)\n" +
            "))", Pattern.IGNORE_CASE | Pattern.IGNORE_SPACES | Pattern.MULTILINE | Pattern.UNICODE);

    private static final Pattern STEMMING_REGEX = new Pattern("" +
            "^({all}\n" +
            "    ({prefiks_1}\n" +
            "          ({pre1_se} (?:se))\n" +
            "        | ({pre1_di} (?:di))\n" +
            "        | ({pre1_ke} (?:ke))\n" +
            "        | ({pre1_te} (?:te(?:r|)))\n" +
            "        | ({pre1_be} (?:be(?:r|)))\n" +
            "        | ({pre1_me} (?:me(?>m(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|ng(?=[gh]|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|n(?!(?:[aiueo])|[gy])|)))\n" +
            "        | ({pre1_pe} (?:pe(?>m(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|ng(?=[gh]|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|n(?!(?:[aiueo])|[gy])|r|)))\n" +
            "        |\n" +
            "    )\n" +
            "    ({prefiks_2}\n" +
            "          (?(pre1_ke) (?:(?:te(?:r|))|(?:be(?:r|))(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|)\n" +
            "        | (?(pre1_be) (?:({pre2_ke_1}(?:ke))|(?:pe(?>m(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|ng(?=[gh]|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|n(?!(?:[aiueo])|[gy])|r|))|)\n" +
            "        | (?(pre1_pe) (?:({pre2_ke_2}(?:ke))|(?:be(?:r|))(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|)\n" +
            "        | (?(pre1_me) (?:({pre2_ke_3}(?:ke))|(?:pe(?>m(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|ng(?=[gh]|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|n(?!(?:[aiueo])|[gy])|r|))|(?:be(?:r|))(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|)\n" +
            "        | (?(pre1_di) (?:({pre2_ke_4}(?:ke))|(?:pe(?>m(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|ng(?=[gh]|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|n(?!(?:[aiueo])|[gy])|r|))|(?:be(?:r|))(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|)\n" +
            "        | (?(pre1_te) (?:({pre2_ke_5}(?:ke))|(?:pe(?>m(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|ng(?=[gh]|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|n(?!(?:[aiueo])|[gy])|r|))|(?:be(?:r|))(?=(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz]))|)\n" +
            "        |\n" +
            "        ))))))\n" +
            "    )\n" +
            "    ({kata_dasar}\n" +
            "            (?:(?:[aiueo])(?:kh|ng|ny|sy|\\w){2,}?|(?<=nge)(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?:[aiueo])(?:kh|ng|ny|sy|\\w)|(?<=\\W)(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?:[aiueo])(?:kh|ng|ny|sy|\\w)|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?:kh|ng|ny|sy|\\w){3,}?)\n" +
            "        (?:(?:\\-(?:(?:[aiueo])(?:kh|ng|ny|sy|\\w){2,}?|(?<=nge)(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?:[aiueo])(?:kh|ng|ny|sy|\\w)|(?<=\\W)(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?:[aiueo])(?:kh|ng|ny|sy|\\w)|(?:kh|ng|ny|sy|[bcdfghjklmnpqrstvwxyz])(?:kh|ng|ny|sy|\\w){3,}?))*)\n" +
            "    )\n" +
            "    ({suffiks_1}\n" +
            "          isasi\n" +
            "        | isme\n" +
            "        | ({suf1_an}\n" +
            "              (?(pre1_ke)   an\n" +
            "            | (?(pre2_ke_1) an\n" +
            "            | (?(pre2_ke_2) an\n" +
            "            | (?(pre2_ke_3) an\n" +
            "            | (?(pre2_ke_4) an\n" +
            "            | (?(pre2_ke_5) an\n" +
            "            | (?:(?(pre1_me)|(?(pre1_di)|(?(pre1_te)|an)))|(?(pre1_se)|(?(pre1_pe)|(?(pre1_ke)|kan)))|)\n" +
            "            ))))))\n" +
            "        )\n" +
            "    )\n" +
            "    ({suffiks_2}\n" +
            "          (?(pre1_se)|(?(pre1_pe)|(?(pre1_ke)|kan)))\n" +
            "        | (?(pre1_be)|(?(pre1_ke)|(?(pre1_se)|(?(pre1_pe)|i))))\n" +
            "        | (?(suf1_an)|(?(pre1_me)|(?(pre1_di)|(?(pre1_te)|an))))\n" +
            "        |\n" +
            "    )\n" +
            "    ({posesif}\n" +
            "          ku\n" +
            "        | mu\n" +
            "        | nya\n" +
            "        |\n" +
            "    )\n" +
            "    ({partikel}\n" +
            "          lah\n" +
            "        | kah\n" +
            "        | tah\n" +
            "        | pun\n" +
            "        |\n" +
            "    )\n" +
            ")$", Pattern.IGNORE_CASE | Pattern.IGNORE_SPACES | Pattern.MULTILINE | Pattern.UNICODE);

    @Autowired
    private KBBIMongoCacher KBBI;

    public KBBIMongoCacher getKBBI() {
        return KBBI;
    }

    public void setKBBI(KBBIMongoCacher KBBI) {
        this.KBBI = KBBI;
    }

    public BahasaWordTokens splitLetters(String word) {
        word = word.toLowerCase();
        MatchIterator matchIterator = SPLIT_LETTERS_REGEX.matcher(word.trim()).findAll();
        List<String> letters = new ArrayList<>();
        while (matchIterator.hasMore()) {
            MatchResult matchResult = matchIterator.nextMatch();

            StringBuffer letter = new StringBuffer();
            matchResult.getGroup("letter", letter);
            letters.add(letter.toString());
        }
        return new BahasaWordTokens(word, letters);
    }

    public boolean isVokal(String letter) {
        return HURUF_VOKAL.contains(letter);
    }

    public boolean isKonsonan(String letter) {
        return HURUF_KONSONAN.contains(letter);
    }

    public BahasaStemStructure stemStructure(String word) {
        word = word.toLowerCase().trim();
        MatchIterator matchIterator = STEMMING_REGEX.matcher(word).findAll();
        while (matchIterator.hasMore()) {
            MatchResult matchResult = matchIterator.nextMatch();

            StringBuffer bufferPrefix_1 = new StringBuffer();
            matchResult.getGroup("prefiks_1", bufferPrefix_1);

            StringBuffer bufferPrefix_2 = new StringBuffer();
            matchResult.getGroup("prefiks_2", bufferPrefix_2);

            StringBuffer bufferStem = new StringBuffer();
            matchResult.getGroup("kata_dasar", bufferStem);

            StringBuffer bufferSuffix_1 = new StringBuffer();
            matchResult.getGroup("suffiks_1", bufferSuffix_1);

            StringBuffer bufferSuffix_2 = new StringBuffer();
            matchResult.getGroup("suffiks_2", bufferSuffix_2);

            StringBuffer bufferPossessive = new StringBuffer();
            matchResult.getGroup("posesif", bufferPossessive);

            StringBuffer bufferParticle = new StringBuffer();
            matchResult.getGroup("partikel", bufferParticle);

            String prefix_1   = bufferPrefix_1.toString();
            String prefix_2   = bufferPrefix_2.toString();
            String stem       = bufferStem.toString();
            String suffix_1   = bufferSuffix_1.toString();
            String suffix_2   = bufferSuffix_2.toString();
            String possessive = bufferPossessive.toString();
            String particle   = bufferParticle.toString();

            BahasaWordTokens stemSyllables = splitSyllables(bufferStem.toString());
            if (stemSyllables.getTokens().size() == 1) {
                if (prefix_1.length() > 0) {
                    stem = prefix_1 + stem;
                    prefix_1 = "";
                } else if (prefix_2.length() > 0) {
                    stem = prefix_2 + stem;
                    prefix_2 = "";
                } else if (suffix_1.length() > 0) {
                    stem = stem + suffix_1;
                    suffix_1 = "";
                } else if (suffix_2.length() > 0) {
                    stem = stem + suffix_2;
                    suffix_2 = "";
                }
            }

            return new BahasaStemStructure(
                    word,
                    prefix_1,
                    prefix_2,
                    stem,
                    suffix_1,
                    suffix_2,
                    possessive,
                    particle
            );
        }
        return null;
    }

    public BahasaWordTokens splitSyllables(String word) {
        word = word.toLowerCase();
        List<String> syllables = Stream.of(
                SPLIT_SYLLABLE_REGEX.replacer("|$&")
                        .replace(word.trim())
                        .split("\\|"))
                .collect(Collectors.toList());
        return new BahasaWordTokens(word, syllables);
    }

    public List<String> getStemCandidates(String word) {
        word = word.toLowerCase();
        BahasaWordTokens syllables = splitSyllables(word);

        String firstSyllable = syllables.getTokens().get(0);
        List<String> firstSyllableLetters = splitLetters(firstSyllable).getTokens();

        List<String> remainingSyllables = new ArrayList<>(syllables.getTokens());
        if (remainingSyllables.size() > 0) {
            remainingSyllables.remove(0);
        }
        String remainingSyllablesString = remainingSyllables.stream().reduce("", String::concat);

        String firstChar = firstSyllableLetters.get(0);
        String secondChar = firstSyllableLetters.size() > 1 ? firstSyllableLetters.get(1) : "";

        List<String> stemCandidates = new ArrayList<>();
        stemCandidates.add(String.join("", firstSyllableLetters));
        if (firstChar.equals("ng")) {
            firstSyllableLetters.set(0, "k");
            stemCandidates.add(String.join("", firstSyllableLetters));

            firstSyllableLetters.set(0, "");
            stemCandidates.add(String.join("", firstSyllableLetters));

            if (secondChar.equals("e")) {
                firstSyllableLetters.set(1, "");
                stemCandidates.add(String.join("", firstSyllableLetters));
            }
        } else if (firstChar.equals("ny")) {
            firstSyllableLetters.set(0, "s");
            stemCandidates.add(String.join("", firstSyllableLetters));
        } else if (firstChar.equals("n")) {
            firstSyllableLetters.set(0, "t");
            stemCandidates.add(String.join("", firstSyllableLetters));
        } else if (firstChar.equals("m")) {
            firstSyllableLetters.set(0, "p");
            stemCandidates.add(String.join("", firstSyllableLetters));
        } else if (firstChar.equals("r")) {
            firstSyllableLetters.set(0, "");
            stemCandidates.add(String.join("", firstSyllableLetters));
        } else if (isVokal(firstChar)) {
            firstSyllableLetters.set(0, "r" + firstChar);
            stemCandidates.add(String.join("", firstSyllableLetters));
        }
        stemCandidates = stemCandidates.stream().map(s -> s.concat(remainingSyllablesString)).collect(Collectors.toList());

        return stemCandidates;
    }

    public List<TaggedWord> getCheckedStemCandidate(String word) {
        word = word.toLowerCase();
        List<String> stemCandidates = getStemCandidates(word);

        return stemCandidates.stream()
                .map(s -> new TaggedWord(s, KBBI.getDocumentKBBI(s).block().getPosTag()))
                .filter(taggedWord -> !taggedWord.getPosTag().equals(POSTagSet.UNKNOWN))
                .sorted((s1, s2) -> Integer.compare(s2.getWord().length(), s1.getWord().length()))
                .collect(Collectors.toList());
    }

    public BahasaStemStructure stemFormula(String word) {
        word = word.toLowerCase();

        DocumentKBBI orig = KBBI.getDocumentKBBI(word).block();
        Document originalWordDocument = null;
        if (orig != null) {
            originalWordDocument = Jsoup.parse(orig.getDocumentHTML());
        }
        Elements mainWordElements = KBBI.getKbbiSource().getMainHighlightedElements(originalWordDocument);
        if (mainWordElements != null) {
            Element mainWordElement = mainWordElements.first();
            if (mainWordElement != null) {
                String mainWordDefinition = mainWordElement.text().replaceAll(MyKBBI.INVALID_CHAR, "");
                if (mainWordDefinition.equals(word)) {
                    return new BahasaStemStructure(
                            word,
                            "",
                            "",
                            word,
                            "",
                            "",
                            "",
                            ""
                    );
                }
            }
        }

        BahasaStemStructure baseStructure = stemStructure(word);

        String initialStem = baseStructure.getStem();

        String prefix_1 = baseStructure.getPrefix_1();
        if (prefix_1.length() >= 2) {
            prefix_1 = prefix_1.substring(0, 2);
        }
        String prefix_2 = baseStructure.getPrefix_2();
        if (prefix_2.length() >= 2) {
            prefix_2 = prefix_2.substring(0, 2);
        }

        List<TaggedWord> stemCandidates = getCheckedStemCandidate(initialStem);

        String pickedStemCandidate = stemCandidates.size() > 0 ? stemCandidates.get(0).getWord() : initialStem;

        return new BahasaStemStructure(
                word,
                prefix_1,
                prefix_2,
                pickedStemCandidate,
                baseStructure.getSuffix_1(),
                baseStructure.getSuffix_2(),
                baseStructure.getPossessive(),
                baseStructure.getParticle()
        );
    }

    public String addPrefiks(String stem, String prefix) {
        stem = stem.toLowerCase();
        switch (prefix) {
            case PREFIKS_BE:
            case PREFIKS_TE: {
                List<String> letters = splitLetters(stem).getTokens();
                String firstLetter = letters.get(0);
                return (firstLetter.equals("r")) ? prefix + stem : prefix + "r" + stem;
            }
            case PREFIKS_DI:
                return PREFIKS_DI + stem;
            case PREFIKS_KE:
                return PREFIKS_KE + addSuffiks(stem, SUFFIKS_AN);
            case PREFIKS_SE:
                return PREFIKS_SE + stem;
            case PREFIKS_ME:
            case PREFIKS_PE: {
                List<String> syllables = splitSyllables(stem).getTokens();
                if (syllables.size() == 1) {
                    return prefix + "nge" + stem;
                }

                List<String> letters = splitLetters(stem).getTokens();
                String firstLetter = letters.get(0);
                String secondLetter = letters.get(1);

                if (firstLetter.equals("k")) {
                    if (isVokal(secondLetter)) {
                        letters.set(0, "ng");
                    } else {
                        letters.set(0, "ng" + firstLetter);
                    }
                } else if (firstLetter.equals("p")) {
                    if (isVokal(secondLetter)) {
                        letters.set(0, "m");
                    } else {
                        letters.set(0, "m" + firstLetter);
                    }
                } else if (firstLetter.equals("s")) {
                    if (isVokal(secondLetter)) {
                        letters.set(0, "ny");
                    } else {
                        letters.set(0, "n" + firstLetter);
                    }
                } else if (firstLetter.equals("t")) {
                    if (isVokal(secondLetter)) {
                        letters.set(0, "n");
                    } else {
                        letters.set(0, "n" + firstLetter);
                    }
                } else if (firstLetter.equals("g")
                        || firstLetter.equals("h")
                        || firstLetter.equals("kh")) {
                    letters.set(0, "ng" + firstLetter);
                } else if (firstLetter.equals("b")
                        || firstLetter.equals("f")
                        || firstLetter.equals("v")) {
                    letters.set(0, "m" + firstLetter);
                } else if (firstLetter.equals("c")
                        || firstLetter.equals("d")
                        || firstLetter.equals("j")
                        || firstLetter.equals("z")
                        || firstLetter.equals("sy")) {
                    letters.set(0, "n" + firstLetter);
                } else if (isVokal(firstLetter)) {
                    letters.set(0, "ng" + firstLetter);
                }

                return prefix + String.join("", letters);
            }
            default:
                return prefix + stem;
        }
    }

    public String addSuffiks(String stem, String suffix) {
        stem = stem.toLowerCase();
        switch (suffix) {
            case SUFFIKS_I:
                return stem + SUFFIKS_I;
            case SUFFIKS_AN:
                return stem + SUFFIKS_AN;
            case SUFFIKS_KAN:
                return stem + SUFFIKS_KAN;
            default:
                return stem + suffix;
        }
    }

    public String addKonfiks(String stem, String konfiks) {
        stem = stem.toLowerCase();
        String[] splitKonfiks = konfiks.split("--");
        String word = stem;
        try {
            word = addPrefiks(word, splitKonfiks[0]);
            word = addSuffiks(word, splitKonfiks[1]);
        } catch (Exception e) {}
        return word;
    }

    public BahasaActivePassiveForm getActivePassiveForm(String word) {
        word = word.toLowerCase();
        BahasaStemStructure stemStructure = stemStructure(word);
        BahasaStemStructure stemFormula   = stemFormula(word);

        String defaultEmbeddedStem = stemStructure.getPrefix_1() + stemStructure.getPrefix_2() + stemStructure.getStem() + stemStructure.getSuffix_1() + stemStructure.getSuffix_2();

        String prefix_1 = stemFormula.getPrefix_1();
        String prefix_2 = stemFormula.getPrefix_2();
        String stem     = stemFormula.getStem();
        String suffix_1 = stemFormula.getSuffix_1();
        String suffix_2 = stemFormula.getSuffix_2();

        String activeForm;
        String passiveForm;
        switch (prefix_1) {
            case PREFIKS_ME:
                activeForm = defaultEmbeddedStem;
                passiveForm = stem;
                passiveForm = addPrefiks(passiveForm, prefix_2);
                passiveForm = addPrefiks(passiveForm, PREFIKS_DI);
                passiveForm = addSuffiks(passiveForm, suffix_1);
                passiveForm = addSuffiks(passiveForm, suffix_2);
                break;
            case PREFIKS_DI:
                passiveForm = defaultEmbeddedStem;
                activeForm = stem;
                activeForm = addPrefiks(activeForm, prefix_2);
                activeForm = addPrefiks(activeForm, PREFIKS_ME);
                activeForm = addSuffiks(activeForm, suffix_1);
                activeForm = addSuffiks(activeForm, suffix_2);
                break;
            case PREFIKS_BE:
            case PREFIKS_TE:
                passiveForm = defaultEmbeddedStem;
                activeForm = stem;
                activeForm = addSuffiks(activeForm, suffix_1);
                activeForm = addSuffiks(activeForm, suffix_2);
                break;
            default:
                activeForm = stem;
                activeForm = addPrefiks(activeForm, prefix_2);
                activeForm = addPrefiks(activeForm, PREFIKS_ME);
                activeForm = addSuffiks(activeForm, suffix_1);
                activeForm = addSuffiks(activeForm, suffix_2);
                passiveForm = stem;
                passiveForm = addPrefiks(passiveForm, prefix_2);
                passiveForm = addPrefiks(passiveForm, PREFIKS_DI);
                passiveForm = addSuffiks(passiveForm, suffix_1);
                passiveForm = addSuffiks(passiveForm, suffix_2);
                break;
        }

        return new BahasaActivePassiveForm(word, stem, activeForm, passiveForm);
    }

}
