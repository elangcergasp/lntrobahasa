/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTag;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TaggedWord;
import reactor.core.publisher.Flux;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface CorpusAnnotation {
    
    public Flux<TaggedWord> annotate(String corpus);
    public Flux<Flux<TaggedWord>> annotate(Flux<String> corpus);

    public Flux<String> getWordsByTag(POSTag tag, Flux<TaggedWord> taggedWords);
    public Flux<String> getVerbs(Flux<TaggedWord> taggedWords);
    
}
