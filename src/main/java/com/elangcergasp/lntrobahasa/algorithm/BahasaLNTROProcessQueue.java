package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROAlgorithmParams;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROBlockOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcess;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcessOutput;
import com.elangcergasp.lntrobahasa.repository.LNTROProcessRepository;
import com.elangcergasp.lntrobahasa.services.cacher.SearchResultMongoCacher;
import com.elangcergasp.lntrobahasa.services.cacher.SearchStatMongoCacher;
import com.elangcergasp.lntrobahasa.util.OpenMono;
import org.apache.jena.ontology.OntModel;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ReplayProcessor;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Service
public class BahasaLNTROProcessQueue {

    private static final Logger LOGGER = LoggerFactory.getLogger(BahasaLNTROProcessQueue.class);

    @Autowired
    private BahasaLNTROAlgorithmFactory bahasaLntroAlgorithmFactory;

    @Autowired
    private LNTROResultToOWLGenerator owlGeneratorService;

    @Autowired
    private LNTROProcessRepository lntroProcessRepository;

    @Autowired
    private SearchResultMongoCacher searchResultMongoCacher;

    @Autowired
    private SearchStatMongoCacher searchStatMongoCacher;

    public static Duration PROCESSING_INTERVAL = Duration.ofMillis(1000);
    private Disposable processingTick = Flux.interval(PROCESSING_INTERVAL)
            .onBackpressureDrop()
            .publishOn(Schedulers.single())
            .subscribe(a -> startProcessingQueue());

    private boolean isProcessing = false;
    private Queue<Map.Entry<String, LNTROProcess>> processQueue = new ConcurrentLinkedQueue<>();
    private Map.Entry<String, LNTROProcess> currentProcessingItem = null;
    private Map<String, ReplayProcessor<Boolean>> processDoneEmitterMap = new ConcurrentHashMap<>();

    public boolean isInQueue(String lntroProcessID) {
        return processQueue.stream()
                .map(entry -> entry.getKey().equals(lntroProcessID))
                .reduce(false, (a, b) -> a || b);
    }

    public synchronized Mono<LNTROProcess> doProcess(LNTROProcess lntroProcess) {
        String processID = lntroProcess.getId();
        if (!isInQueue(processID)) {
            processQueue.add(new AbstractMap.SimpleEntry<>(processID, lntroProcess));
            processDoneEmitterMap.put(processID, ReplayProcessor.create());
            LOGGER.debug("Added Process ID [" + processID + "] to Queue - Queue Size is " + processQueue.size());
        }

        return getProcess(processID);
    }

    public Mono<LNTROProcess> getProcess(String processID) {
        try {
            return processDoneEmitterMap.get(processID).collectList()
                    .flatMap(a -> {
                        processDoneEmitterMap.remove(processID);
                        return lntroProcessRepository.findById(processID);
                    });
        } catch (Exception e) {
            return Mono.empty();
        }
    }

    public void startProcessingQueue() {
        if (!isProcessing) {
            if (!processQueue.isEmpty()) {
                isProcessing = true;
                LOGGER.debug("Start Processing Queue - Queue Size is " + processQueue.size());
                while (!processQueue.isEmpty() && isProcessing) {
                    try {
                        Map.Entry<String, LNTROProcess> processItem = processQueue.remove();
                        String processID = processItem.getKey();
                        LNTROProcess lntroProcess = processItem.getValue();

                        currentProcessingItem = processItem;
                        LOGGER.debug("Queue ID [" + processID + "] Start");

                        LNTROProcess finalProcess;
                        try {
                            finalProcess = doAlgorithm(lntroProcess);
                        } catch (Exception e) {
                            LOGGER.error("LNTRO Algorithm error : " + e.getMessage());
                        }

                        ReplayProcessor replayProcessor = processDoneEmitterMap.get(processID);
                        replayProcessor.onNext(true);
                        replayProcessor.onComplete();

                        try {
                            // Clean up
                            searchStatMongoCacher.getSearchStatRepository().deleteByCount(null).subscribe();
                            searchResultMongoCacher.getSearchResultRepository().deleteDuplicatesByUrlAndMeta().subscribe();
                        } catch (Exception e) {
                        }

                        currentProcessingItem = null;
                        LOGGER.debug("Queue ID [" + processID + "] Done - Queue Size now is " + processQueue.size());
                    } catch (Exception e) {
                        LOGGER.error("Processing Queue encounter error : " + e.getMessage());
                    }
                }

                LOGGER.debug("Done Processing Queue - Queue should be empty");
                isProcessing = false;
            }
        }
    }

    public LNTROProcess doAlgorithm(LNTROProcess lntroProcess) {
        LNTROAlgorithmParams lntroAlgorithmParams = lntroProcess.getAlgorithmParams();
        String corpusMethod = lntroAlgorithmParams.getCorpusConstructionMethod();
        List<String> evaluationSearchEngineIDList = lntroAlgorithmParams.getEvaluationSearchEngines();

        LNTROBlockOutput oldOutput = lntroProcess.getOutput();
        Flux<List<String>> data = Flux.fromIterable(oldOutput.getData());
        List<String> columnNames = oldOutput.getColumns();

        LNTROAlgorithm algorithm = bahasaLntroAlgorithmFactory.buildLNTROAlgorithm(lntroAlgorithmParams);

        LNTROProcessOutput output1 = algorithm.constructData(data);
        LNTROProcessOutput output2 = corpusMethod.equals(CorpusConstruction.FROM_SOURCE) ? algorithm.constructCorpusSearched(output1)
                : ((corpusMethod.equals(CorpusConstruction.FROM_CACHE) ? algorithm.constructCorpusCached(output1)
                : algorithm.constructCorpus(output1)));
        LNTROProcessOutput output3 = algorithm.annotateCorpus(output2);
        LNTROProcessOutput output4 = algorithm.extract(output3);
        LNTROProcessOutput finalOutput = evaluationSearchEngineIDList.size() > 0 ? algorithm.evaluate(output4) : output4;

        finalOutput.setColumns(columnNames);

        LNTROProcess finalProcess = new LNTROProcess();
        finalProcess.setFetchDate(lntroProcess.getFetchDate());
        finalProcess.setId(lntroProcess.getId());
        finalProcess.setTitle(lntroProcess.getTitle());
        finalProcess.setDescription(lntroProcess.getDescription());
        finalProcess.setAlgorithmParams(lntroAlgorithmParams);
        finalProcess.setOutput(finalOutput.block());

        OntModel ontologyModel = owlGeneratorService.createOntologyModel(
                finalProcess.getId(),
                finalProcess.getOutput(),
                columnNames.get(0),
                columnNames.get(1)
        );
        String owlFilename = owlGeneratorService.saveOntology(ontologyModel, finalProcess.getTitle());
        finalProcess.setOntologyOWLFile(owlFilename);
        LOGGER.debug("Ontology File : " + owlFilename);

        Mono<LNTROProcess> result = lntroProcessRepository.save(finalProcess);
        result.doOnError(throwable -> {
            LOGGER.error("LNTRO Process not saved : " + throwable.getMessage());
        }).subscribe(p -> {
            LOGGER.debug("Saved LNTRO Process : " + p.getId());
        });

        return finalProcess;
    }

    public void stopProcessing() {
        isProcessing = false;
    }

    public boolean isProcessing() {
        return isProcessing;
    }

    public Disposable getProcessingTick() {
        return processingTick;
    }

    public Queue<Map.Entry<String, LNTROProcess>> getProcessQueueImmutable() {
        return new ConcurrentLinkedQueue<>(new ArrayList<>(processQueue));
    }

    public Map.Entry<String, LNTROProcess> getCurrentProcessingItem() {
        return currentProcessingItem;
    }

    public void setCurrentProcessingItem(Map.Entry<String, LNTROProcess> currentProcessingItem) {
        this.currentProcessingItem = currentProcessingItem;
    }
}
