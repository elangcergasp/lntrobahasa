package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROBlockOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcessOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaActivePassiveForm;
import com.elangcergasp.lntrobahasa.properties.StorageProperties;
import com.elangcergasp.lntrobahasa.services.storage.StorageService;
import org.apache.commons.text.WordUtils;
import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class LNTROResultToOWLGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(LNTROResultToOWLGenerator.class);

    public static String SOURCE_URL = "http://www.example.com/ontology/";

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageProperties storageProperties;

    public String normalizeClassName(String ontologyClassName) {
        String className = ontologyClassName
                .replaceAll("[\\-_]", " ")
                .replaceAll("[^\\w\\s]", "");
        className = WordUtils.capitalizeFully(className);
        className = className.replaceAll("\\s", "_");
        return className;
    }

    public String normalizeIndividualName(String ontologyIndividualName) {
        String individualName = ontologyIndividualName
                .replaceAll("[\\-_]", " ")
                .replaceAll("[^\\w\\s]", "");
        individualName = individualName.toLowerCase();
        individualName = individualName.replaceAll("\\s", "_");
        return individualName;
    }

    public OntModel createOntologyModel(String id, LNTROProcessOutput output, String firstClassName, String secondClassName) {
        return createOntologyModel(id, output.block(), firstClassName, secondClassName);
    }

    public OntModel createOntologyModel(String id, LNTROBlockOutput output, String firstClassName, String secondClassName) {
        return createOntologyModel(id, output.getData(), firstClassName, secondClassName, output.getLntroTermResult());
    }

    public OntModel createOntologyModel(String id, List<List<String>> data, String firstClassName, String secondClassName, BahasaActivePassiveForm relationshipName) {
        LOGGER.debug("Creating Ontology Model");
        String namespace = SOURCE_URL + id + "#";
//        PrintUtil.registerPrefix(id, namespace);
        OntModel model = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
//        model.setNsPrefix("", namespace);

        LOGGER.debug("Creating Ontology Class");
        OntClass firstClass = model.createClass(namespace + normalizeClassName(firstClassName));
        LOGGER.debug("Created Ontology Class 1 : "+firstClass.getLocalName());
        OntClass secondClass = model.createClass(namespace + normalizeClassName(secondClassName));
        LOGGER.debug("Created Ontology Class 2 : "+secondClass.getLocalName());

        String mainClassRelationshipName = relationshipName.getWord();
        ObjectProperty mainClassRelationship = model.createObjectProperty(namespace + mainClassRelationshipName);
        LOGGER.debug("Created Ontology Relationship Main : "+mainClassRelationship.getLocalName());
        mainClassRelationship.addDomain(firstClass);
        mainClassRelationship.addRange(secondClass);

        String inverseClassRelationshipName = relationshipName.getWord().equals(relationshipName.getActiveForm())
                ? relationshipName.getPassiveForm()
                : relationshipName.getActiveForm();
        ObjectProperty inverseClassRelationship = model.createObjectProperty(namespace + inverseClassRelationshipName);
        LOGGER.debug("Created Ontology Relationship Inverse : "+inverseClassRelationship.getLocalName());
        inverseClassRelationship.addDomain(secondClass);
        inverseClassRelationship.addRange(firstClass);

        mainClassRelationship.addInverseOf(inverseClassRelationship);
//        inverseClassRelationship.addInverseOf(mainClassRelationship);

        LOGGER.debug("Adding Individuals");
        data.forEach(rows -> {
            Individual firstIndividual = model.createIndividual(namespace + normalizeIndividualName(rows.get(0)), firstClass);
            LOGGER.debug("Added Individual 1 : " + firstIndividual.getLocalName());
            Individual secondIndividual = model.createIndividual(namespace + normalizeIndividualName(rows.get(1)), secondClass);
            LOGGER.debug("Added Individual 2 : " + secondIndividual.getLocalName());

            //firstIndividual.addProperty(mainClassRelationship, secondIndividual);
            model.add(model.createStatement(firstIndividual, mainClassRelationship, secondIndividual));
            model.add(model.createStatement(secondIndividual, inverseClassRelationship, firstIndividual));
            LOGGER.debug("Added Relationship Statement");
        });

//        String mergeURI = "http://www.example.com/merge.owl#";
//        PrintUtil.registerPrefix("merge", mergeURI);
//
//        OntModel mo = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
//        mo.setNsPrefix("", mergeURI);
//        mo.setNsPrefix("base", mergeURI);
//        for (ArrayList<String> co:clsrlsmerge){
//            mo.createClass(mergeURI+co.get(1));
//        }

        return model;
    }

    public String saveOntology(OntModel model, String filename) {
        String owlFilename = storageService.safeFilename(filename);
        if (!owlFilename.endsWith(".owl")) {
            owlFilename = owlFilename + ".owl";
        }
        LOGGER.debug("Save Ontology init filename : " + owlFilename);
        try {
            String filepath = storageService.safePath(storageProperties.getOntology()) + owlFilename;
            LOGGER.debug("Save Ontology init file path : " + filepath);
            File owlFile = storageService.createFile(filepath);
            LOGGER.debug("Ontology File Output created");
            FileOutputStream fos = new FileOutputStream(owlFile);
            model.write(fos);
            fos.close();
            return owlFile.getName();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public File saveOntologyTemporary(OntModel model, String filename) {
        try {
            String tmpDirectoryOp = System.getProperty("java.io.tmpdir");
            LOGGER.debug("Save Ontology init temporary file path : " + tmpDirectoryOp);
            File tmpDirectory = new File(tmpDirectoryOp);
            File owlFile = File.createTempFile(filename, ".owl", tmpDirectory);
            LOGGER.debug("Ontology File Output created");
            FileOutputStream fos = new FileOutputStream(owlFile);
            model.write(fos);
            fos.close();
            owlFile.deleteOnExit();
            return owlFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
