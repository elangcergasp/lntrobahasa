/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import reactor.core.publisher.Flux;

import java.io.StringReader;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Elang Cergas Pembrani
 */
public class BahasaPreprocessor implements Preprocessor {

    private Set<String> stopwords = null;

    @Override
    public String preprocess(String text, List<Function<String, String>> pipeline) {
        String result = text;
        for (Function<String, String> f : pipeline) {
            result = f.apply(result);
        }
        return result;
    }

    @Override
    public Flux<String> preprocess(Flux<String> text, List<Function<String, String>> pipeline) {
        return text
                // .parallel()
                .map(t -> this.preprocess(t, pipeline));
                // .sequential();
    }

    @Override
    public Flux<String> sentenceSplit(String text) {
        return Flux.fromStream(MaxentTagger.tokenizeText(new StringReader(text))
                .stream()
                // .parallel()
                .map(s -> String.join(" ", s.stream().map(HasWord::word).collect(Collectors.toList())))
        );
    }

    @Override
    public Flux<String> tokenize(String text) {
        return Flux.fromStream(MaxentTagger.tokenizeText(new StringReader(text)).stream()
                // .parallel()
                .map(s -> s.stream().map(HasWord::word))
                .reduce(Stream.empty(), Stream::concat)
        );
    }

    @Override
    public String caseFold(String text) {
        return text.toLowerCase();
    }

    @Override
    public Flux<String> caseFold(Flux<String> text) {
        return text
                // .parallel()
                .map(this::caseFold);
                // .sequential();
    }

    @Override
    public String removePattern(String regex, String text) {
        return text.replaceAll(regex, "");
    }

    @Override
    public Flux<String> removePattern(String regex, Flux<String> text) {
        return text
                // .parallel()
                .map(t -> removePattern(t, regex));
                // .sequential();
    }

    @Override
    public void setStopwords(Set<String> stopwords) {
        this.stopwords = stopwords;
    }

    @Override
    public String removeStopwords(String text) {
        return removeStopwords(stopwords, text);
    }

    @Override
    public Flux<String> removeStopwords(Flux<String> text) {
        return removeStopwords(stopwords, text);
    }

    @Override
    public String removeStopwords(Set<String> stopwords, String text) {
        if (stopwords == null) {
            return text;
        }
        return removeWordToken(stopwords, text);
    }

    @Override
    public Flux<String> removeStopwords(Set<String> stopwords, Flux<String> text) {
        if (stopwords == null) {
            return text;
        }
        return text
                // .parallel()
                .map(t -> removeStopwords(stopwords, t));
                // .sequential();
    }

    @Override
    public String removePunctuationToken(String text) {
        return removeAnyToken(PUNCTUATIONS, text);
    }

    @Override
    public Flux<String> removePunctuationToken(Flux<String> text) {
        return text
                // .parallel()
                .map(this::removePunctuationToken);
                // .sequential();
    }

    @Override
    public String removeNumberToken(String text) {
        return removeAnyToken(Stream.of("\\d+((\\.|,)\\d+)?").collect(Collectors.toSet()), text);
    }

    @Override
    public Flux<String> removeNumberToken(Flux<String> text) {
        return removeAnyToken(Stream.of("\\d+((\\.|,)\\d+)?").collect(Collectors.toSet()), text);
    }

    @Override
    public String removeExtraWhitespace(String text) {
        return text.replaceAll("(\\s){2,}", "$1").trim();
    }

    @Override
    public Flux<String> removeExtraWhitespace(Flux<String> text) {
        return text
                // .parallel()
                .map(this::removeExtraWhitespace);
                // .sequential();
    }

    @Override
    public String removeAnyToken(Set<String> set, String text) {
        if (set == null) {
            return text;
        }
        String result = text;
        for (String s : set) {
            result = result.replaceAll(s, " ");
        }
        return result;
    }

    @Override
    public Flux<String> removeAnyToken(Set<String> set, Flux<String> text) {
        return text
                // .parallel()
                .map(t -> removeAnyToken(set, t));
                // .sequential();
    }

    @Override
    public String removeWordToken(Set<String> set, String text) {
        if (set == null) {
            return text;
        }
        String result = text;
        for (String s : set) {
            result = result.replaceAll("(?<=\\W)" + s + "(?=\\W)|^" + s + "(?=\\W)|(?<=\\W)" + s + "$|^" + s + "$", " ");
        }
        return result;
    }

    @Override
    public Flux<String> removeWordToken(Set<String> set, Flux<String> text) {
        return text
                // .parallel()
                .map(t -> removeWordToken(set, t));
                // .sequential();
    }

    @Override
    public String removeTokenContaining(Set<String> set, String text) {
        if (set == null) {
            return text;
        }
        String result = text;
        for (String s : set) {
            result = result.replaceAll("\\w*" + s + "\\w+|\\w+" + s + "\\w*", " ");
        }
        return result;
    }

    @Override
    public Flux<String> removeTokenContaining(Set<String> set, Flux<String> text) {
        return text
                // .parallel()
                .map(t -> removeTokenContaining(set, t));
                // .sequential();
    }

    @Override
    public Flux<String> filterHighlight(List<String> mustContained, String text) {
        return filterHighlight(mustContained, Flux.just(text)).flatMap(t -> t);
    }

    @Override
    public Flux<Flux<String>> filterHighlight(List<String> mustContained, Flux<String> text) {
        String backRef = "__BACKREF__";

        List<String> mustContainedList = mustContained.stream()
                // .parallel()
                .map(s -> "(" + s + ")")
                .collect(Collectors.toList());

        Set<String> mustContainedSet = mustContained.stream()
                // .parallel()
                .map(this::caseFold)
                .collect(Collectors.toSet());

        String regexStr = String.join("\\b(?:(?!\\" + backRef + ").)*\\b", mustContainedList);
        int i = 1;
        for (String s : mustContainedList) {
            regexStr = regexStr.replaceFirst(backRef, Integer.toString(i));
            i++;
        }

        Pattern regex = Pattern.compile(regexStr, Pattern.CASE_INSENSITIVE);

        return Flux.create(emitter1 -> {
            text.doFinally(t -> {
                emitter1.complete();
            }).subscribe(t -> {
                Matcher m = regex.matcher(t);
                Flux<String> matches = Flux.create(emitter2 -> {
                    try {
                        while (m.find()) {
                            emitter2.next(removeExtraWhitespace(removeWordToken(mustContainedSet, m.group(0))));
                        }
                    } catch (Exception e) {}
                    emitter2.complete();
                });
                emitter1.next(matches);
            });
        });
    }

    @Override
    public <T> Collection<T> flattenList(Collection<Collection<T>> corpus) {
        return corpus.stream()
                .map(Collection::stream)
                .reduce(Stream.empty(), Stream::concat)
                .collect(Collectors.toList());
    }

    @Override
    public <T> Flux<T> flattenFlux(Flux<Flux<T>> corpus) {
        return corpus.flatMap(c -> c);
    }

    public Set<String> getStopwords() {
        return stopwords;
    }

}
