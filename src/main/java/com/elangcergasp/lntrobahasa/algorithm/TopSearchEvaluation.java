package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.EvaluationOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaActivePassiveForm;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import com.elangcergasp.lntrobahasa.services.cacher.SearchStatMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.util.MultipleFluxSyncChainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TopSearchEvaluation implements LNTROEvaluation {

    private static final Logger LOGGER = LoggerFactory.getLogger(TopSearchEvaluation.class);

    private MultipleFluxSyncChainer<SearchResultStat> syncChainer;
    private Map<String, SearchEngineRunner> searchEngineRunnerMap;
    private SearchStatMongoCacher cacher;

    private int corpusCount;

    public TopSearchEvaluation(
            SearchStatMongoCacher cacher,
            MultipleFluxSyncChainer<SearchResultStat> syncChainer,
            Map<String, SearchEngineRunner> searchEngineRunnerMap,
            int corpusCount) {
        this.cacher = cacher;
        this.syncChainer = syncChainer;
        this.searchEngineRunnerMap = searchEngineRunnerMap.entrySet().stream()
                .filter(entry -> entry.getValue().getSearchEngine().getScrapper().isCountable())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
        LOGGER.debug("Eval Search Engines " +  this.searchEngineRunnerMap);
        this.corpusCount = corpusCount;
    }

    public String createKeyword(Collection<String> data, String relation) {
//        return "\"" + String.join(" " + relation + " ", data).toLowerCase() + "\"";
        return String.join(" " + relation + " ", data).toLowerCase();
    }

    public Collection<String> createRelatedData(Collection<String> data, String relation) {
        List<String> newData = data.stream().flatMap(s -> Stream.of(s, relation)).collect(Collectors.toList());
        newData.remove(newData.size() - 1);
        return newData;
    }

    public Mono<SearchResultStat> getSearchStatFromCacheThenSource(SearchEngineRunner searchEngineRunner, String keyword) {
        return cacher
                .getSearchStatFromCache(searchEngineRunner, keyword)
                .switchIfEmpty(Mono.delay(Duration.ofMillis(searchEngineRunner.getSearchEngine().getParams().getRetryDelay()))
                        .then(cacher.getSearchStatFromSearchEngine(searchEngineRunner, keyword)));
    }

    public Mono<SearchResultStat> getSearchStatFromCacheThenSource(SearchEngineRunner searchEngineRunner, Collection<String> data) {
        return cacher
                .getSearchStatFromCache(searchEngineRunner, data)
                .switchIfEmpty(Mono.delay(Duration.ofMillis(searchEngineRunner.getSearchEngine().getParams().getRetryDelay()))
                        .then(cacher.getSearchStatFromSearchEngine(searchEngineRunner, data)));
    }

    @Override
    public Mono<EvaluationOutput> evaluate(Flux<List<String>> data, List<BahasaActivePassiveForm> terms) {
        Flux<String> topTerms = Flux.fromIterable(terms).map(BahasaActivePassiveForm::getWord);

        return topTerms
                .map(term -> new AbstractMap.SimpleEntry<>(
                        term,
                        Flux.fromStream(searchEngineRunnerMap.values().stream())
                                .parallel()
                                // .runOn(Schedulers.parallel())
                                .map(searchEngineRunner -> data
                                            .map(d -> createRelatedData(d, term))
                                            .flatMap(d -> {
                                                if (searchEngineRunner.getSearchEngine().getParams().getThreadSync()) {
                                                    synchronized (this) {
                                                        return syncChainer.enqueue(
                                                                searchEngineRunner.getSearchEngine().getID(),
                                                                getSearchStatFromCacheThenSource(searchEngineRunner, d).mergeWith(Flux.empty())
                                                        );
                                                    }
                                                } else {
                                                    return syncChainer.enqueue(
                                                            searchEngineRunner.getSearchEngine().getID(),
                                                            getSearchStatFromCacheThenSource(searchEngineRunner, d).mergeWith(Flux.empty())
                                                    );
                                                }
                                            })
                                            .doOnNext(searchResultStat -> {
                                                LOGGER.debug("Search Result Stat (" + searchResultStat.getKeyword() + ") : " + searchResultStat);
                                            })
                                            .filter(searchResultStat -> searchResultStat.getCount() != null))
                                .sequential()
                                .timeout(Duration.ofMillis(60000))
                                .flatMap(stats -> stats.reduce(new TermFrequencyMap(), (tfm, stat) -> {
                                    tfm.put(stat.getSearchEngine(), stat.getCount());
                                    return tfm;
                                }))
                                .reduce((tfm1, tfm2) -> TermFrequencyMap.join(tfm1, tfm2))
                ))
                .collectList()
                .map(entries -> new EvaluationOutput(
                        topTerms.collectList().block(),
                        entries.stream().collect(Collectors.toMap(
                                Map.Entry::getKey,
                                entry -> entry.getValue().block()
                        ))));
    }

    public Map<String, SearchEngineRunner> getSearchEngineRunnerMap() {
        return searchEngineRunnerMap;
    }

    public void setSearchEngineRunnerMap(Map<String, SearchEngineRunner> searchEngineRunnerMap) {
        this.searchEngineRunnerMap = searchEngineRunnerMap;
    }

    public SearchStatMongoCacher getCacher() {
        return cacher;
    }

    public void setCacher(SearchStatMongoCacher cacher) {
        this.cacher = cacher;
    }

    public MultipleFluxSyncChainer<SearchResultStat> getSyncChainer() {
        return syncChainer;
    }

    public void setSyncChainer(MultipleFluxSyncChainer<SearchResultStat> syncChainer) {
        this.syncChainer = syncChainer;
    }

    public int getCorpusCount() {
        return corpusCount;
    }

    public void setCorpusCount(int corpusCount) {
        this.corpusCount = corpusCount;
    }
}
