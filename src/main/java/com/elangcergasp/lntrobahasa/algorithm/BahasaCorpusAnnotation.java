/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTag;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTagSet;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TaggedWord;
import reactor.core.publisher.Flux;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class BahasaCorpusAnnotation implements CorpusAnnotation {

    private POSTagger posTagger;

    public BahasaCorpusAnnotation(POSTagger posTagger) {
        this.posTagger = posTagger;
    }

    public POSTagger getPosTagger() {
        return posTagger;
    }

    public void setPosTagger(POSTagger posTagger) {
        this.posTagger = posTagger;
    }

    @Override
    public Flux<TaggedWord> annotate(String corpus) {
        return Flux.fromIterable(posTagger.posTag(corpus));
    }

    @Override
    public Flux<Flux<TaggedWord>> annotate(Flux<String> corpus) {
        return corpus
                .parallel()
                .map(this::annotate)
                .sequential();
    }

    @Override
    public Flux<String> getWordsByTag(POSTag tag, Flux<TaggedWord> taggedWords) {
        return taggedWords
                .parallel()
                .filter(w -> w.getPosTag().equals(tag))
                .map(TaggedWord::getWord)
                .sequential();
    }

    @Override
    public Flux<String> getVerbs(Flux<TaggedWord> taggedWords) {
        return getWordsByTag(POSTagSet.VERB, taggedWords);
    }

}
