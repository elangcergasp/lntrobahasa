/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TaggedWord;

import java.util.List;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface POSTagger {
    
    public List<TaggedWord> posTag(String corpus);
    
}
