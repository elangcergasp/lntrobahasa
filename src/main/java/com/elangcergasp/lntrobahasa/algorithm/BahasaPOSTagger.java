/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTagSet;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TaggedWord;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Elang Cergas Pembrani
 */
public class BahasaPOSTagger implements POSTagger {

    private static final Logger LOGGER = LoggerFactory.getLogger(BahasaPOSTagger.class);

    private String modelPath;
    private MaxentTagger posTagger;

    public static final POSTagSet POS_TAG_SET = new POSTagSet();

    public BahasaPOSTagger(String modelPath) {
        this.modelPath = modelPath;
        this.posTagger = new MaxentTagger(modelPath);
        LOGGER.debug("POS Tagger Tag Count is " + posTagger.numTags());
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
        this.posTagger = new MaxentTagger(modelPath);
    }

    public MaxentTagger getPosTagger() {
        return posTagger;
    }

    public void setPosTagger(MaxentTagger posTagger) {
        this.posTagger = posTagger;
        this.modelPath = null;
    }

    @Override
    public List<TaggedWord> posTag(String corpus) {
        List<List<HasWord>> sentences = MaxentTagger.tokenizeText(new StringReader(corpus));
        List<TaggedWord> words = new ArrayList<>();
        sentences.forEach(s -> {
            posTagger.tagSentence(s).forEach(w -> {
                words.add(new TaggedWord(w.word(), POS_TAG_SET.valueOf(w.tag())));
            });
        });
        return words;
    }

}
