package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROAlgorithmParams;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.properties.POSTaggerProperties;
import com.elangcergasp.lntrobahasa.services.cacher.KBBIMongoCacher;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import com.elangcergasp.lntrobahasa.util.MyArray;
import io.netty.util.internal.ConcurrentSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class BahasaLNTROAlgorithmFactory implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(BahasaLNTROAlgorithmFactory.class);

    // public static final String DEFAULT_SEARCH_ENGINE_RUNNER_ID = "GOOGLE";
    public static final String DEFAULT_SEARCH_ENGINE_RUNNER_ID = null;

    @Autowired
    private KBBIMongoCacher KBBI;

    @Autowired
    private BahasaStemmer stemmer;

    @Autowired
    private LNTROProperties lntroProperties;

    @Autowired
    private POSTaggerProperties posTaggerProperties;

    private Set<String> extractionStopwords = null;

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public Set<String> getStopwords() {
        if (extractionStopwords == null) {
            String extractionStopwordsPath = lntroProperties.getPreprocessing().getStopwords().get("extraction");
            if (extractionStopwordsPath != null && extractionStopwordsPath.length() > 0) {
                InputStream extractionStopwordsInput;
                try {
                    extractionStopwordsInput = new FileInputStream(extractionStopwordsPath);

                    extractionStopwords = new ConcurrentSet<>();

                    MyArray.tableToRowsFlux(
                            Table.read().csv(CsvReadOptions
                                    .builder(extractionStopwordsInput, "STOPWORDS")
                                    .separator('\t')
                                    .header(false)))
                            .doFinally(signalType -> {
                                LOGGER.info("Fetched Extraction Stopwords from \"" + extractionStopwordsPath + "\" with " + (extractionStopwords == null ? 0 : extractionStopwords.size()) + " words");
                            })
                            .subscribe(stopwordRow -> {
                                extractionStopwords.addAll(stopwordRow);
                            });
                } catch (FileNotFoundException ex) {
                    LOGGER.error("Stopword file not found : " + ex.getMessage());
                } catch (IOException ex) {
                    LOGGER.error("Input Stopwords error : " + ex.getMessage());
                }
            }
        }
        return extractionStopwords;
    }

    public LNTROAlgorithm buildLNTROAlgorithm(LNTROAlgorithmParams params) {
        List<String> constructionSearchEngines = params.getConstructionSearchEngines();
        Integer corpusCount = params.getCorpusCount();
        String posTaggerID = params.getPosTagger();
        List<String> extractionMethods = params.getExtractionMethods();
        List<String> evaluationSearchEngines = params.getEvaluationSearchEngines();

        LNTROAlgorithm algorithm = new LNTROAlgorithm();

        algorithm.setPreprocessor(applicationContext.getBean(Preprocessor.class));

        Map<String, SearchEngineRunner> constructionSearchEngineRunnerMap = (Map<String, SearchEngineRunner>) applicationContext.getBean("searchEngineRunners", constructionSearchEngines);
        LOGGER.debug("Factory: " + constructionSearchEngineRunnerMap);

        BahasaCorpusConstruction corpusConstruction = (BahasaCorpusConstruction) (constructionSearchEngines != null && constructionSearchEngines.size() > 0
                ? applicationContext.getBean("corpusConstruction", constructionSearchEngineRunnerMap)
                : applicationContext.getBean(CorpusConstruction.class));
        corpusConstruction.setCorpusCount(corpusCount);
        algorithm.setCorpusConstruction(corpusConstruction);
        LOGGER.debug("Factory: " + corpusConstruction.getAllSearchEngineRunner());

        CorpusAnnotation corpusAnnotation = posTaggerID != null
                ? applicationContext.getBean(CorpusAnnotation.class, posTaggerProperties.getModels().get(posTaggerID).getModelFile())
                : applicationContext.getBean(CorpusAnnotation.class);
        algorithm.setCorpusAnnotation(corpusAnnotation);
        LOGGER.debug("Factory: " + corpusAnnotation);

        for (String extractionMethod : extractionMethods) {
            try {
                algorithm.addLntroExtraction(extractionMethod, applicationContext.getBean(extractionMethod, LNTROExtraction.class));
            } catch (BeansException e) {
                throw new IllegalArgumentException("Extraction Method '" + extractionMethod + "' does not exists", e);
            }
        }

        Map<String, SearchEngineRunner> evaluationSearchEngineRunnerMap = (Map<String, SearchEngineRunner>) applicationContext.getBean("searchEngineRunnersStatCounter", evaluationSearchEngines, DEFAULT_SEARCH_ENGINE_RUNNER_ID);
        LOGGER.debug("Factory: " + evaluationSearchEngineRunnerMap);

        TopSearchEvaluation evaluationMethod = (TopSearchEvaluation) applicationContext.getBean(LNTROEvaluation.class, evaluationSearchEngineRunnerMap);
        algorithm.setLntroEvaluation(evaluationMethod);
        LOGGER.debug("Factory: " + evaluationMethod.getSearchEngineRunnerMap());

        algorithm.setKBBI(KBBI);

        algorithm.setStemmer(stemmer);

        algorithm.setExtractionStopwords(getStopwords());

        algorithm.setLntroProperties(lntroProperties);

        return algorithm;
    }
}
