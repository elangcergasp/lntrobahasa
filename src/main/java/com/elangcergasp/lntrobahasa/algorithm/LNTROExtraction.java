/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermScoreMap;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface LNTROExtraction {

    public String getDescription();
    
    public Mono<TermScoreMap> getFinalScores(Flux<TermFrequencyMap> tfmList);
    
    public Mono<String> getExtractionTerm(Flux<TermFrequencyMap> tfmList);
    public Mono<String> getExtractionTerm(Mono<TermScoreMap> tm);
    public String getExtractionTerm(TermScoreMap tm);
    
    public Mono<Double> getExtractionScore(Flux<TermFrequencyMap> tfmList);
    public Mono<Double> getExtractionScore(Mono<TermScoreMap> tm);
    public double getExtractionScore(TermScoreMap tm);
    
}
