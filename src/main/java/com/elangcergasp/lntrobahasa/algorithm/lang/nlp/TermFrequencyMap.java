/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import reactor.core.publisher.Flux;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Elang Cergas Pembrani
 */
public class TermFrequencyMap extends ConcurrentHashMap<String, Long> {

    protected static final String DELIMITER_TERM = "\n";
    protected static final String DELIMITER_FREQUENCY = "\t";

    public TermFrequencyMap() {
    }

    public TermFrequencyMap(Collection<String> terms) {
        terms.forEach(this::put);
    }

    public TermFrequencyMap(Flux<String> terms) {
        terms.subscribe(this::put);
    }

    public TermFrequencyMap(TermFrequencyMap tfm) {
        tfm.forEach(this::put);
    }

    public Long safeGet(String k) {
        return this.getOrDefault(k, 0L);
    }

    public boolean contains(Object o) {
        return super.containsKey(o.toString()); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long put(String k, Long v) {
        Long newCount = (v == null) ? 0 : (v + this.safeGet(k));
        if (newCount < 0) {
            throw new IllegalArgumentException("Term Frequency must not less than zero");
        }
        super.put(k, newCount);
        return newCount;
    }

    public Long put(String s) {
        return this.put(s, 1L);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Long> map) {
        map.keySet().forEach(s -> {
            this.put(s, map.get(s));
        });
    }

    public Long countTerm(String s) {
        return this.safeGet(s);
    }

    public Long countAll() {
        return this.values().stream().reduce(0L, Long::sum);
    }

    public Set<String> termsSet() {
        return new HashSet<>(this.keySet());
    }

    public List<String> termsVector() {
        List<String> vector = new ArrayList<>();
        this.termsSet().forEach(s -> {
            Long termCount = this.countTerm(s);
            for (int i = 0; i < termCount; i++) {
                vector.add(s);
            }
        });
        return vector;
    }

    public Long min() {
        return values().stream().reduce(Long.MAX_VALUE, Math::min);
    }

    public Long max() {
        return values().stream().reduce(Long.MIN_VALUE, Math::max);
    }

    public String getByValue(Long v) {
        return termsSet().stream()
                .filter(s -> Objects.equals(safeGet(s), v))
                .findFirst()
                .orElse(null);
    }

    public String minKey() {
        return getByValue(min());
    }

    public String maxKey() {
        return getByValue(max());
    }

    public double percentageScore(String s) {
        return ((double) countTerm(s)) / ((double) countAll());
    }

    public double normalScore(String s, double newMin, double newMax) {
        double mapMin = (double) min();
        double mapMax = (double) max();
        double count = (double) countTerm(s);
        double normalScore = (((count - mapMin) / (mapMax - mapMin)) * (newMax - newMin)) + newMin;
        return !Double.isNaN(normalScore)
                ? normalScore
                : ((newMax - newMin) / size());
    }

    public double normalScore(String s) {
        return normalScore(s, 0, 1);
    }

    public TermScoreMap getAllPercentageScores() {
        TermScoreMap result = new TermScoreMap();
        termsSet().stream()
                .map(s -> new AbstractMap.SimpleEntry<>(
                        s,
                        percentageScore(s)
                ))
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .forEach(entry -> {
                    result.put(entry.getKey(), entry.getValue());
                });
        return result;
    }

    public TermScoreMap getAllNormalizedScores() {
        TermScoreMap result = new TermScoreMap();
        termsSet().stream()
                .map(s -> new AbstractMap.SimpleEntry<>(
                        s,
                        normalScore(s)
                ))
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .forEach(entry -> {
                    result.put(entry.getKey(), entry.getValue());
                });
        return result;
    }

    public List<AtomicTermFrequency> toAtomicTermList() {
        return this.entrySet().stream()
                .parallel()
                .filter(e -> e.getValue() != null)
                .map(e -> new AtomicTermFrequency(e.getKey(), e.getValue()))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public void join(Map<? extends String, ? extends Long> other) {
        this.putAll(other);
    }

    public static TermFrequencyMap join(TermFrequencyMap... tfms) {
        TermFrequencyMap result = new TermFrequencyMap();
        for (TermFrequencyMap tfm : tfms) {
            result.join(tfm);
        }
        return result;
    }

    public static TermFrequencyMap join(Collection<TermFrequencyMap> tfms) {
        TermFrequencyMap result = new TermFrequencyMap();
        tfms.forEach(result::join);
        return result;
    }

    public static TermFrequencyMap fromString(String serialized) {
        TermFrequencyMap result = new TermFrequencyMap();
        Arrays.asList(serialized.split(DELIMITER_TERM)).forEach(s -> {
            String[] tfArray = s.split(DELIMITER_FREQUENCY);
            try {
                result.put(tfArray[0], Long.parseLong(tfArray[1]));
            } catch (Exception e) {
                result.put(tfArray[0], 0L);
            }
        });
        return result;
    }

    @Override
    public String toString() {
        return String.join(DELIMITER_TERM, this.termsSet().stream()
                .sorted((t1, t2) -> {
                    Long compared = this.countTerm(t2) - this.countTerm(t1);
                    return compared != 0L ? compared.intValue() : t1.compareTo(t2);
                })
                .map(s -> s + DELIMITER_FREQUENCY + this.countTerm(s)
                        + DELIMITER_FREQUENCY + this.percentageScore(s)
                        + DELIMITER_FREQUENCY + this.normalScore(s))
                .collect(Collectors.toList()));
    }

}
