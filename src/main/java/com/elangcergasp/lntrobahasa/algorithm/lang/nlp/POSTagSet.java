/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import java.util.HashSet;

/**
 *
 * @author Elang Cergas Pembrani
 * @see http://bahasa.cs.ui.ac.id/postag/downloads/Tagset.pdf
 */
public class POSTagSet extends HashSet<POSTag> {

    public static final POSTag CC = new POSTag("CC");
    public static final POSTag CD = new POSTag("CD");
    public static final POSTag DT = new POSTag("DT");
    public static final POSTag FW = new POSTag("FW");
    public static final POSTag IN = new POSTag("IN");
    public static final POSTag JJ = new POSTag("JJ");
    public static final POSTag MD = new POSTag("MD");
    public static final POSTag NEG = new POSTag("NEG");
    public static final POSTag NN = new POSTag("NN");
    public static final POSTag NND = new POSTag("NND");
    public static final POSTag NNP = new POSTag("NNP");
    public static final POSTag OD = new POSTag("OD");
    public static final POSTag PR = new POSTag("PR");
    public static final POSTag PRP = new POSTag("PRP");
    public static final POSTag RB = new POSTag("RB");
    public static final POSTag RP = new POSTag("RP");
    public static final POSTag SC = new POSTag("SC");
    public static final POSTag SYM = new POSTag("SYM");
    public static final POSTag UH = new POSTag("UH");
    public static final POSTag VB = new POSTag("VB");
    public static final POSTag WH = new POSTag("WH");
    public static final POSTag X = new POSTag("X");
    public static final POSTag Z = new POSTag("Z");
    
    /*  LOCAL ALIASES  */
    public static final POSTag VERB = VB;
    public static final POSTag NOUN = NN;
    public static final POSTag ADJECTIVE = JJ;
    public static final POSTag ADVERB = RB;
    public static final POSTag PARTICLE = CC;
    public static final POSTag NUMERAL = CD;
    public static final POSTag PRONOUN = PR;
    public static final POSTag UNKNOWN = X;

    /*  LOCAL TAG SET  */
    public static final POSTag ACTIVE_VERB = new POSTag("ACTIVE_VERB", VB);
    public static final POSTag PASSIVE_VERB = new POSTag("PASSIVE_VERB", VB);

    public POSTagSet() {
        add(CC);
        add(CD);
        add(OD);
        add(DT);
        add(FW);
        add(IN);
        add(JJ);
        add(MD);
        add(NEG);
        add(NN);
        add(NNP);
        add(NND);
        add(PR);
        add(PRP);
        add(RB);
        add(RP);
        add(SC);
        add(SYM);
        add(UH);
        add(VB);
        add(WH);
        add(X);
        add(Z);
    }

    public POSTag valueOf(String tag) {
        POSTag posTag = new POSTag(tag);
        if (this.stream().noneMatch(p -> p.equals(posTag))) {
            throw new UnsupportedOperationException();
        }
        return posTag;
    }

}
