package com.elangcergasp.lntrobahasa.algorithm.lang.lntro;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "LNTROProcess")
public class LNTROProcess {

    @Id
    private String id;

    @CreatedDate
    private Date fetchDate = defaultFetchDate();

    private String title;
    private String description;
    private LNTROAlgorithmParams algorithmParams;
    private LNTROBlockOutput output;
    private String ontologyOWLFile;

    public static Date defaultFetchDate() { return Calendar.getInstance().getTime(); }
}
