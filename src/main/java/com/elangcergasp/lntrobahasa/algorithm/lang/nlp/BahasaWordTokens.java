package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import lombok.*;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BahasaWordTokens {

    private String word;
    private List<String> tokens;
}
