/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class AtomicTermScore implements Comparable<AtomicTermScore> {

    public static final String DELIMITER_SCORE = "\t";

    public final String TERM;
    public final double SCORE;

    public AtomicTermScore(String TERM, double SCORE) {
        this.TERM = TERM;
        this.SCORE = SCORE;
    }

    @Override
    public String toString() {
        return TERM + DELIMITER_SCORE + SCORE;
    }

    @Override
    public int compareTo(AtomicTermScore t) {
        return Double.compare(SCORE, t.SCORE);
    }

}
