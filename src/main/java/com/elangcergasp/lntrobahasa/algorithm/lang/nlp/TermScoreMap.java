/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import reactor.core.publisher.Flux;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class TermScoreMap extends ConcurrentHashMap<String, Double> {

    protected static final String DELIMITER_TERM = "\n";
    protected static final String DELIMITER_SCORE = "\t";

    public TermScoreMap() {
    }

    public TermScoreMap(Collection<String> terms) {
        terms.forEach(s -> put(s, 1d));
    }

    public TermScoreMap(Flux<String> terms) {
        terms.subscribe(s -> {
            put(s, 1d);
        });
    }

    public TermScoreMap(TermScoreMap tfm) {
        tfm.forEach((k, v) -> this.put((String) k, (Double) v));
    }

    public Double safeGet(String k) {
        return this.getOrDefault(k, 0d);
    }

    public boolean contains(Object o) {
        return super.containsKey(o.toString()); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Double put(String k, Double v) {
        double newCount = (v == null) ? 0d : (v + this.safeGet(k));
        if (newCount < 0) {
            throw new IllegalArgumentException("Term Frequency must not less than zero");
        }
        super.put(k, newCount);
        return newCount;
    }

    @Override
    public void putAll(Map<? extends String, ? extends Double> map) {
        map.keySet().forEach(s -> {
            this.put(s, map.get(s));
        });
    }

    public Set<String> termsSet() {
        return new HashSet<>(this.keySet());
    }

    public Double min() {
        return values().stream().reduce(Double.MAX_VALUE, Math::min);
    }

    public Double max() {
        return values().stream().reduce(Double.MIN_VALUE, Math::max);
    }

    public String getByValue(Double v) {
        return termsSet().stream()
                .filter(s -> Objects.equals(safeGet(s), v))
                .findFirst()
                .orElse(null);
    }

    public String minKey() {
        return getByValue(min());
    }

    public String maxKey() {
        return getByValue(max());
    }
    
    public List<AtomicTermScore> toAtomicTermList() {
        return this.entrySet().stream()
                .parallel()
                .filter(e -> e.getValue() != null)
                .map(e -> new AtomicTermScore(e.getKey(), e.getValue()))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public void join(Map<? extends String, ? extends Double> other) {
        this.putAll(other);
    }

    public static TermScoreMap join(TermScoreMap... tfms) {
        TermScoreMap result = new TermScoreMap();
        for (TermScoreMap tfm : tfms) {
            result.join(tfm);
        }
        return result;
    }

    public static TermScoreMap join(Collection<TermScoreMap> tfms) {
        TermScoreMap result = new TermScoreMap();
        tfms.forEach(result::join);
        return result;
    }

    public static TermScoreMap fromString(String serialized) {
        TermScoreMap result = new TermScoreMap();
        Arrays.asList(serialized.split(DELIMITER_TERM)).forEach(s -> {
            String[] tfArray = s.split(DELIMITER_SCORE);
            try {
                result.put(tfArray[0], Double.parseDouble(tfArray[1]));
            } catch (Exception e) {
                result.put(tfArray[0], 0d);
            }
        });
        return result;
    }

    @Override
    public String toString() {
        return String.join(DELIMITER_TERM, this.termsSet().stream()
                .sorted((t1, t2) -> {
                    int compared = Double.compare(this.safeGet(t2), this.safeGet(t1));
                    return compared != 0 ? compared : t1.compareTo(t2);
                })
                .map(s -> s + DELIMITER_SCORE + this.safeGet(s))
                .collect(Collectors.toList()));
    }

}
