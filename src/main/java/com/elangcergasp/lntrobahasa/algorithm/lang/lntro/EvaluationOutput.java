package com.elangcergasp.lntrobahasa.algorithm.lang.lntro;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@ToString
@Getter
@Setter
@Document
public class EvaluationOutput {

    /*
    public double MAX_SCORE = 1;
    public double INITIAL_SCORE_MODIFIER = 0.5;
    public double DIFF_SCORE_MODIFIER = 0.5;
    */

    private List<String> extractedTerms;
    private Map<String, TermFrequencyMap> evaluatedTermsMap;
    // private TermScoreMap evaluationScores;

    public EvaluationOutput(List<String> extractedTerms, Map<String, TermFrequencyMap> evaluatedTermsMap) {
        this.extractedTerms = extractedTerms;
        this.evaluatedTermsMap = evaluatedTermsMap;
        /*
        List<String> evaluatedTerms = evaluatedTermsMap.toAtomicTermList().stream()
                .map(atf -> atf.TERM).collect(Collectors.toList());

        this.evaluationScores = new TermScoreMap();
        for (int i = 0; i < extractedTerms.size(); i++) {
            double tempScore = 0;
            String extractedTerm = extractedTerms.get(i);
            if (extractedTerm.equals(evaluatedTerms.get(i))) {
                tempScore = (i == 0) ? MAX_SCORE : Math.pow(INITIAL_SCORE_MODIFIER, i);
            } else {
                for (int j = 0; j < evaluatedTerms.size(); j++) {
                    if (i != j) {
                        if (extractedTerm.equals(evaluatedTerms.get(j))) {
                            tempScore = Math.pow(DIFF_SCORE_MODIFIER, Math.abs(i-j));
                        }
                    }
                }
            }
            this.evaluationScores.put(extractedTerm, tempScore);
        }
        */
    }
}
