/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class AtomicTermFrequency implements Comparable<AtomicTermFrequency> {

    public static final String DELIMITER_FREQUENCY = "\t";

    public final String TERM;
    public final Long FREQUENCY;

    public AtomicTermFrequency(String TERM, long SCORE) {
        this.TERM = TERM;
        this.FREQUENCY = SCORE;
    }

    @Override
    public String toString() {
        return TERM + DELIMITER_FREQUENCY + FREQUENCY;
    }

    @Override
    public int compareTo(AtomicTermFrequency t) {
        return Long.compare(FREQUENCY, t.FREQUENCY);
    }

}
