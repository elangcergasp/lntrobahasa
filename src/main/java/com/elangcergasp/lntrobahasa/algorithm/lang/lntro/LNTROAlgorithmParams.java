package com.elangcergasp.lntrobahasa.algorithm.lang.lntro;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class LNTROAlgorithmParams {

    private String corpusConstructionMethod;
    private List<String> constructionSearchEngines;
    private Integer corpusCount;
    private String posTagger;
    private List<String> extractionMethods;
    private List<String> evaluationSearchEngines;
}
