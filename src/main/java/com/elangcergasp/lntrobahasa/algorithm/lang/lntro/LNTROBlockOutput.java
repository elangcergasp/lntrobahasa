package com.elangcergasp.lntrobahasa.algorithm.lang.lntro;

import com.elangcergasp.lntrobahasa.algorithm.CorpusConstruction;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaActivePassiveForm;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TaggedWord;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermScoreMap;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class LNTROBlockOutput {

    private List<String> columns;
    private List<List<String>> data;
    private Map<String, List<CorpusConstruction.CorpusItem>> rawCorpus;
    private Map<String, List<String>> corpus;
    private Map<String, List<List<TaggedWord>>> annotatedCorpus;
    private Map<String, TermFrequencyMap> termFrequencyMaps;
    private Map<String, TermScoreMap> extractionScores;
    private Map<String, List<BahasaActivePassiveForm>> topExtractionTerms;
    private List<List<String>> evaluationDataSample;
    private Map<String, EvaluationOutput> evaluationScores;
    private BahasaActivePassiveForm lntroTermResult;

    private Map<String, String> _benchmark;
}
