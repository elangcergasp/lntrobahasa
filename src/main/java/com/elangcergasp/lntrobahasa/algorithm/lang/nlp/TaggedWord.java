/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class TaggedWord {
    
    public static String delimiter = "/";
    
    protected final POSTag posTag;
    protected final String word;
    
    public TaggedWord(String word, POSTag tag) {
        this.word = word;
        this.posTag = tag;
    }

    public POSTag getPosTag() {
        return posTag;
    }

    public String getWord() {
        return word;
    }
    
    @Override
    public String toString() {
        return word + delimiter + posTag;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TaggedWord) {
            TaggedWord other = (TaggedWord) obj;
            return word.equals(other.word) && posTag.equals(other.posTag);
        } else {
            return false;
        }
    }
    
}
