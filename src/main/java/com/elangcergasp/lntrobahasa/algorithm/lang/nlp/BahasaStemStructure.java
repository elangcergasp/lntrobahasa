package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BahasaStemStructure {

    private String word;
    private String prefix_1;
    private String prefix_2;
    private String stem;
    private String suffix_1;
    private String suffix_2;
    private String possessive;
    private String particle;
}
