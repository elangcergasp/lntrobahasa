package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BahasaActivePassiveForm {

    private String word;
    private String stem;
    private String activeForm;
    private String passiveForm;
}
