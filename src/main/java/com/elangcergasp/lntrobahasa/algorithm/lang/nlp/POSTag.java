/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.nlp;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Elang Cergas Pembrani
 */
public class POSTag implements Serializable {

    private String tag = null;
    private POSTag parent = null;

    public POSTag() {
    }

    public POSTag(String tag) {
        this.setTag(tag);
    }

    public POSTag(String tag, POSTag parent) {
        this.setTag(tag);
        this.setParent(parent);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        if (this.tag == null) {
            this.tag = tag;
        }
    }

    public POSTag getParent() {
        return parent;
    }

    public void setParent(POSTag parent) {
        if (this.parent == null) {
            this.parent = parent;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof POSTag)) return false;
        POSTag other = (POSTag) o;
        if (Objects.equals(this.tag, other.tag)) return true;
        if (this.parent != null) {
            POSTag tmp = this;
            do {
                tmp = tmp.parent;
                if (Objects.equals(tmp.tag, other.tag)) return true;
            } while (tmp.parent != null);
        }
        if (other.parent != null) {
            POSTag tmp = other;
            do {
                tmp = tmp.parent;
                if (Objects.equals(tmp.tag, this.tag)) return true;
            } while (tmp.parent != null);
        }
        return false;
    }

    @Override
    public String toString() {
        return getTag();
    }

}
