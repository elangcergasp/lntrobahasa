/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.lntro;

import com.elangcergasp.lntrobahasa.algorithm.CorpusConstruction;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.BahasaActivePassiveForm;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TaggedWord;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermFrequencyMap;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.TermScoreMap;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.services.SimpleBenchmarkService;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.mapping.Document;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 *
 * @author Elang Cergas Pembrani
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class LNTROProcessOutput {

    private static final Logger LOGGER = LoggerFactory.getLogger(LNTROProcessOutput.class);

    private List<String> columns;
    private Flux<List<String>> data;
    private Flux<Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>> rawCorpus;
    private Flux<Map.Entry<List<String>, Flux<String>>> corpus;
    private Flux<Map.Entry<List<String>, Flux<Flux<TaggedWord>>>> annotatedCorpus;
    private Flux<Map.Entry<List<String>, Mono<TermFrequencyMap>>> termFrequencyMaps;
    private Flux<Map.Entry<String, Mono<TermScoreMap>>> extractionScores;
    private Flux<Map.Entry<String, Mono<List<BahasaActivePassiveForm>>>> topExtractionTerms;
    private Flux<List<String>> evaluationDataSample;
    private Flux<Map.Entry<String, Mono<EvaluationOutput>>> evaluationScores;
    private Mono<BahasaActivePassiveForm> lntroTermResult;

    public LNTROBlockOutput block() {
        LNTROBlockOutput output = new LNTROBlockOutput();

        SimpleBenchmarkService.TimeMark benchmarkStart = SimpleBenchmarkService.getInstance().start();
        SimpleBenchmarkService.TimeMark lastBenchmark = benchmarkStart;
        SimpleBenchmarkService.TimeMark benchmarkPoint = benchmarkStart;

        output.set_benchmark(new ConcurrentHashMap<>());
        output.get_benchmark().put("Data", SimpleBenchmarkService.format(0L));
        output.get_benchmark().put("Corpus Construction", SimpleBenchmarkService.format(0L));
        output.get_benchmark().put("Corpus Annotation", SimpleBenchmarkService.format(0L));
        output.get_benchmark().put("LNTRO Extraction", SimpleBenchmarkService.format(0L));
        output.get_benchmark().put("LNTRO Evaluation", SimpleBenchmarkService.format(0L));
        output.get_benchmark().put("Full Process", SimpleBenchmarkService.format(0L));

        if (columns != null) {
            LOGGER.debug("LNTRO Block Output : Setting Columns");
            output.setColumns(columns);
        }

        if (data != null) {
            LOGGER.debug("LNTRO Block Output : Setting Data");
            output.setData(data.collectList().block());

            lastBenchmark = benchmarkPoint;
            benchmarkPoint = SimpleBenchmarkService.getInstance().start();
            output.get_benchmark().put("Data", SimpleBenchmarkService.format(benchmarkPoint.diff(lastBenchmark)));
        }

        if (rawCorpus != null) {
            LOGGER.debug("LNTRO Block Output : Setting Raw Corpus");
            output.setRawCorpus(rawCorpus.collectList().block().stream().collect(Collectors.toMap(
                    entry -> String.join(",", entry.getKey()),
                    entry -> entry.getValue().collectList().block()
            )));
        }

        if (corpus != null) {
            LOGGER.debug("LNTRO Block Output : Setting Corpus");
            output.setCorpus(corpus.collectList().block().stream().collect(Collectors.toMap(
                    entry -> String.join(",", entry.getKey()),
                    entry -> entry.getValue().collectList().block()
            )));

            lastBenchmark = benchmarkPoint;
            benchmarkPoint = SimpleBenchmarkService.getInstance().start();
            output.get_benchmark().put("Corpus Construction", SimpleBenchmarkService.format(benchmarkPoint.diff(lastBenchmark)));
        }

        if (annotatedCorpus != null) {
            LOGGER.debug("LNTRO Block Output : Setting Annotated Corpus");
            output.setAnnotatedCorpus(annotatedCorpus.collectList().block().stream().collect(Collectors.toMap(
                    entry -> String.join(",", entry.getKey()),
                    entry -> entry.getValue().map(tw -> tw.collectList().block()).collectList().block()
            )));

            lastBenchmark = benchmarkPoint;
            benchmarkPoint = SimpleBenchmarkService.getInstance().start();
            output.get_benchmark().put("Corpus Annotation", SimpleBenchmarkService.format(benchmarkPoint.diff(lastBenchmark)));
        }

        if (termFrequencyMaps != null) {
            LOGGER.debug("LNTRO Block Output : Setting Term Frequency Maps");
            output.setTermFrequencyMaps(termFrequencyMaps.collectList().block().stream().collect(Collectors.toMap(
                    entry -> String.join(",", entry.getKey()),
                    entry -> entry.getValue().block()
            )));
        }

        if (extractionScores != null) {
            LOGGER.debug("LNTRO Block Output : Setting Extraction Scores");
            output.setExtractionScores(extractionScores.collectList().block().stream().collect(Collectors.toMap(
                    entry -> entry.getKey(),
                    entry -> entry.getValue().block()
            )));
        }

        if (topExtractionTerms != null) {
            LOGGER.debug("LNTRO Block Output : Setting Top Extraction Terms");
            output.setTopExtractionTerms(topExtractionTerms.collectList().block().stream().collect(Collectors.toMap(
                    entry -> entry.getKey(),
                    entry -> entry.getValue().block()
            )));

            lastBenchmark = benchmarkPoint;
            benchmarkPoint = SimpleBenchmarkService.getInstance().start();
            output.get_benchmark().put("LNTRO Extraction", SimpleBenchmarkService.format(benchmarkPoint.diff(lastBenchmark)));
        }

        if (evaluationDataSample != null) {
            LOGGER.debug("LNTRO Block Output : Setting Evaluation Data Sample");
            output.setEvaluationDataSample(evaluationDataSample.collectList().block());
        }

        if (evaluationScores != null) {
            LOGGER.debug("LNTRO Block Output : Setting Evaluation Scores");
            output.setEvaluationScores(evaluationScores.collectList().block().stream().collect(Collectors.toMap(
                    entry -> entry.getKey(),
                    entry -> entry.getValue().block()
            )));

            lastBenchmark = benchmarkPoint;
            benchmarkPoint = SimpleBenchmarkService.getInstance().start();
            output.get_benchmark().put("LNTRO Evaluation", SimpleBenchmarkService.format(benchmarkPoint.diff(lastBenchmark)));
        }

        benchmarkPoint = SimpleBenchmarkService.getInstance().start();
        output.get_benchmark().put("Full Process", SimpleBenchmarkService.format(benchmarkPoint.diff(benchmarkStart)));

        if (lntroTermResult != null) {
            output.setLntroTermResult(lntroTermResult.block());
        }

        return output;
    }
}
