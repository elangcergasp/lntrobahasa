/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm.lang.lntro;

/**
 *
 * @author Elang Cergas Pembrani
 */
public interface EvaluationMeasure {
    
    public double getRecall();

    public double getPrecision();

    public double getFMeasure();

    public int getValidRelationCount();

}
