/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.algorithm;

import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.EvaluationOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.lntro.LNTROProcessOutput;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.*;
import com.elangcergasp.lntrobahasa.model.DocumentKBBI;
import com.elangcergasp.lntrobahasa.properties.LNTROProperties;
import com.elangcergasp.lntrobahasa.services.cacher.KBBIMongoCacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Elang Cergas Pembrani
 */
public class LNTROAlgorithm {

    private static final Logger LOGGER = LoggerFactory.getLogger(LNTROAlgorithm.class);

    private KBBIMongoCacher KBBI;
    private LNTROProperties lntroProperties;

    private BahasaStemmer stemmer;
    private Preprocessor preprocessor;
    private CorpusConstruction corpusConstruction;
    private CorpusAnnotation corpusAnnotation;
    private Map<String, LNTROExtraction> lntroExtractions = new ConcurrentHashMap<>();
    private LNTROEvaluation lntroEvaluation;

    private Set<String> extractionStopwords = null;

    public List<Function<String, String>> getCorpusPreprocessing() {
        return Arrays.asList(
                preprocessor::caseFold,
                preprocessor::removeNumberToken,
                preprocessor::removePunctuationToken,
                /*(String s) -> {
                    Set<String> customRegex = Preprocessor.PUNCTUATIONS;
                    customRegex.add("\\d+((\\.|,)\\d+)?");
                    return preprocessor.removeTokenContaining(customRegex, s);
                },*/
                //preprocessor::removeStopwords,
                preprocessor::removeExtraWhitespace
        );
    }

    public LNTROProcessOutput constructData(Flux<List<String>> rows) {
        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(rows.distinct());
        return output;
    }

    /* [DUPLICATE] FROM constructCorpus */
    public LNTROProcessOutput constructCorpusCached(LNTROProcessOutput oldOutput) {
        Flux<List<String>> data = oldOutput.getData();

        // Convert All Data to Cached Corpus - Result : Data -> Corpus
        Flux<Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>> cachedCorpus = data
                .doOnSubscribe((t) -> {
                    LOGGER.info("Getting All Cached Corpus from Data");
                })
                .parallel()
                // // .runOn(Schedulers.parallel())
                .map(d -> (Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>) new AbstractMap.SimpleEntry<>(
                        d,
                        corpusConstruction.getCorpusFromCache(d)
                                .cache(Integer.MAX_VALUE)
                                .collectList().flatMapMany(Flux::fromIterable)
                                .share()
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .collectList().flatMapMany(Flux::fromIterable)
                .share();

        // Convert All Corpus to Preprocessed - Result : Data -> Preprocessed Corpus
        Flux<Map.Entry<List<String>, Flux<String>>> preprocessedCorpus = cachedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Preprocessing Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        preprocessor.preprocess(e.getValue().map(CorpusConstruction.CorpusItem::getContent), getCorpusPreprocessing())
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        // Filter Sentences of Corpus that does not contain words from data - Result : Data -> Filtered Corpus
        Flux<Map.Entry<List<String>, Flux<String>>> highlightedCorpus = preprocessedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Highlighting Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        preprocessor.filterHighlight(e.getKey(), e.getValue())
                                .flatMap(t -> t)
                                .filter(s -> s.trim().length() > 0)
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(oldOutput.getData());
        output.setRawCorpus(cachedCorpus);
        output.setCorpus(highlightedCorpus);
        return output;
    }

    /* [DUPLICATE] FROM constructCorpus */
    public LNTROProcessOutput constructCorpusSearched(LNTROProcessOutput oldOutput) {
        Flux<List<String>> data = oldOutput.getData();

        // Convert All Data to Searched Corpus - Result : Data -> Corpus
        Flux<Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>> searchedCorpus = data
                .doOnSubscribe((t) -> {
                    LOGGER.info("Getting All Searched Corpus from Data");
                })
                /*
                .collectList()
                .map(dataList -> {
                    List<Map.Entry<List<String>, Flux<String>>> corpusList = new ArrayList<>();
                    dataList.forEach(d -> {
                        corpusList.add(new AbstractMap.SimpleEntry<>(
                                d,
                                corpusConstruction.getCorpusFromSource(d)
                        ));
                    });
                    return corpusList;
                })
                .flatMapMany(Flux::fromIterable)
                */
                .map(d -> (Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>) new AbstractMap.SimpleEntry<>(
                        d,
                        corpusConstruction.getCorpusFromSource(d)
                                .cache(Integer.MAX_VALUE)
                                .collectList().flatMapMany(Flux::fromIterable)
                                .share()
                ))
                .cache(Integer.MAX_VALUE)
                .collectList().flatMapMany(Flux::fromIterable)
                .share();

        // Convert All Corpus to Preprocessed - Result : Data -> Preprocessed Corpus
        Flux<Map.Entry<List<String>, Flux<String>>> preprocessedCorpus = searchedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Preprocessing Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        preprocessor.preprocess(e.getValue().map(CorpusConstruction.CorpusItem::getContent), getCorpusPreprocessing())
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        // Filter Sentences of Corpus that does not contain words from data - Result : Data -> Filtered Corpus
        Flux<Map.Entry<List<String>, Flux<String>>> highlightedCorpus = preprocessedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Highlighting Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        preprocessor.filterHighlight(e.getKey(), e.getValue())
                                .flatMap(t -> t)
                                .filter(s -> s.trim().length() > 0)
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(oldOutput.getData());
        output.setRawCorpus(searchedCorpus);
        output.setCorpus(highlightedCorpus);
        return output;
    }

    /* [DUPLICATE] FROM constructCorpus */
    public LNTROProcessOutput constructCorpusCachedSearched(LNTROProcessOutput oldOutput) {
        Flux<List<String>> data = oldOutput.getData();

        // Convert All Data to Corpus (Cached and Searched Combined) - Result : Data -> Corpus
        Flux<Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>> corpus = data
                .doOnSubscribe((t) -> {
                    LOGGER.info("Getting All Combined Corpus from Data");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(d -> (Map.Entry<List<String>, Flux<CorpusConstruction.CorpusItem>>) new AbstractMap.SimpleEntry<>(
                        d,
                        corpusConstruction.getCorpus(d)
                                .cache(Integer.MAX_VALUE)
                                .collectList().flatMapMany(Flux::fromIterable)
                                .share()
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .collectList().flatMapMany(Flux::fromIterable)
                .share();

        // Convert All Corpus to Preprocessed - Result : Data -> Preprocessed Corpus
        Flux<Map.Entry<List<String>, Flux<String>>> preprocessedCorpus = corpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Preprocessing Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        preprocessor.preprocess(e.getValue().map(CorpusConstruction.CorpusItem::getContent), getCorpusPreprocessing())
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        // Filter Sentences of Corpus that does not contain words from data - Result : Data -> Filtered Corpus
        Flux<Map.Entry<List<String>, Flux<String>>> highlightedCorpus = preprocessedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Highlighting Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        preprocessor.filterHighlight(e.getKey(), e.getValue())
                                .flatMap(t -> t)
                                .filter(s -> s.trim().length() > 0)
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(oldOutput.getData());
        output.setRawCorpus(corpus);
        output.setCorpus(highlightedCorpus);
        return output;
    }

    public LNTROProcessOutput constructCorpus(LNTROProcessOutput oldOutput) {
        return constructCorpusCachedSearched(oldOutput);
    }

    public LNTROProcessOutput constructCorpus(Flux<List<String>> rows) {
        return constructCorpus(constructData(rows));
    }

    public LNTROProcessOutput annotateCorpus(LNTROProcessOutput oldOutput) {
        Flux<Map.Entry<List<String>, Flux<String>>> highlightedCorpus = oldOutput.getCorpus();

        // Annotate Sentences of Corpus - Result : Data -> Annotated Corpus
        Flux<Map.Entry<List<String>, Flux<Flux<TaggedWord>>>> annotatedCorpus = highlightedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Annotating All Corpus");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<Flux<TaggedWord>>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        corpusAnnotation.annotate(e.getValue())
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        // Get only Verbs (each corpus must contain only one verb) - Result : Data -> Verbs Array
        Flux<Map.Entry<List<String>, Flux<String>>> verbsCorpus = annotatedCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Getting Only Verbs");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Flux<String>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        e.getValue()
                                .map(tw -> corpusAnnotation.getVerbs(tw))
                                .filterWhen(v -> v.collectList().map(l -> l.size() == 1))
                                .flatMap(t -> t)
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        // Build Term Frequency Map - Result : Data -> Term Frequency Map
        Flux<Map.Entry<List<String>, Mono<TermFrequencyMap>>> termFrequencyMapCorpus = verbsCorpus
                .doOnSubscribe((t) -> {
                    LOGGER.info("Building Term Frequency Map");
                })
                .parallel()
                // .runOn(Schedulers.parallel())
                .map(e -> (Map.Entry<List<String>, Mono<TermFrequencyMap>>) new AbstractMap.SimpleEntry<>(
                        e.getKey(),
                        Mono.just(new TermFrequencyMap(e.getValue()))
                ))
                .sequential()
                .cache(Integer.MAX_VALUE)
                .share();

        termFrequencyMapCorpus.subscribe(t -> {
            t.getValue().subscribe(termFrequencyMap -> {
            });
        });

        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(oldOutput.getData());
        output.setRawCorpus(oldOutput.getRawCorpus());
        output.setCorpus(oldOutput.getCorpus());
        output.setAnnotatedCorpus(annotatedCorpus);
        output.setTermFrequencyMaps(termFrequencyMapCorpus);
        return output;
    }

    public LNTROProcessOutput annotateCorpus(Flux<List<String>> rows) {
        return annotateCorpus(constructCorpus(rows));
    }

    public Mono<TermScoreMap> finalScoreFilterKBBI(TermScoreMap scores) {
        return Flux.fromIterable(scores.entrySet())
                .publishOn(Schedulers.parallel())
                .filterWhen(entry -> KBBI.getDocumentKBBI(entry.getKey())
                        .map(DocumentKBBI::getPosTag)
                        .onErrorReturn(POSTagSet.UNKNOWN)
                        .map(posTag -> posTag.equals(POSTagSet.VERB))
                )
                .collectList()
                .map(entries -> {
                    TermScoreMap result = new TermScoreMap();
                    entries.forEach(entry -> {
                        result.put(entry.getKey(), entry.getValue());
                    });
                    return result;
                });
    }

    public Mono<TermScoreMap> finalScoreFilterStopwords(TermScoreMap scores, Set<String> stopwords) {
        return preprocessor.removeStopwords(stopwords, Flux.fromIterable(scores.termsSet()))
                .map(String::trim)
                .filter(t -> t.length() > 0)
                .collectList()
                .map(strings -> {
                    TermScoreMap result = new TermScoreMap();
                    strings.forEach(s -> {
                        result.put(s, scores.get(s));
                    });
                    return result;
                });
    }

    public Mono<TermFrequencyMap> finalScoreFilterStopwords(TermFrequencyMap frequencies, Set<String> stopwords) {
        return preprocessor.removeStopwords(stopwords, Flux.fromIterable(frequencies.termsSet()))
                .map(String::trim)
                .filter(t -> t.length() > 0)
                .collectList()
                .map(strings -> {
                    TermFrequencyMap result = new TermFrequencyMap();
                    strings.forEach(s -> {
                        result.put(s, frequencies.get(s));
                    });
                    return result;
                });
    }

    public LNTROProcessOutput extract(LNTROProcessOutput oldOutput) {
        int topExtractionTermThreshold = lntroProperties.getEvaluation().getTopSearch().getTopTermCount();
        Flux<Map.Entry<List<String>, Mono<TermFrequencyMap>>> termFrequencyMapCorpus = oldOutput.getTermFrequencyMaps();

        Flux<TermFrequencyMap> flatTermFrequencyMapCorpus = termFrequencyMapCorpus.flatMap(Map.Entry::getValue);

        Flux<Map.Entry<String, Mono<TermScoreMap>>> extractionScores = Flux.fromStream(lntroExtractions
                .entrySet().stream()
                .parallel()
                .map(entry -> new AbstractMap.SimpleEntry<>(
                        entry.getKey(),
                        entry.getValue()
                                .getFinalScores(flatTermFrequencyMapCorpus
                                        .flatMap(m -> finalScoreFilterStopwords(m, extractionStopwords)))
                                .flatMap(this::finalScoreFilterKBBI)
                                .doOnSubscribe((t) -> {
                                    LOGGER.info("Extracting Final Scores with method '" + entry.getKey() + "'");
                                }).cache()
                ))
                .map(entry -> (Map.Entry<String, Mono<TermScoreMap>>) entry)
        ).cache(Integer.MAX_VALUE).share();

        Flux<Map.Entry<String, Mono<List<BahasaActivePassiveForm>>>> topExtractionTerms = extractionScores
                .map(entry -> new AbstractMap.SimpleEntry<>(
                        entry.getKey(),
                        entry.getValue()
                                .map(TermScoreMap::toAtomicTermList)
                                .map(atomicTermList -> atomicTermList.size() > topExtractionTermThreshold
                                        ? atomicTermList.subList(0, topExtractionTermThreshold)
                                        : atomicTermList
                                )
                                .map(atomicTermList -> atomicTermList
                                        .stream()
                                        .map(atomicTermScore -> stemmer.getActivePassiveForm(atomicTermScore.TERM))
                                        .collect(Collectors.toList())
                                )
                                .doOnSubscribe((t) -> {
                                    LOGGER.info("Extracting Top Extraction Terms '" + entry.getKey() + "'");
                                }).cache()
                ));

        Mono<BahasaActivePassiveForm> lntroTermResult = topExtractionTerms
                //.map(entry -> entry.getValue().map(list -> list.get(ThreadLocalRandom.current().nextInt(list.size()))))
                .flatMap(entry -> entry.getValue().map(list -> list.get(0)))
                .collectList()
                .map(list -> list.get(ThreadLocalRandom.current().nextInt(list.size())))
                .doOnNext((t) -> {
                    LOGGER.info("Getting term result : '" + t.getWord() + "'");
                }).cache();

        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(oldOutput.getData());
        output.setRawCorpus(oldOutput.getRawCorpus());
        output.setCorpus(oldOutput.getCorpus());
        output.setAnnotatedCorpus(oldOutput.getAnnotatedCorpus());
        output.setTermFrequencyMaps(oldOutput.getTermFrequencyMaps());
        output.setExtractionScores(extractionScores);
        output.setTopExtractionTerms(topExtractionTerms);
        output.setLntroTermResult(lntroTermResult);
        return output;
    }

    public LNTROProcessOutput extract(Flux<List<String>> rows) {
        return extract(annotateCorpus(rows));
    }

    public Flux<List<String>> randomSampling(Flux<List<String>> rows, int randomDataSamplingCount) {
        LOGGER.debug("Random Sampling Init");
        Flux<List<String>> result = Flux.create(emitter -> {
            rows.collectList().doFinally(signalType -> emitter.complete()).subscribe(lists -> {
                int samplingCount = 0;
                int realRandomDataSamplingCount = Math.min(randomDataSamplingCount, lists.size());
                try {
                    while (samplingCount < realRandomDataSamplingCount) {
                        int randomIndex = ThreadLocalRandom.current().nextInt(lists.size());
                        emitter.next(lists.get(randomIndex));
                        lists.remove(randomIndex);
                        samplingCount++;
                    }
                } catch (Exception e) {
                }
                LOGGER.debug("Random Sampling Complete");
                emitter.complete();
            });
        });

        return result
                .collectSortedList(Comparator.comparing(a -> a.get(0)))
                .flatMapMany(Flux::fromIterable);
    }

    public LNTROProcessOutput evaluate(LNTROProcessOutput oldOutput) {
        int randomSamplingCount = lntroProperties.getEvaluation().getTopSearch().getRandomDataSamplingCount();

        Flux<List<String>> data = oldOutput.getData();
        Flux<Map.Entry<String, Mono<List<BahasaActivePassiveForm>>>> topExtractionTerms = oldOutput.getTopExtractionTerms();

        Flux<List<String>> evaluationDataSample = randomSampling(data, randomSamplingCount).cache(randomSamplingCount).share();
        Flux<Map.Entry<String, Mono<EvaluationOutput>>> evaluationScores = topExtractionTerms
                .map(entry -> new AbstractMap.SimpleEntry<>(
                        entry.getKey(),
                        entry.getValue()
                                .flatMap(terms -> lntroEvaluation.evaluate(evaluationDataSample, terms))
                                .doOnSubscribe((t) -> {
                                    LOGGER.info("Evaluating Top Terms with method '" + entry.getKey() + "'");
                                }).cache()
                ));

        LNTROProcessOutput output = new LNTROProcessOutput();
        output.setData(oldOutput.getData());
        output.setRawCorpus(oldOutput.getRawCorpus());
        output.setCorpus(oldOutput.getCorpus());
        output.setAnnotatedCorpus(oldOutput.getAnnotatedCorpus());
        output.setTermFrequencyMaps(oldOutput.getTermFrequencyMaps());
        output.setExtractionScores(oldOutput.getExtractionScores());
        output.setTopExtractionTerms(topExtractionTerms);
        output.setLntroTermResult(oldOutput.getLntroTermResult());
        output.setEvaluationDataSample(evaluationDataSample);
        output.setEvaluationScores(evaluationScores);
        return output;
    }

    public LNTROProcessOutput evaluate(Flux<List<String>> rows) {
        return evaluate(extract(rows));
    }

    public BahasaStemmer getStemmer() {
        return stemmer;
    }

    public void setStemmer(BahasaStemmer stemmer) {
        this.stemmer = stemmer;
    }

    public Preprocessor getPreprocessor() {
        return preprocessor;
    }

    public void setPreprocessor(Preprocessor preprocessor) {
        this.preprocessor = preprocessor;
    }

    public CorpusConstruction getCorpusConstruction() {
        return corpusConstruction;
    }

    public void setCorpusConstruction(CorpusConstruction corpusConstruction) {
        this.corpusConstruction = corpusConstruction;
    }

    public CorpusAnnotation getCorpusAnnotation() {
        return corpusAnnotation;
    }

    public void setCorpusAnnotation(CorpusAnnotation corpusAnnotation) {
        this.corpusAnnotation = corpusAnnotation;
    }

    public Map<String, LNTROExtraction> getLntroExtractions() {
        return lntroExtractions;
    }

    public void addLntroExtraction(String ID, LNTROExtraction lntroExtraction) {
        this.lntroExtractions.put(ID, lntroExtraction);
    }

    public LNTROEvaluation getLntroEvaluation() {
        return lntroEvaluation;
    }

    public void setLntroEvaluation(LNTROEvaluation lntroEvaluation) {
        this.lntroEvaluation = lntroEvaluation;
    }

    public Set<String> getExtractionStopwords() {
        return extractionStopwords;
    }

    public void setExtractionStopwords(Set<String> extractionStopwords) {
        this.extractionStopwords = extractionStopwords;
    }

    public KBBIMongoCacher getKBBI() {
        return KBBI;
    }

    public void setKBBI(KBBIMongoCacher KBBI) {
        this.KBBI = KBBI;
    }

    public LNTROProperties getLntroProperties() {
        return lntroProperties;
    }

    public void setLntroProperties(LNTROProperties lntroProperties) {
        this.lntroProperties = lntroProperties;
    }
}
