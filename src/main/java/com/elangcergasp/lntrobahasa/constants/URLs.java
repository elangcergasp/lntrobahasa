/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.constants;

/**
 * @author elangcergasp
 */
public class URLs {

    public static class TestController {

        public static final String PREFIX = "/test";

        public static final String DATA
                = PREFIX + "/data";
        public static final String ROWS
                = PREFIX + "/rows";

        public static final String CONSTRUCT
                = PREFIX + "/construct";
        public static final String CONSTRUCT_CACHED_SEARCHED
                = CONSTRUCT + "/cached_searched";
        public static final String CONSTRUCT_CACHED
                = CONSTRUCT + "/cached";
        public static final String CONSTRUCT_SEARCHED
                = CONSTRUCT + "/searched";

        public static final String ANNOTATE
                = PREFIX + "/annotate";
        public static final String ANNOTATE_CACHED_SEARCHED
                = ANNOTATE + "/cached_searched";
        public static final String ANNOTATE_CACHED
                = ANNOTATE + "/cached";
        public static final String ANNOTATE_SEARCHED
                = ANNOTATE + "/searched";

        public static final String EXTRACT
                = PREFIX + "/extract";
        public static final String EXTRACT_CACHED_SEARCHED
                = EXTRACT + "/cached_searched";
        public static final String EXTRACT_CACHED
                = EXTRACT + "/cached";
        public static final String EXTRACT_SEARCHED
                = EXTRACT + "/searched";
    }

    public static class FunctionController {

        public static final String PREFIX = "/function";

        public static final String SEARCH
                = PREFIX + "/search";

        public static final String SEARCH_CACHING
                = PREFIX + "/search_caching";

        public static final String KBBI
                = PREFIX + "/kbbi";
    }

    public static class LNTROController {

        public static final String PREFIX = "/lntro";

        public static final String SUBMIT
                = PREFIX + "/submit";

        public static final String PROCESS_LIST
                = PREFIX + "/processes";

        public static final String PROCESS_DETAIL
                = PREFIX + "/process";

        public static final String PROCESS_QUEUE
                = PREFIX + "/queue";

    }

    public static class DownloadController {

        public static final String PREFIX = "/download";

        public static final String ONTOLOGY
                = PREFIX + "/ontology";

    }

    public static class PropertiesController {

        public static final String PREFIX = "/properties";

        public static final String VALID_CSV_SEPARATORS
                = PREFIX + "/valid_csv_separators";

        public static final String SEARCH_ENGINES
                = PREFIX + "/search_engines";

        public static final String CONSTRUCTION_METHODS
                = PREFIX + "/construction_methods";

        public static final String POS_TAGGERS
                = PREFIX + "/pos_taggers";

        public static final String EXTRACTION_METHODS
                = PREFIX + "/extraction_methods";

        public static final String POS_TAG_INFO
                = PREFIX + "/pos_tag_info";

    }

    public static class DataSeederController {

        public static final String PREFIX = "/seeder";

        public static final String SEARCH_RESULT_ITEM
                = PREFIX + "/search_result_item";

        public static final String SEARCH_RESULT_STAT
                = PREFIX + "/search_result_stat";

        public static final String KBBI
                = PREFIX + "/kbbi";

        public static final String CREATE_URL
                = PREFIX + "/create_url";

        public static final String SEARCH_ERROR_BUFFER_URLS
                = PREFIX + "/search_error_buffer_urls";

        public static final String SEARCH_ERROR_BUFFER_CLEAR
                = PREFIX + "/search_error_buffer_clear";
    }
}
