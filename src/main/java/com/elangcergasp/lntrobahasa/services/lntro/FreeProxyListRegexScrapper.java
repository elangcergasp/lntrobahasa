package com.elangcergasp.lntrobahasa.services.lntro;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FreeProxyListRegexScrapper extends ProxySelector {

    private static final Logger LOGGER = LoggerFactory.getLogger(FreeProxyListRegexScrapper.class);

    public static final String FREE_PROXY_LIST_URL = "http://spys.me/proxy.txt";
    public static final String REGEX_IP_PORT = "((?:\\d{1,3})(?:\\.\\d{1,3}){3}):(\\d{2,5})\\D+\\+\\s*";

    private Long lastFetch = System.currentTimeMillis();
    private static Long CACHE_TTL = 60L * 60L * 1000L;
    private List<Proxy> cacheProxies = null;

    public FreeProxyListRegexScrapper() {
        try {
            select(URI.create(FREE_PROXY_LIST_URL));
        } catch (Exception e) {
            cacheProxies = new ArrayList<>();
        }
    }

    @Override
    public List<Proxy> select(URI uri) {
        if (cacheProxies != null && lastFetch > (System.currentTimeMillis() - CACHE_TTL)) {
            return cacheProxies;
        }

        List<Proxy> proxies = new ArrayList<>();
        LOGGER.debug("Start Proxy List Scrapping : " + FREE_PROXY_LIST_URL);
        try {
            String txt = Jsoup.connect(FREE_PROXY_LIST_URL).get().outerHtml();
            Matcher m = Pattern.compile(REGEX_IP_PORT).matcher(txt);
            while (m.find()) {
                String ip = m.group(1);
                int port = Integer.parseInt(m.group(2));
                proxies.add(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port)));
            }
            cacheProxies = proxies;
            lastFetch = System.currentTimeMillis();
            LOGGER.debug("Proxy List : " + cacheProxies);
        } catch (Exception e) {
            LOGGER.error("Proxy Scrapping failed - " + e.getMessage());
        }
        return cacheProxies;
    }

    @Override
    public void connectFailed(URI uri, SocketAddress socketAddress, IOException e) {

    }
}
