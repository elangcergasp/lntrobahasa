/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.lntro;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTag;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTagSet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Elang Cergas Pembrani
 */
@Service
public class MyKBBI {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyKBBI.class);

    public static final String KBBI_ENDPOINT = "https://www.kbbi.web.id/";

    public static final String DESCRIPTION_SELECTOR = "#d1";
    public static final String INVALID_CHAR = "[^a-zA-Z\\-\\ ]";
    public static final String HIGHLIGHTED_CANDIDATE_MAIN_SELECTOR = "b";
    public static final String HIGHLIGHTED_CANDIDATE_SUB_SELECTOR = "br + b";
    public static final String TERM_SELECTOR = "input#w";
    public static final String POS_TAG_SELECTOR = "em";

    public static final Integer CRAWLER_REFRESH_AFTER_COUNTER = 10;

    public static final Integer CRAWLER_RETRY_DELAY = 500;

    public static final Integer CRAWLER_RETRY_LIMIT = 3;

    public static final Integer BASE_TIMEOUT = 30000; // ms

    protected final WebCrawler crawler = new WebCrawler(KBBI_ENDPOINT);

    private int crawlerRefreshCounter = 0;

    public void crawlerRefreshConnection() {
        crawler.refreshConnection();
        crawlerRefreshCounter = 0;
    }

    public void crawlerRefreshIfAfterCounter() {
        if (crawlerRefreshCounter > CRAWLER_REFRESH_AFTER_COUNTER) {
            crawlerRefreshConnection();
        } else {
            crawlerRefreshCounter++;
        }
    }

    @Cacheable(value = "KBBIDocumentCache", key = "#term")
    public String getDocumentAsString(String term) {
        Mono<String> result = Mono.create(emitter -> {
            Flux<Long> attempts = Flux.interval(Duration.ZERO, Duration.ofMillis(CRAWLER_RETRY_DELAY))
                    .onBackpressureLatest()
                    //.timeout(Duration.ofMillis(BASE_TIMEOUT + CRAWLER_RETRY_DELAY))
                    .publishOn(Schedulers.parallel());

            //attempts.take(searchEngine.getParams().getRetryLimit()).subscribe(new Subscriber<Long>() {
            attempts.subscribe(new Subscriber<Long>() {
                private Subscription subscription;
                private int retryCount;

                @Override
                public void onSubscribe(Subscription s) {
                    LOGGER.debug("[KBBI] Getting KBBI Document for term : " + term);
                    this.subscription = s;
                    this.retryCount = -1;

                    s.request(1);
                }

                @Override
                public void onNext(Long attemptCount) {
                    retryCount++;
                    if (retryCount > CRAWLER_RETRY_LIMIT) {
                        subscription.cancel();
                        return;
                    }
                    LOGGER.debug("[KBBI] Attempting [" + (retryCount + 1) + "] Document '" + term + "'");
                    Mono<String> kbbiDocument = Mono.fromCallable(() -> {
                        return crawler.GET(term).execute().body();
                    }).doOnError(e -> {
                        LOGGER.error("[KBBI] Attempt [" + (retryCount + 1) + "] Document '" + term + "' FAILED - Message: " + e.getMessage());
                        crawlerRefreshConnection();

                        if (retryCount < CRAWLER_RETRY_LIMIT) {
                            LOGGER.debug("[KBBI] Waiting " + CRAWLER_RETRY_DELAY + " ms for next attempt");
                            subscription.request(1);
                        } else {
                            LOGGER.error("[KBBI] Failed to fetch Document '" + term + "' in " + retryCount + " retries");
                            emitter.success();
                            subscription.cancel();
                        }
                    });
                    kbbiDocument.subscribe(document -> {
                        LOGGER.debug("[KBBI] Document fetched '" + term + "'");
                        emitter.success(document);
                        subscription.cancel();
                    });
                }

                @Override
                public void onError(Throwable t) {
                    LOGGER.error("[KBBI] Failed to fetch Document - " + t.getMessage());
                    emitter.success();
                }

                @Override
                public void onComplete() {
                    emitter.success();
                }
            });
        });

        return result.block();
    }

    public Document getDocument(String term) {
        try {
            return Jsoup.parse(getDocumentAsString(term));
        } catch (Exception ex) {
            return null;
        }
    }

    public String getTermFromDocument(Document document) {
        return document == null ? null : document.selectFirst(TERM_SELECTOR).val();
    }

    public Element getDescription(Document document) {
        return document == null ? null : document.selectFirst(DESCRIPTION_SELECTOR);
    }

    public Element getDescription(String term) {
        return getDescription(getDocument(term));
    }

    public boolean isValidTerm(Document document) {
       Element descriptionElement = getDescription(document);
       if (descriptionElement != null) {
           String descriptionString = descriptionElement.html().trim();
           return descriptionString.length() > 0;
       }
       return false;
    }

    public String getReference(Document document) {
        try {
            // String term = getTermFromDocument(document);
            Element descriptionElement = getDescription(document);
            if (descriptionElement != null) {
                String descriptionString = descriptionElement.text().trim();
                String[] splitReference = descriptionString.split(" lihat ");
                return splitReference.length > 1 ? splitReference[1].trim() : null;
            }
        } catch (Exception e) {}
        return null;
    }

    public Elements getMainHighlightedElements(Document document) {
        Elements result = null;
        try {
            Element description = getDescription(document);
            Element highlighted = description.selectFirst(HIGHLIGHTED_CANDIDATE_MAIN_SELECTOR);
            if (highlighted != null) {
                Elements siblings = highlighted.parent().children();
                int highlightedIndex = highlighted.elementSiblingIndex();
                int afterHighlightedIndex = highlighted.lastElementSibling().elementSiblingIndex();
                for (Element sibling : siblings) {
                    int index = sibling.elementSiblingIndex();
                    if (index > highlightedIndex) {
                        if (sibling.is("br")) {
                            afterHighlightedIndex = index;
                            break;
                        }
                    }
                }
                Elements nodes = highlighted.parent().children();
                result = new Elements(nodes.subList(highlightedIndex, afterHighlightedIndex));
            }
        } catch (Exception e) {}
        return result;
    }

    public Elements getHighlightedElements(Document document) {
        String term = getTermFromDocument(document);
        Elements result = null;
        try {
            Element description = getDescription(document);
            Element candidateMain = description.selectFirst(HIGHLIGHTED_CANDIDATE_MAIN_SELECTOR);
            Elements candidateSub = description.select(HIGHLIGHTED_CANDIDATE_SUB_SELECTOR);
            Element highlighted = null;
            if (candidateMain != null) {
                if (candidateMain.text().replaceAll(INVALID_CHAR, "").equals(term)) {
                    highlighted = candidateMain;
                }
            }
            if (candidateSub != null) {
                for (Element element : candidateSub) {
                    if (element.text().replaceAll(INVALID_CHAR, "").equals(term)) {
                        highlighted = element;
                        break;
                    }
                }
            }
            if (highlighted != null) {
                Elements siblings = highlighted.parent().children();
                int highlightedIndex = highlighted.elementSiblingIndex();
                int afterHighlightedIndex = highlighted.lastElementSibling().elementSiblingIndex();
                for (Element sibling : siblings) {
                    int index = sibling.elementSiblingIndex();
                    if (index > highlightedIndex) {
                        if (sibling.is("br")) {
                            afterHighlightedIndex = index;
                            break;
                        }
                    }
                }
                Elements nodes = highlighted.parent().children();
                result = new Elements(nodes.subList(highlightedIndex, afterHighlightedIndex));
            }
        } catch (Exception e) {}
        return result;
    }

    public Elements getHighlightedElements(String term) {
        return getHighlightedElements(getDocument(term));
    }

    public POSTag getHighlightedPOSTag(Document document) {
        POSTag result = POSTagSet.UNKNOWN;

        try {
            Elements highlightedElements = getHighlightedElements(document);
            if (highlightedElements != null) {
                Element highlightedPOSTag = highlightedElements.select(POS_TAG_SELECTOR).first();
                if (highlightedPOSTag != null) {
                    List<String> highlightedAttribute = Arrays.stream(highlightedPOSTag
                            .text()
                            .toLowerCase()
                            .split("\\s"))
                            .collect(Collectors.toList());

                    if (highlightedAttribute.contains("adv")) {
                        result = POSTagSet.ADVERB;
                    } else if (highlightedAttribute.contains("a")) {
                        result = POSTagSet.ADJECTIVE;
                    } else if (highlightedAttribute.contains("p")) {
                        result = POSTagSet.PARTICLE;
                    } else if (highlightedAttribute.contains("num")) {
                        result = POSTagSet.NUMERAL;
                    } else if (highlightedAttribute.contains("pron")) {
                        result = POSTagSet.PRONOUN;
                    } else if (highlightedAttribute.contains("n")) {
                        result = POSTagSet.NOUN;
                    } else if (highlightedAttribute.contains("v")) {
                        result = POSTagSet.VERB;
                    }
                }
            }
        } catch (Exception e) {}
        return result;
    }

    public POSTag getHighlightedPOSTag(String term) {
        return getHighlightedPOSTag(getDocument(term));
    }

}
