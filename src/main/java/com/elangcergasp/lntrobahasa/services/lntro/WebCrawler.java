/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.lntro;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class WebCrawler implements RandomUserAgent {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebCrawler.class);

    public static final String DEFAULT_POST_CONTENT_TYPE = "application/x-www-form-urlencoded";

    // public static final ProxySelector proxySelector = new FreeProxyListHTMLScrapper();
    // public static final ProxySelector proxySelector = new FreeProxyListRegexScrapper();
    public static final ProxySelector proxySelector = new FreeProxyListRegexStream(
            FreeProxyListRegexStream.fileTextInputStream("./data/Indo_Proxy2.txt")
    );

    /*
    public static final String DEFAULT_PAC_PROXY = "https://www.proxynova.com/proxy.pac";

    private static final int DEFAULT_PAC_CACHE_SIZE = 50;

    private static final long DEFAULT_PAC_CACHE_TTL = 1000 * 60 * 10; // 10 Minutes

    private static final BufferedProxySelector.CacheScope DEFAULT_PAC_CACHE_SCOPE = BufferedProxySelector.CacheScope.CACHE_SCOPE_HOST;

    static {
        UrlPacScriptSource pacScriptSource = new UrlPacScriptSource(DEFAULT_PAC_PROXY);
        ProxySelector proxySelector = new PacProxySelector(pacScriptSource);

        //proxySelector = new BufferedProxySelector(DEFAULT_PAC_CACHE_SIZE, DEFAULT_PAC_CACHE_TTL, proxySelector, DEFAULT_PAC_CACHE_SCOPE);
        proxySelector = new ProxyListFallbackSelector(proxySelector);

        ProxySelector.setDefault(proxySelector);
    };
    */

    private boolean usingProxy = true;
    private Proxy proxy = Proxy.NO_PROXY;
    private String endpoint;
    private String userAgent;

    public WebCrawler(String endpoint) {
        setEndpoint(endpoint);
        setUsingProxy(usingProxy);
        changeUserAgentToRandom();
        // refreshConnection();
    }

    public WebCrawler(String endpoint, boolean usingProxy) {
        setEndpoint(endpoint);
        setUsingProxy(usingProxy);
        changeUserAgentToRandom();
        // refreshConnection();
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint.replaceAll("/$", "") + "/";
    }
    
    public void refreshConnection() {
        refreshConnection(endpoint);
    }

    public void refreshConnection(String uri) {
        this.changeUserAgentToRandom();

        if (usingProxy) {
            this.changeProxyToRandom(uri);
        }
    }

    public void changeUserAgentToRandom() {
        userAgent = getRandomUserAgent();
        LOGGER.debug("Changed User Agent to : " + userAgent);
    }

    public void changeProxyToRandom(String uri) {
        proxy = getRandomProxy(uri);
        LOGGER.debug("Changed Proxy to : " + proxy.toString());
    }

    public void setUsingProxy(boolean usingProxy) {
        if (this.usingProxy != usingProxy) {
            if (usingProxy) {
                this.changeProxyToRandom(endpoint);
            } else {
                proxy = Proxy.NO_PROXY;
            }
        }
        this.usingProxy = usingProxy;
    }

    public Proxy getRandomProxy(String uri) {
        Proxy randomProxy = Proxy.NO_PROXY;

        double chanceDirect = 0.1;
        double flipDirectChance = 1 / chanceDirect;
        if (ThreadLocalRandom.current().nextInt((int) flipDirectChance) == 0) {
            return randomProxy;
        }

        double chanceTor = 0.05;
        double flipTorChance = 1 / chanceTor;
        if (ThreadLocalRandom.current().nextInt((int) flipTorChance) == 0) {
            return new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 8118));
        }

        // Get list of proxies from default ProxySelector available for given URL
        List<Proxy> proxies = proxySelector.select(URI.create(uri));

        List<Proxy> httpProxies = proxies.stream()
                .filter(p -> p.type().equals(Proxy.Type.HTTP))
                .collect(Collectors.toList());
        if (httpProxies.size() > 0) {
            //LOGGER.debug("Proxy list is : " + httpProxies);
            randomProxy = httpProxies.get(ThreadLocalRandom.current().nextInt(httpProxies.size()));
        } else {
            List<Proxy> directProxies = proxies.stream()
                    .filter(p -> p.type().equals(Proxy.Type.DIRECT))
                    .collect(Collectors.toList());
            if (directProxies.size() > 0) {
                randomProxy = directProxies.get(ThreadLocalRandom.current().nextInt(directProxies.size()));
            }
        }

        return randomProxy;
    }

    public String buildEndpoint(String uriSegment) {
        return endpoint == null ? uriSegment :
                (uriSegment.startsWith("http://") || uriSegment.startsWith("https://") ? uriSegment : endpoint + uriSegment);
    }

    public String buildEndpoint(String uriSegment, Map<String, String> queryData) {
        return queryData.entrySet().stream().reduce(
                UriComponentsBuilder.fromUriString(buildEndpoint(uriSegment)),
                (ucb, entry) -> ucb.queryParam(entry.getKey(), entry.getValue()),
                (ucb1, ucb2) -> ucb2
        ).build().toString();
    }

    public Connection connect(String uriSegment, Map<String, String> data) {
        return connect(buildEndpoint(uriSegment))
                .data(data);
                //.validateTLSCertificates(false);
    }
    
    public Connection connect(String uriSegment) {
        return Jsoup.connect(buildEndpoint(uriSegment))
                .referrer(buildEndpoint(""))
                .userAgent(userAgent)
                .proxy(proxy);
    }
    
    public Connection GET(String uriSegment, Map<String, String> data) {
        return connect(uriSegment, data)
                .method(Connection.Method.GET);
    }
    
    public Connection GET(String uriSegment) {
        return connect(uriSegment)
                .method(Connection.Method.GET);
    }
    
    public Connection POST(String uriSegment, Map<String, String> data) {
        return connect(uriSegment, data)
                .header("Content-Type", DEFAULT_POST_CONTENT_TYPE)
                .method(Connection.Method.POST);
    }
    
    public Connection POST(String uriSegment) {
        return connect(uriSegment)
                .method(Connection.Method.POST);
    }
    
    public Map<String, Elements> parseElements(Document doc, String... selectors) {
        return Stream.of(selectors)
                .parallel()
                .collect(Collectors.toMap(k -> k, s -> doc.select(s)));
    }
    
    public Elements parseElementsFlattened(Document doc, String... selectors) {
        return Stream.of(selectors)
                .parallel()
                .map(doc::select)
                .map(Collection::stream)
                .reduce(Stream.empty(), Stream::concat)
                .collect(Collectors.toCollection(Elements::new));
    }

    public Proxy getCurrentProxy() {
        return proxy;
    }
}
