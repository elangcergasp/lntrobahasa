/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.lntro;

import com.elangcergasp.lntrobahasa.model.SearchEngine;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Elang Cergas Pembrani
 */
@EqualsAndHashCode
@ToString
public abstract class SearchEngineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchEngineRunner.class);

    public static final int DEFAULT_SEARCH_RESULT_LIMIT = 100;

    protected final SearchEngine searchEngine;

    public SearchEngineRunner(SearchEngine searchEngine) {
        this.searchEngine = searchEngine;
    }

    public abstract String createKeyword(Collection<String> data, String relation);
    public abstract String createKeyword(Collection<String> data);

    public String createKeyword(String... data) {
        return createKeyword(Stream.of(data).collect(Collectors.toList()));
    }

    public abstract Flux<SearchResultItem> getSearchResult(String keyword);

    public abstract Flux<SearchResultItem> getSearchResult(String keyword, int limit);

    public abstract Flux<SearchResultItem> getSearchResult(String keyword, int initialOffset, int limit);

    public abstract Mono<SearchResultStat> getSearchResultStat(Collection<String> data);
    public abstract Mono<SearchResultStat> getSearchResultStat(String keyword);

    public SearchEngine getSearchEngine() {
        return searchEngine;
    }

}
