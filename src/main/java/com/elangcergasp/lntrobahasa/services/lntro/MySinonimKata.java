/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.lntro;

import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Elang Cergas Pembrani
 */
public class MySinonimKata {

    protected static String SINONIM_KATA_ENDPOINT = "http://www.sinonimkata.com/";

    protected static String QUERY_FORM_ENDPOINT = "search.php";
    protected static String QUERY_INPUT_NAME = "q";
    protected static String SINONIM_SELECTOR = ".content .lemmas table a";

    protected final WebCrawler crawler = new WebCrawler(SINONIM_KATA_ENDPOINT);

    public Collection<String> getSinonimKata(String term) {
        Map<String, String> data = new LinkedHashMap<>();
        data.put(QUERY_INPUT_NAME, term);
        try {
            return crawler.POST(QUERY_FORM_ENDPOINT, data).execute().parse()
                    .select(SINONIM_SELECTOR)
                    .stream().parallel()
                    .map(Element::text)
                    .collect(Collectors.toList());
        } catch (IOException ex) {
            return null;
        }
    }
    
}
