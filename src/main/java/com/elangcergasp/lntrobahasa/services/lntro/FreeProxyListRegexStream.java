package com.elangcergasp.lntrobahasa.services.lntro;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FreeProxyListRegexStream extends ProxySelector {

    private static final Logger LOGGER = LoggerFactory.getLogger(FreeProxyListRegexStream.class);

    public static final String REGEX_IP_PORT = "((?:\\d{1,3})(?:\\.\\d{1,3}){3}):(\\d{2,5})";

    private List<Proxy> cacheProxies = null;

    public FreeProxyListRegexStream(InputStream inputStream) {
        try {
            rebuildProxyList(inputStream);
            select(URI.create(""));
        } catch (Exception e) {
            cacheProxies = new ArrayList<>();
        }
    }

    public void rebuildProxyList(InputStream inputStream) {
        List<Proxy> proxies = new ArrayList<>();
        LOGGER.debug("Start Proxy List Scrapping From Input Stream");
        try {
            String txt = new BufferedReader(new InputStreamReader(inputStream))
                    .lines().collect(Collectors.joining("\n"));
            Matcher m = Pattern.compile(REGEX_IP_PORT).matcher(txt);
            while (m.find()) {
                String ip = m.group(1);
                int port = Integer.parseInt(m.group(2));
                proxies.add(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port)));
            }
            cacheProxies = proxies;
            LOGGER.debug("Proxy List : " + cacheProxies);
        } catch (Exception e) {
            LOGGER.error("Proxy Input failed - " + e.getMessage());
        }
    }

    public static InputStream urlScrappingInputStream(String url) {
        String inputString = "";
        try {
            inputString = Jsoup.connect(url).get().outerHtml();
        } catch (IOException e) {
            LOGGER.error("URL Scrapping failed - " + e.getMessage());
        }
        return new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
    }

    public static InputStream fileTextInputStream(String path) {
        try {
            return new FileInputStream(Paths.get(path).toFile());
        } catch (FileNotFoundException e) {
            return new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8));
        }
    }

    @Override
    public List<Proxy> select(URI uri) {
        return cacheProxies;
    }

    @Override
    public void connectFailed(URI uri, SocketAddress socketAddress, IOException e) {

    }
}
