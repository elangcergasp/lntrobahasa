/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.lntro;

import com.elangcergasp.lntrobahasa.model.SearchEngine;
import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import com.elangcergasp.lntrobahasa.services.ErrorBufferService;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Elang Cergas Pembrani
 */
@EqualsAndHashCode
@ToString
public class SearchEngineScrapper extends SearchEngineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchEngineScrapper.class);

    private final WebCrawler crawler;

    public static final Integer CRAWLER_REFRESH_AFTER_COUNTER = 50;

    public static final Integer BASE_TIMEOUT = 10000; // ms

    private int crawlerRefreshCounter = 0;

    public SearchEngineScrapper(SearchEngine searchEngine) {
        super(searchEngine);

        crawler = new WebCrawler(searchEngine.getHttp().getUrl(), searchEngine.getParams().getUseProxy());
        //crawler.setUsingProxy(searchEngine.getParams().getUseProxy());
    }

    public int getInitialOffset() {
        return searchEngine.getHttp().getInitialOffset() != null
                ? searchEngine.getHttp().getInitialOffset()
                : 0;
    }

    public int getOffsetPerIncrement() {
        return searchEngine.getParams().getOffsetPerIncrement() != null
                ? searchEngine.getParams().getOffsetPerIncrement()
                : searchEngine.getParams().getLimitPerIncrement();
    }

    public int getNearestOffsetPerIncrement(int rawOffset) {
        int initialOffset = getInitialOffset();
        int offsetPerIncrement = getOffsetPerIncrement();
        int limitPerIncrement = searchEngine.getParams().getLimitPerIncrement();
        return Math.floorDiv(rawOffset, limitPerIncrement) * offsetPerIncrement + initialOffset;
    }

    public void crawlerRefreshConnection() {
        if (crawlerRefreshCounter > 1) {
            LOGGER.debug("Crawler Proxy [" + crawler.getCurrentProxy() + "] scored : " + crawlerRefreshCounter);
        }
        crawler.refreshConnection();
        crawlerRefreshCounter = 0;
    }

    public void crawlerRefreshIfAfterCounter() {
        if (crawlerRefreshCounter > CRAWLER_REFRESH_AFTER_COUNTER) {
            crawlerRefreshConnection();
        } else {
            crawlerRefreshCounter++;
        }
    }

    public Map<String, String> makeQueryData(String keyword, Integer offset, Integer limit) {
        Map<String, String> queryData = new ConcurrentHashMap<>();

        String keywordField = searchEngine.getHttp().getKeywordField();
        if (keywordField != null && keywordField.length() > 0) {
            queryData.put(keywordField, keyword);
        }

        String offsetField = searchEngine.getHttp().getOffsetField();
        if (offsetField != null && offsetField.length() > 0 && offset != null) {
            queryData.put(offsetField, Integer.toString(offset));
        }

        String limitField = searchEngine.getHttp().getLimitField();
        if (limitField != null && limitField.length() > 0 && limit != null) {
            queryData.put(limitField, Integer.toString(limit));
        }

        if (searchEngine.getHttp().getAdditionalFields() != null) {
            searchEngine.getHttp().getAdditionalFields().entrySet().forEach(entry -> {
                if (entry.getValue() == null) {
                    queryData.put(entry.getKey(), "");
                } else {
                    queryData.put(entry.getKey(), entry.getValue());
                }
            });
        }

        return queryData;
    }

    public Connection makeSearchRequest(String keyword, Integer offset, Integer limit) {
        Map<String, String> queryData = makeQueryData(keyword, offset, limit);

        String targetUrl = searchEngine.getHttp().getUrl();
        String requestMethod = searchEngine.getHttp().getMethod().toUpperCase();
        Connection request = requestMethod.equals("GET")
                ? crawler.GET(targetUrl, queryData)
                : crawler.POST(targetUrl, queryData);

        if (offset != null && !searchEngine.getHttp().getInitialOffset().equals(offset)
                && requestMethod.equals("GET")
                && searchEngine.getParams().getUsePrevOffsetReferrer()) {
            int prevOffset = offset - getOffsetPerIncrement();
            Map<String, String> prevQueryData = makeQueryData(keyword, prevOffset, limit);
            request = request.referrer(crawler.buildEndpoint(targetUrl, prevQueryData));
        }

        return request;
    }

    public List<String> getAttributeOfElements(Document doc, String selector, String attribute) {
        if (selector == null || selector.length() == 0 || attribute == null || attribute.length() == 0) {
            return new ArrayList<>();
        }

        Elements elements = doc.select(selector);

        return elements.stream()
                .map((element) -> element.attr(attribute))
                .collect(Collectors.toList());
    }

    public List<String> getContentOfElements(Document doc, String selector) {
        if (selector == null || selector.length() == 0) {
            return new ArrayList<>();
        }

        Elements elements = doc.select(selector);

        return elements.stream()
                .map(Element::text)
                .collect(Collectors.toList());
    }

    public List<String> getSearchURL(Document doc) {
        return getAttributeOfElements(doc, searchEngine.getScrapper().getUrlSelector(), "abs:href");
    }

    public List<String> getSearchTitles(Document doc) {
        return getContentOfElements(doc, searchEngine.getScrapper().getTitleSelector());
    }

    public List<String> getSearchSnippets(Document doc) {
        return getContentOfElements(doc, searchEngine.getScrapper().getSnippetSelector());
    }

    public List<String> getSearchStatCount(Document doc) {
        return getContentOfElements(doc, searchEngine.getScrapper().getStatCountSelector());
    }

    public List<String> getSearchStatElapsed(Document doc) {
        return getContentOfElements(doc, searchEngine.getScrapper().getStatElapsedSelector());
    }

    public List<String> getSearchNegation(Document doc) {
        return getContentOfElements(doc, searchEngine.getScrapper().getNegativeSelector());
    }

    public boolean isSearchResultNegated(Document doc) {
        List<String> negationElements = getSearchNegation(doc);
        if (negationElements.size() == 1) {
            String element = negationElements.get(0);
            return element.trim().length() > 0;
        } else {
            return negationElements.size() > 0;
        }
    }

    public List<Double> getOnlyNumbers(String text) {
        Matcher matcher = Pattern.compile("\\d+(?:[,.]\\d+)*").matcher(text);
        List<Double> doubleList = new ArrayList<>();
        while (matcher.find()) {
            doubleList.add(Double.valueOf(matcher.group()
                    .replaceAll("[,.](\\d{3})", "$1")
                    .replaceAll("[,.](\\d{1,2}|\\d{4,})", ".$1")));
        }
        return doubleList;
    }

    public Collection<SearchResultItem> parseSearchResultItem(String keyword, Document doc) {
        List<String> searchURL = getSearchURL(doc);
        List<String> searchTitles = getSearchTitles(doc);
        List<String> searchSnippets = getSearchSnippets(doc);

        Stream<SearchResultItem> searchResults = Stream.empty();
        int searchResultSize = searchTitles.size();
        try {
            for (int i = 0; i < searchResultSize; i++) {
                searchResults = Stream.concat(searchResults, Stream.of(
                        new SearchResultItem(
                                UUID.randomUUID().toString(),
                                SearchResultItem.defaultFetchDate(),
                                searchEngine.getID(),
                                keyword,
                                searchURL.get(i),
                                searchTitles.get(i),
                                searchSnippets.get(i),
                                null
                        ))
                );
            }
        } catch (Exception e) {
            LOGGER.error("[" + searchEngine.getID() + "] For keyword " + keyword + " Search Titles size (" + searchTitles.size() + ") and Search Snippets size (" + searchSnippets.size() + ") mismatch");
        }

        return searchResults.collect(Collectors.toList());
    }

    public SearchResultStat parseSearchResultStat(String keyword, Document doc) {
        List<String> searchStatCount = getSearchStatCount(doc);
        List<String> searchStatElapsed = getSearchStatElapsed(doc);

        LOGGER.debug("[" + searchEngine.getTitle() + "] Strings stat count (" + keyword + ") : " + searchStatCount);
        Long searchStatCountValue;
        if (searchStatCount.size() > 0) {
            List<Double> searchStatCountDoubles = getOnlyNumbers(searchStatCount.get(0));
            searchStatCountValue = searchStatCountDoubles.get(0).longValue();

            LOGGER.debug("[" + searchEngine.getTitle() + "] Parsed stat count (" + keyword + ") : " + searchStatCountValue);
        } else {
            // searchStatCountValue = null;
            searchStatCountValue = 0L;
        }

        Double searchStatElapsedValue;
        if (searchStatElapsed.size() > 0) {
            List<Double> searchStatElapsedDoubles = getOnlyNumbers(searchStatElapsed.get(0));
            searchStatElapsedValue = searchStatElapsedDoubles.get(0);
        } else {
            searchStatElapsedValue = null;
        }

        if (isSearchResultNegated(doc)) {
            searchStatCountValue = null;
            searchStatElapsedValue = null;
            LOGGER.debug("[" + searchEngine.getTitle() + "] Stat count is negated (" + keyword + ") : " + searchStatCountValue);
        }

        return new SearchResultStat(
                UUID.randomUUID().toString(),
                SearchResultStat.defaultFetchDate(),
                searchEngine.getID(),
                keyword,
                searchStatCountValue,
                searchStatElapsedValue,
                null
        );
    }

    public Mono<Document> getSearchResultDocument(String keyword, Integer offset, Integer limit) {
        long searchDelay = searchEngine.getParams().getRetryDelay();
        long searchLimit = searchEngine.getParams().getRetryLimit();

        return Mono.create(emitter -> {
            Flux<Long> attempts = Flux.interval(Duration.ZERO, Duration.ofMillis(searchDelay))
                    .onBackpressureLatest()
                    //.timeout(Duration.ofMillis(BASE_TIMEOUT + searchDelay * (searchLimit + 1)))
                    .publishOn(Schedulers.parallel());

            //attempts.take(searchEngine.getParams().getRetryLimit()).subscribe(new Subscriber<Long>() {
            attempts.subscribe(new Subscriber<Long>() {
                private Subscription subscription;
                private int retryCount;

                @Override
                public void onSubscribe(Subscription s) {
                    LOGGER.info("[" + searchEngine.getTitle() + "] Getting Document (keyword: '" + keyword + "', offset: " + offset + ", count: " + limit + ")");
                    this.subscription = s;
                    this.retryCount = -1;

                    s.request(1);
                }

                @Override
                public void onNext(Long intervalCount) {
                    retryCount++;
                    LOGGER.debug("[" + searchEngine.getTitle() + "] Attempting [" + (retryCount + 1) + "] Document '" + keyword + "'");
                    Mono<Document> searchResultDocument = Mono.fromCallable(() -> {
                        return makeSearchRequest(keyword, offset, limit).execute().parse();
                    }).doOnError(e -> {
                        LOGGER.error("[" + searchEngine.getTitle() + "] Attempt [" + (retryCount + 1) + "] Document '" + keyword + "' FAILED - Message: " + e.getMessage());
                        crawlerRefreshConnection();

                        if (retryCount < searchEngine.getParams().getRetryLimit()) {
                            LOGGER.debug("[" + searchEngine.getTitle() + "] Waiting " + searchDelay + " ms for next attempt");
                            subscription.request(1);
                        } else {
                            LOGGER.error("[" + searchEngine.getTitle() + "] Failed to fetch Document '" + keyword + "' in " + retryCount + " retries");
                            emitter.success();
                            subscription.cancel();

                            Map<String, Object> errorMeta = new ConcurrentHashMap<>();
                            errorMeta.put("searchEngine", searchEngine.getID());
                            errorMeta.put("keyword", keyword);
                            errorMeta.put("url", searchEngine.getHttp().getUrl());
                            errorMeta.put("query", makeQueryData(keyword, offset, limit));
                            ErrorBufferService.getInstance().push(SearchEngineScrapper.class, e.getMessage(), errorMeta);
                        }
                    });
                    searchResultDocument.subscribe(document -> {
                        LOGGER.debug("[" + searchEngine.getTitle() + "] Document fetched '" + keyword + "'");
                        emitter.success(document);
                        subscription.cancel();
                        crawlerRefreshIfAfterCounter();
                    });
                }

                @Override
                public void onError(Throwable t) {
                    LOGGER.error("[" + searchEngine.getTitle() + "] Failed to fetch Document - " + t.getMessage());
                    emitter.success();
                }

                @Override
                public void onComplete() {
                    emitter.success();
                }
            });
        });
    }

    public Flux<SearchResultItem> getSearchResultOnce(String keyword, int offset, int limit) {
        long searchDelay = searchEngine.getParams().getRetryDelay();
        long searchLimit = searchEngine.getParams().getRetryLimit();

        return Flux.create(emitter -> {
            Flux<Long> attempts = Flux.interval(Duration.ZERO, Duration.ofMillis(searchDelay))
                    .onBackpressureLatest()
                    //.timeout(Duration.ofMillis(BASE_TIMEOUT + searchDelay * (searchLimit + 1)))
                    .publishOn(Schedulers.parallel());

            //attempts.take(searchEngine.getParams().getRetryLimit()).subscribe(new Subscriber<Long>() {
            attempts.subscribe(new Subscriber<Long>() {
                private Subscription subscription;
                private int retryCount;

                @Override
                public void onSubscribe(Subscription s) {
                    LOGGER.info("[" + searchEngine.getTitle() + "] Getting Corpus (keyword: '" + keyword + "', offset: " + offset + ", count: " + limit + ")");
                    this.subscription = s;
                    this.retryCount = -1;

                    s.request(1);
                }

                @Override
                public void onNext(Long intervalCount) {
                    retryCount++;
                    if (retryCount > searchEngine.getParams().getRetryLimit() + 1) {
                        emitter.complete();
                        subscription.cancel();
                        return;
                    }

                    LOGGER.debug("[" + searchEngine.getTitle() + "] Attempting [" + (retryCount + 1) + "] corpus '" + keyword + "'");
                    Mono<Document> searchResultDocument = Mono.fromCallable(() -> {
                        return makeSearchRequest(keyword, offset, limit).execute().parse();
                    }).doOnError(e -> {
                        LOGGER.error("[" + searchEngine.getTitle() + "] Attempt [" + (retryCount + 1) + "] corpus '" + keyword + "' FAILED - Message: " + e.getMessage());
                        crawlerRefreshConnection();

                        if (retryCount < searchEngine.getParams().getRetryLimit()) {
                            LOGGER.debug("[" + searchEngine.getTitle() + "] Waiting " + searchDelay + " ms for next attempt");
                            subscription.request(1);
                        } else {
                            LOGGER.error("[" + searchEngine.getTitle() + "] Failed to fetch corpus '" + keyword + "' in " + retryCount + " retries");
                            emitter.complete();
                            subscription.cancel();

                            Map<String, Object> errorMeta = new ConcurrentHashMap<>();
                            errorMeta.put("searchEngine", searchEngine.getID());
                            errorMeta.put("keyword", keyword);
                            errorMeta.put("url", searchEngine.getHttp().getUrl());
                            errorMeta.put("query", makeQueryData(keyword, offset, limit));
                            ErrorBufferService.getInstance().push(SearchEngineScrapper.class, e.getMessage(), errorMeta);
                        }
                    });

                    searchResultDocument.subscribe(document -> {
                        if (document != null) {
                            LOGGER.debug("[" + searchEngine.getTitle() + "] Document corpus '" + keyword + "' fetched from connection response");

                            Collection<SearchResultItem> parsedSearchResult = parseSearchResultItem(keyword, document);
                            parsedSearchResult.forEach(sr -> {
                                emitter.next(sr);
                            });

                            emitter.complete();
                            subscription.cancel();
                            crawlerRefreshIfAfterCounter();
                            LOGGER.debug("[" + searchEngine.getTitle() + "] Fetched Search Result for corpus '" + keyword + "' with [" + parsedSearchResult.size() + "] entries");
                        }
                    });
                }

                @Override
                public void onError(Throwable t) {
                    LOGGER.error("[" + searchEngine.getTitle() + "] Failed to fetch corpus " + keyword + " - " + t.getMessage());
                    emitter.complete();
                }

                @Override
                public void onComplete() {
                    emitter.complete();
                }
            });
        });
    }

    @Override
    public String createKeyword(Collection<String> data, String relation) {
        String quoteAll = searchEngine.getHttp().getQuoteAll() != null ? searchEngine.getHttp().getQuoteAll() : "";
        String quoteData = searchEngine.getHttp().getQuoteData() != null ? searchEngine.getHttp().getQuoteData() : "";
        String spacing = relation != null && relation.length() > 0 ? " " + relation + " " : " ";

        Collection<String> dataCollection = data.stream().parallel()
                .map(String::toLowerCase)
                .map(s -> quoteData + s + quoteData)
                .collect(Collectors.toList());
        // return result.toLowerCase();
        /*
        return relation != null && relation.length() > 0
                ? quoteAll + String.join(" " + relation + " ", dataCollection) + quoteAll
                : String.join(" ", dataCollection.stream().parallel()
                        .map(d -> quoteAll + d + quoteAll)
                        .collect(Collectors.toList()));
        */
        return quoteAll + String.join(spacing, dataCollection) + quoteAll;
    }

    @Override
    public String createKeyword(Collection<String> data) {
        return createKeyword(data, searchEngine.getHttp().getWildcard());
    }

    @Override
    public Flux<SearchResultItem> getSearchResult(String keyword) {
        return getSearchResult(keyword, DEFAULT_SEARCH_RESULT_LIMIT);
    }

    @Override
    public Flux<SearchResultItem> getSearchResult(String keyword, int limit) {
        return getSearchResult(keyword, searchEngine.getHttp().getInitialOffset(), limit);
    }

    @Override
    public Flux<SearchResultItem> getSearchResult(String keyword, int initialOffset, int limit) {
        long searchDelay = searchEngine.getParams().getRetryDelay();
        long searchLimit = searchEngine.getParams().getRetryLimit();

        return Flux.create(emitter -> {
            Flux<Long> attempts = Flux.interval(Duration.ofMillis(searchDelay))
                    .onBackpressureLatest()
                    //.timeout(Duration.ofMillis(BASE_TIMEOUT + searchDelay * (searchLimit + 1)))
                    .publishOn(Schedulers.parallel());

            //attempts.take(searchEngine.getParams().getRetryLimit()).subscribe(new Subscriber<Long>() {
            attempts.subscribe(new Subscriber<Long>() {
                private Subscription subscription;
                private int retryCount;
                private List<SearchResultItem> tmpPrevSearchResult;
                private int offset;
                private int limitPerIncrement;
                private int offsetPerIncrement;
                private int threshold;
                private int searchResultCounter;

                @Override
                public void onSubscribe(Subscription s) {
                    this.subscription = s;
                    this.retryCount = -1;
                    this.tmpPrevSearchResult = null;
                    this.offset = getNearestOffsetPerIncrement(initialOffset);
                    this.limitPerIncrement = searchEngine.getParams().getLimitPerIncrement();
                    this.offsetPerIncrement = getOffsetPerIncrement();

                    this.threshold = this.limitPerIncrement / 2;
                    this.searchResultCounter = 0;

                    s.request(1);
                }

                @Override
                public void onNext(Long intervalCount) {
                    retryCount++;
                    if (emitter.isCancelled()) {
                        return;
                    }

                    //crawler.refreshConnection();
                    Mono<List<SearchResultItem>> tmpSearchResult = getSearchResultOnce(keyword, offset, limitPerIncrement).collectList();

                    tmpSearchResult.subscribe(tsr -> {
                        if (!tsr.isEmpty()) {
                            if (tmpPrevSearchResult != null && tmpPrevSearchResult.size() == tsr.size()) {
                                final List<SearchResultItem> tmpPrevSearchResult2 = tmpPrevSearchResult;
                                tsr = tsr.stream()
                                        .parallel()
                                        .filter(sr -> !tmpPrevSearchResult2.stream()
                                                .parallel()
                                                .map(psr -> psr.equals(sr))
                                                .reduce(false, (a, b) -> a || b))
                                        .collect(Collectors.toList());
                            }
                            tsr.forEach(sr -> {
                                emitter.next(sr);
                            });
                            tmpPrevSearchResult = tsr;
                        }

                        offset += offsetPerIncrement;
                        searchResultCounter += tsr.size();

                        if (offset < (offsetPerIncrement == limitPerIncrement ? limit : Math.ceil(((double) limit) / ((double) limitPerIncrement)) + getInitialOffset())
                                && searchResultCounter < limit
                                && tsr.size() > threshold) {
                            subscription.request(1);
                        } else {
                            emitter.complete();
                            subscription.cancel();
                        }
                    });
                }

                @Override
                public void onError(Throwable t) {
                    LOGGER.error("[" + searchEngine.getTitle() + "] Failed to fetch corpus " + keyword + " - " + t.getMessage());
                    emitter.complete();
                }

                @Override
                public void onComplete() {
                    emitter.complete();
                }
            });
        });
    }

    @Override
    public Mono<SearchResultStat> getSearchResultStat(String keyword) {
        return getSearchResultDocument(keyword, null, null)
                .map(document -> parseSearchResultStat(keyword, document))
                .switchIfEmpty(Mono.fromCallable(() -> new SearchResultStat(
                        UUID.randomUUID().toString(),
                        SearchResultStat.defaultFetchDate(),
                        searchEngine.getID(),
                        keyword,
                        null,
                        null,
                        null
                )));
    }

    @Override
    public Mono<SearchResultStat> getSearchResultStat(Collection<String> data) {
        String keyword = createKeyword(data);
        return getSearchResultDocument(keyword, null, null)
                .map(document -> parseSearchResultStat(keyword, document))
                .switchIfEmpty(Mono.fromCallable(() -> new SearchResultStat(
                        UUID.randomUUID().toString(),
                        SearchResultStat.defaultFetchDate(),
                        searchEngine.getID(),
                        keyword,
                        null,
                        null,
                        null
                )));
    }

}
