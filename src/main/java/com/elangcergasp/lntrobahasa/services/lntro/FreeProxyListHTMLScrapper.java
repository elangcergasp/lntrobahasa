package com.elangcergasp.lntrobahasa.services.lntro;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FreeProxyListHTMLScrapper extends ProxySelector {

    private static final Logger LOGGER = LoggerFactory.getLogger(FreeProxyListHTMLScrapper.class);

    public static final String FREE_PROXY_LIST_URL = "https://www.free-proxy-list.net/";
    public static final String SELECTOR_TABLE_ROWS = "table#proxylisttable tbody tr";
    public static final String SELECTOR_IP = "td:nth-child(1)";
    public static final String SELECTOR_PORT = "td:nth-child(2)";
    public static final Map<String, String> SELECTOR_FILTER_REGEX = new ConcurrentHashMap<>();
    static {
        // SELECTOR_FILTER_REGEX.put("td:nth-child(3)", "ID");
        // SELECTOR_FILTER_REGEX.put("td:nth-child(5)", "anonymous|elite");
    }
    /*
    public static final String FREE_PROXY_LIST_URL = "http://eproxy.xyz/proxy-list";
    public static final String SELECTOR_TABLE_ROWS = ".container table tbody tr";
    public static final String SELECTOR_IP = "td:nth-child(1)";
    public static final String SELECTOR_PORT = "td:nth-child(2)";
    public static final Map<String, String> SELECTOR_FILTER_REGEX = new ConcurrentHashMap<>();
    static {
        SELECTOR_FILTER_REGEX.put("td:nth-child(3)", "ID");
        SELECTOR_FILTER_REGEX.put("td:nth-child(5)", "anonymous|elite proxy");
    }
    */

    private Long lastFetch = System.currentTimeMillis();
    private static Long CACHE_TTL = 60L * 60L * 1000L;
    private List<Proxy> cacheProxies = null;

    public FreeProxyListHTMLScrapper() {
        try {
            select(URI.create(FREE_PROXY_LIST_URL));
        } catch (Exception e) {
            cacheProxies = new ArrayList<>();
        }
    }

    @Override
    public List<Proxy> select(URI uri) {
        if (cacheProxies != null && lastFetch > (System.currentTimeMillis() - CACHE_TTL)) {
            return cacheProxies;
        }

        List<Proxy> proxies = new ArrayList<>();
        LOGGER.debug("Start Proxy List Scrapping : " + FREE_PROXY_LIST_URL);
        try {
            Document doc = Jsoup.connect(FREE_PROXY_LIST_URL).get();
            Elements rows = doc.select(SELECTOR_TABLE_ROWS);
            rows.forEach(element -> {
                boolean checkFilter = true;
                if (SELECTOR_FILTER_REGEX.size() > 0) {
                    try {
                        for (Map.Entry<String, String> e : SELECTOR_FILTER_REGEX.entrySet()) {
                            if (!element.selectFirst(e.getKey()).text().matches(e.getValue())) {
                                checkFilter = false;
                            }
                        }
                    } catch (Exception e) {}
                }
                if (checkFilter) {
                    String ip = element.selectFirst(SELECTOR_IP).text();
                    int port = Integer.parseInt(element.selectFirst(SELECTOR_PORT).text());
                    proxies.add(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port)));
                }
            });
            cacheProxies = proxies;
            lastFetch = System.currentTimeMillis();
            LOGGER.debug("Proxy List : " + cacheProxies);
        } catch (Exception e) {
            LOGGER.error("Proxy Scrapping failed - " + e.getMessage());
        }
        return cacheProxies;
    }

    @Override
    public void connectFailed(URI uri, SocketAddress socketAddress, IOException e) {

    }
}
