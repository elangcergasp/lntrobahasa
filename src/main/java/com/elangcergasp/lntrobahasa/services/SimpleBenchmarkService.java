package com.elangcergasp.lntrobahasa.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SimpleBenchmarkService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleBenchmarkService.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static final String START_MARK = "BENCHMARK:START";

    private static SimpleBenchmarkService singleton = new SimpleBenchmarkService();

    private Map<String, TimeMark> markerMap = new ConcurrentHashMap<>();

    public SimpleBenchmarkService() {
        start(START_MARK);
    }

    public static SimpleBenchmarkService getInstance() {
        return singleton;
    }

    public static String format(Long timeInMillis) {
        Duration duration = Duration.ofMillis(timeInMillis);

        long seconds = duration.getSeconds();
        long millis = Math.abs(duration.toMillis());
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%02d:%02d:%02d.%03d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60,
                millis % 1000);
        /*
        String shour = (absSeconds / 3600) > 0 ? Math.floor(absSeconds / 3600d) + "h" : "";
        String sminute = (absSeconds / 60) > 0 ? Math.floor((absSeconds % 3600d) / 60d) + "m" : "";
        String ssecond = (millis >= 1000) ? Double.toString(Math.floor(absSeconds % 60d)) + "s" : "";
        String smillis = Double.toString(millis % 1000d) + "ms";
        String positive = shour + " " + sminute + " " + ssecond + " " + smillis;
        positive = positive.trim();
        */
        return seconds < 0 ? "-" + positive : positive;
    }

    public TimeMark start(String id) {
        if (markerMap.containsKey(id)) {
            int i = 2;
            while (markerMap.containsKey(id)) {
                id = id + "-" + i;
                i++;
            }
        }
        TimeMark timeMark = new TimeMark(id, System.currentTimeMillis());
        markerMap.putIfAbsent(id, timeMark);
        LOGGER.debug("Added " + timeMark);
        return timeMark;
    }

    public TimeMark startUnique(String id) {
        if (!markerMap.containsKey(id)) {
            TimeMark timeMark = new TimeMark(id, System.currentTimeMillis());
            markerMap.putIfAbsent(id, timeMark);
            LOGGER.debug("Added " + timeMark);
            return timeMark;
        } else {
            return get(id);
        }
    }

    public String generateRandomID() {
        return "BENCHMARK:" + UUID.randomUUID().toString();
    }

    public TimeMark start() {
        return start(generateRandomID());
    }

    public TimeMark get(String id) {
        return markerMap.get(id);
    }

    public Long diff(String id_1, String id_2) {
        return get(id_1).diff(get(id_2));
    }

    public Long elapsed(String id) {
        return get(id).elapsed();
    }

    public Long end(TimeMark timeMark) {
        Long result = timeMark.elapsed();
        markerMap.remove(timeMark.ID);
        return result;
    }

    public Long end(String id) {
        return end(get(id));
    }

    public class TimeMark {
        public final String ID;
        public final Long TIME;

        public TimeMark(String ID, Long TIME) {
            this.ID = ID;
            this.TIME = TIME;
        }

        public Long diff(Long otherTime) {
            return Math.abs(rawDiff(otherTime));
        }

        public Long diff(TimeMark other) {
            return diff(other.TIME);
        }

        public Long elapsed() {
            return diff(System.currentTimeMillis());
        }

        public String elapsedString() {
            return SimpleBenchmarkService.format(elapsed());
        }

        public Long rawDiff(Long otherTime) {
            return this.TIME - otherTime;
        }

        public Long rawDiff(TimeMark other) {
            return rawDiff(other.TIME);
        }

        @Override
        public String toString() {
            return "TimeMark{" +
                    "ID='" + ID + '\'' +
                    ", TIME=" + TIME +
                    ", START=" + sdf.format(new Date(TIME)) +
                    '}';
        }
    }

}
