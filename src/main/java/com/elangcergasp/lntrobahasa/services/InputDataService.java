package com.elangcergasp.lntrobahasa.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.DataFrameReader;
import tech.tablesaw.io.csv.CsvReadOptions;

import java.io.*;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class InputDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InputDataService.class);

    private static final Map<String, Character> VALID_SEPARATOR_MAP = new ConcurrentHashMap<>();
    static {
        VALID_SEPARATOR_MAP.put("TAB", '\t');
        VALID_SEPARATOR_MAP.put("COMMA", ',');
        VALID_SEPARATOR_MAP.put("SEMICOLON", ';');
    }
    public static final Collection<String> VALID_SEPARATORS = VALID_SEPARATOR_MAP.keySet();

    private static final DataFrameReader TABLE_READER = Table.read();

    public static String defaultTableName = "DATA";

    public Table getDataCSV(InputStream dataInput, String separatorKey, String tableName) {
        Table data;
        try {
            char separator = VALID_SEPARATOR_MAP.getOrDefault(separatorKey, ',');

            data = TABLE_READER.csv(CsvReadOptions
                    .builder(dataInput, tableName)
                    .separator(separator)
                    .header(true)
            );
        } catch (FileNotFoundException ex) {
            LOGGER.error("Data file not found : " + ex.getMessage());
            return null;
        } catch (IOException ex) {
            LOGGER.error("Input Data error : " + ex.getMessage());
            return null;
        }
        return data;
    }

    public Table getDataCSV(String dataPath, String separatorKey, String tableName) {
        Table data = null;
        if (dataPath != null && dataPath.length() > 0) {
            InputStream dataInput;
            try {
                dataInput = new FileInputStream(dataPath);

                data = getDataCSV(dataInput, separatorKey, tableName);
            } catch (FileNotFoundException ex) {
                LOGGER.error("Data file not found : " + ex.getMessage());
                return null;
            }
        }
        return data;
    }

    public Table getDataCSV(File dataFile, String separatorKey, String tableName) {
        Table data = null;
        if (dataFile != null) {
            InputStream dataInput;
            try {
                dataInput = new FileInputStream(dataFile);

                data = getDataCSV(dataInput, separatorKey, tableName);
            } catch (FileNotFoundException ex) {
                LOGGER.error("Data file not found : " + ex.getMessage());
                return null;
            }
        }
        return data;
    }

    public Table getDataCSV(InputStream dataInput, String separatorKey) {
        return getDataCSV(dataInput, separatorKey, defaultTableName);
    }

    public Table getDataCSV(String dataPath, String separatorKey) {
        return getDataCSV(dataPath, separatorKey, defaultTableName);
    }

    public Table getDataCSV(File dataFile, String separatorKey) {
        return getDataCSV(dataFile, separatorKey, defaultTableName);
    }

    public Table getDataTSV(String dataPath) {
        return getDataCSV(dataPath, "TAB");
    }
}
