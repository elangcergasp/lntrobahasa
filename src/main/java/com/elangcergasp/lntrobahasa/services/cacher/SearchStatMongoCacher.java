/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.cacher;

import com.elangcergasp.lntrobahasa.model.SearchResultStat;
import com.elangcergasp.lntrobahasa.repository.SearchStatRepository;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Collection;

/**
 * @author Elang Cergas Pembrani
 */
@Service
public class SearchStatMongoCacher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchStatMongoCacher.class);

    @Autowired
    private SearchStatRepository searchStatRepository;

    public Mono<SearchResultStat> getSearchStatFromCache(SearchEngineRunner searchEngineRunner, String keyword) {
        return searchStatRepository.findBySearchEngineAndKeyword(searchEngineRunner.getSearchEngine().getID(), keyword);
    }

    public Mono<SearchResultStat> getSearchStatFromCache(SearchEngineRunner searchEngineRunner, Collection<String> data) {
        return searchStatRepository.findBySearchEngineAndMeta(searchEngineRunner.getSearchEngine().getID(), data);
    }

    public Mono<SearchResultStat> getSearchStatFromSearchEngine(SearchEngineRunner searchEngineRunner, String keyword) {
        return searchEngineRunner.getSearchResultStat(keyword)
                .doOnSubscribe((t) -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Fetching keyword " + keyword + " from Search Engine");
                })
                .doOnSuccess(ss -> {
                    searchStatRepository.save(ss).subscribe(searchResultStat -> {
                        LOGGER.trace("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Saved search stat " + searchResultStat + "");
                    });
                })
                .switchIfEmpty(Mono.create(emitter -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Keyword " + keyword + " Not Found from Search Engine");
                    emitter.success();
                }));
    }

    public Mono<SearchResultStat> getSearchStatFromSearchEngine(SearchEngineRunner searchEngineRunner, Collection<String> data) {
        return searchEngineRunner.getSearchResultStat(data)
                .map(searchResultStat -> {
                    searchResultStat.setMeta(data);
                    return searchResultStat;
                })
                .doOnSubscribe((t) -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Fetching data " + data + " from Search Engine");
                })
                .doOnSuccess(ss -> {
                    searchStatRepository.save(ss).subscribe(searchResultStat -> {
                        LOGGER.trace("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Saved search stat " + searchResultStat + "");
                    });
                })
                .switchIfEmpty(Mono.create(emitter -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Data " + data + " Not Found");
                    emitter.success();
                }));
    }

    public void cleanCache() {
        searchStatRepository.deleteAll();
    }

    public void cleanCache(String keyword) {
        searchStatRepository.deleteByKeyword(keyword);
    }

    public SearchStatRepository getSearchStatRepository() {
        return searchStatRepository;
    }

    public void setSearchStatRepository(SearchStatRepository searchStatRepository) {
        this.searchStatRepository = searchStatRepository;
    }

}
