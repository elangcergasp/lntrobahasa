package com.elangcergasp.lntrobahasa.services.cacher;

import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTag;
import com.elangcergasp.lntrobahasa.algorithm.lang.nlp.POSTagSet;
import com.elangcergasp.lntrobahasa.model.DocumentKBBI;
import com.elangcergasp.lntrobahasa.repository.KBBIRepository;
import com.elangcergasp.lntrobahasa.services.lntro.MyKBBI;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.UUID;

@Service
public class KBBIMongoCacher {

    private static final Logger LOGGER = LoggerFactory.getLogger(KBBIMongoCacher.class);

    public static final Integer BASE_TIMEOUT = 20000;

    @Autowired
    private KBBIRepository kbbiRepository;

    @Autowired
    private MyKBBI kbbiSource;

    public Mono<DocumentKBBI> getDocumentKBBIFromCache(String term) {
        return kbbiRepository.findByTerm(term);
    }

    public Mono<DocumentKBBI> getDocumentKBBIFromSource(String term) {
        return Mono.create(emitter -> {
            try {
                Document doc = kbbiSource.getDocument(term);
                POSTag posTag = kbbiSource.getHighlightedPOSTag(doc);
                DocumentKBBI reference = null;
                if (posTag.equals(POSTagSet.UNKNOWN)) {
                    String referenceTerm = kbbiSource.getReference(doc);
                    LOGGER.debug("[KBBI] Reference : " + referenceTerm);
                    if (referenceTerm != null) {
                        reference = getDocumentKBBI(referenceTerm).block();
                        if (reference != null) {
                            posTag = reference.getPosTag();
                        }
                    }
                }
                if (doc != null) {
                    emitter.success(new DocumentKBBI(
                            UUID.randomUUID().toString(),
                            DocumentKBBI.defaultFetchDate(),
                            term,
                            doc.outerHtml(),
                            reference,
                            posTag
                    ));
                }
            } catch (Exception e) {}
            emitter.success(new DocumentKBBI(
                    UUID.randomUUID().toString(),
                    DocumentKBBI.defaultFetchDate(),
                    term,
                    null,
                    null,
                    POSTagSet.UNKNOWN
            ));
        });
    }

    public Mono<DocumentKBBI> getDocumentKBBI(String term) {
        return getDocumentKBBIFromCache(term).switchIfEmpty(
                getDocumentKBBIFromSource(term)
                        .timeout(Duration.ofMillis(BASE_TIMEOUT))
                        .doOnNext(documentKBBI -> {
                            if (documentKBBI.getDocumentHTML() != null) {
                                kbbiRepository.save(documentKBBI).subscribe(d -> {
                                    LOGGER.trace("KBBI Cacher save term : " + d.getTerm());
                                });
                            }
                        })
        );
    }

    public Mono<Boolean> saveDocumentKBBI(String term, String document) {
        Document doc = Jsoup.parse(document);
        POSTag posTag = kbbiSource.getHighlightedPOSTag(doc);
        DocumentKBBI reference = null;
        if (doc != null) {
            kbbiRepository.save(new DocumentKBBI(
                    UUID.randomUUID().toString(),
                    DocumentKBBI.defaultFetchDate(),
                    term,
                    doc.outerHtml(),
                    reference,
                    posTag
            )).subscribe();
            return Mono.just(true);
        } else {
            return Mono.just(false);
        }
    }

    public MyKBBI getKbbiSource() {
        return kbbiSource;
    }

    public void setKbbiSource(MyKBBI kbbiSource) {
        this.kbbiSource = kbbiSource;
    }
}
