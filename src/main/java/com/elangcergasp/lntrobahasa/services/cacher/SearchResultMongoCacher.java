/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elangcergasp.lntrobahasa.services.cacher;

import com.elangcergasp.lntrobahasa.model.SearchResultItem;
import com.elangcergasp.lntrobahasa.repository.SearchResultRepository;
import com.elangcergasp.lntrobahasa.services.lntro.SearchEngineRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.Objects;

/**
 * @author Elang Cergas Pembrani
 */
@Service
public class SearchResultMongoCacher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchResultMongoCacher.class);

    @Autowired
    private SearchResultRepository searchResultRepository;

    public Flux<SearchResultItem> getSearchResultFromCache(Collection<String> data) {
        return searchResultRepository.findByMeta(data).distinct(SearchResultItem::sourceHashCode);
        // return searchResultRepository.findByMeta(data);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(Collection<String> data, int count) {
        return getSearchResultFromCache(data).take(count);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String[] searchEngines, Collection<String> data) {
        return searchResultRepository.findByMetaAndSearchEngines(data, searchEngines).distinct(SearchResultItem::sourceHashCode);
        // return searchResultRepository.findByMetaAndSearchEngines(data, searchEngines);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String[] searchEngines, Collection<String> data, int count) {
        return getSearchResultFromCache(searchEngines, data).take(count);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String keyword) {
        return searchResultRepository.findByKeyword(keyword).distinct(SearchResultItem::sourceHashCode);
        // return searchResultRepository.findByKeyword(keyword);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String keyword, int count) {
        return getSearchResultFromCache(keyword).take(count);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String[] searchEngines, String keyword) {
        return searchResultRepository.findByKeywordAndSearchEngines(keyword, searchEngines).distinct(SearchResultItem::sourceHashCode);
        // return searchResultRepository.findByKeywordAndSearchEngines(keyword, searchEngines);
    }

    public Flux<SearchResultItem> getSearchResultFromCache(String[] searchEngines, String keyword, int count) {
        return getSearchResultFromCache(searchEngines, keyword).take(count);
    }

    public Flux<SearchResultItem> getSearchResultFromSearchEngine(SearchEngineRunner searchEngineRunner, Collection<String> data, int count) {
        String keyword = searchEngineRunner.createKeyword(data);
        Flux<SearchResultItem> searchResults = searchEngineRunner.getSearchResult(keyword, count)
                .map(searchResultItem -> {
                    searchResultItem.setMeta(data);
                    return searchResultItem;
                }).doOnSubscribe((t) -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Fetching keyword " + keyword);
                }).doOnNext(sr -> {
                    searchResultRepository.save(sr).subscribe(searchResultItem -> {
                        LOGGER.trace("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Saved search result " + searchResultItem + "");
                    });
                }).switchIfEmpty(Flux.create(emitter -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Keyword " + keyword + " Not Found");
                    emitter.complete();
                }));

        return searchResults.share();
    }

    public Flux<SearchResultItem> getSearchResultFromSearchEngine(SearchEngineRunner searchEngineRunner, Collection<String> data, int offset, int count) {
        String keyword = searchEngineRunner.createKeyword(data);
        Flux<SearchResultItem> searchResults = searchEngineRunner.getSearchResult(keyword, offset, count)
                .map(searchResultItem -> {
                    searchResultItem.setMeta(data);
                    return searchResultItem;
                }).doOnSubscribe((t) -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Fetching keyword " + keyword);
                }).doOnNext(sr -> {
                    searchResultRepository.save(sr).subscribe(searchResultItem -> {
                        LOGGER.trace("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Saved search result " + searchResultItem + "");
                    });
                }).switchIfEmpty(Flux.create(emitter -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Keyword " + keyword + " Not Found");
                    emitter.complete();
                }));

        return searchResults.share();
    }

    public Flux<SearchResultItem> getSearchResultFromSearchEngine(SearchEngineRunner searchEngineRunner, String keyword, int count) {
        Flux<SearchResultItem> searchResults = searchEngineRunner.getSearchResult(keyword, count)
                .doOnSubscribe((t) -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Fetching keyword " + keyword);
                }).doOnNext(sr -> {
                    searchResultRepository.save(sr).subscribe(searchResultItem -> {
                        LOGGER.trace("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Saved search result " + searchResultItem + "");
                    });
                }).switchIfEmpty(Flux.create(emitter -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Keyword " + keyword + " Not Found");
                    emitter.complete();
                }));

        return searchResults.share();
    }

    public Flux<SearchResultItem> getSearchResultFromSearchEngine(SearchEngineRunner searchEngineRunner, String keyword, int offset, int count) {
        Flux<SearchResultItem> searchResults = searchEngineRunner.getSearchResult(keyword, offset, count)
                .doOnSubscribe((t) -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Fetching keyword " + keyword);
                }).doOnNext(sr -> {
                    searchResultRepository.save(sr).subscribe(searchResultItem -> {
                        LOGGER.trace("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Saved search result " + searchResultItem + "");
                    });
                }).switchIfEmpty(Flux.create(emitter -> {
                    LOGGER.debug("[" + searchEngineRunner.getSearchEngine().getTitle() + "] Keyword " + keyword + " Not Found");
                    emitter.complete();
                }));

        return searchResults.share();
    }

    public boolean isSearchResultEquals(SearchResultItem sr1, SearchResultItem sr2) {
        if (sr1 == sr2) return true;
        return Objects.equals(sr1.getContentUrl(), sr2.getContentUrl()) &&
                Objects.equals(sr1.getMeta(), sr2.getMeta());
    }

    public void cleanCache() {
        searchResultRepository.deleteAll();
    }

    public void cleanCache(String keyword) {
        searchResultRepository.deleteByKeyword(keyword);
    }

    public SearchResultRepository getSearchResultRepository() {
        return searchResultRepository;
    }

    public void setSearchResultRepository(SearchResultRepository searchResultRepository) {
        this.searchResultRepository = searchResultRepository;
    }

}
