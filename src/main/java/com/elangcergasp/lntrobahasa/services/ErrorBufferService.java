package com.elangcergasp.lntrobahasa.services;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ErrorBufferService {

    private Map<Class, List<ErrorBuffer>> errorBufferMap = new ConcurrentHashMap<>();

    private static ErrorBufferService singleton = new ErrorBufferService();

    private ErrorBufferService() {}

    public static ErrorBufferService getInstance() {
        return singleton;
    }

    public synchronized ErrorBuffer push(Class sourceClass, String exceptionMessage, Map<String, Object> meta) {
        ErrorBuffer errorBuffer = new ErrorBuffer(
                sourceClass,
                exceptionMessage,
                meta
        );
        errorBufferMap.compute(sourceClass, (k, v) -> {
            if (v == null) {
                List<ErrorBuffer> newErrorBuffer = new ArrayList<>();
                newErrorBuffer.add(errorBuffer);
                return newErrorBuffer;
            } else {
                v.add(errorBuffer);
                return v;
            }
        });
        return errorBuffer;
    }

    public synchronized ErrorBuffer pop(Class sourceClass) {
        if (isEmpty(sourceClass)) {
            return null;
        } else {
            return errorBufferMap.get(sourceClass).remove(0);
        }
    }

    public synchronized boolean isEmpty(Class sourceClass) {
        if (errorBufferMap.containsKey(sourceClass)) {
            return errorBufferMap.get(sourceClass).size() == 0;
        } else {
            return true;
        }
    }

    public List<ErrorBuffer> getAllByClass(Class sourceClass) {
        return errorBufferMap.getOrDefault(sourceClass, new ArrayList<>());
    }

    public void destroyMap() {
        errorBufferMap = new ConcurrentHashMap<>();
    }

    public List<ErrorBuffer> clearClass(Class sourceClass) {
        return errorBufferMap.remove(sourceClass);
    }

    @ToString
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Document
    public class ErrorBuffer {
        @CreatedDate
        private Date timestamp = Calendar.getInstance().getTime();

        private Class sourceClass;
        private String exceptionMessage;
        private Map<String, Object> meta;

        public ErrorBuffer(Class sourceClass, String exceptionMessage, Map<String, Object> meta) {
            this.sourceClass = sourceClass;
            this.exceptionMessage = exceptionMessage;
            this.meta = meta;
        }
    }
}
