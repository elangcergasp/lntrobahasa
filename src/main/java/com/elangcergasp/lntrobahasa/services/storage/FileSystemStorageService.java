package com.elangcergasp.lntrobahasa.services.storage;

import com.elangcergasp.lntrobahasa.properties.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    public static final int FILENAME_LENGTH_LIMIT = 100;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getBaseLocation());
    }

    @Override
    public Path store(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());

        return store(file, load(filename));
    }

    @Override
    public Path store(MultipartFile file, Path targetBasePath) {
        String oldFilename = StringUtils.cleanPath(file.getOriginalFilename());
        Path filepath = load(oldFilename);
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + oldFilename);
            }
            if (oldFilename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + oldFilename);
            }
            Path targetPath = newFileToTarget(targetBasePath, oldFilename);
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, targetPath, StandardCopyOption.REPLACE_EXISTING);
                filepath = targetPath;
            }
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + oldFilename + " : " + e.getMessage(), e);
        }
        return filepath;
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        }
        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public String safePath(String text) {
        return text.endsWith("/") ? text : text + "/";
    }

    @Override
    public String safeFilename(String text) {
        String result = text.trim();
        result = result.replaceAll("[^\\w.\\s]", "");
        result = result.replaceAll("\\s+", "_");
        if (result.length() > FILENAME_LENGTH_LIMIT) {
            result = result.substring(0, FILENAME_LENGTH_LIMIT);
        }
        return result;
    }

    @Override
    public Path newFileToTarget(Path targetPath, String baseFilename) {
        String separator = "__";
        String[] splitBaseFilename = splitFileNameExtension(baseFilename);
        String baseFileTitle = safeFilename(splitBaseFilename[0]);
        String extension = splitBaseFilename.length > 1 ? splitBaseFilename[1] : "";
        Path targetFile = targetPath.resolve(baseFilename);
        int iterationName = 0;
        while (targetFile.toFile().exists()) {
            iterationName++;
            String newFilename = baseFileTitle + separator + iterationName + "." + extension;
            targetFile = targetPath.resolve(newFilename);
        }
        return targetFile;
    }

    @Override
    public String[] splitFileNameExtension(String fileName) {
        return fileName.split("\\.(?=[^.]+$)");
    }

    @Override
    public File createFile(String filename) throws IOException {
        Path path = this.load(filename);

        int i = 1;
        File newFile = path.toFile();
        while (!newFile.createNewFile()) {
            i++;
            path = this.load(filename.replaceAll("\\.(?=[^.]+$)", "-" + i + "."));
            newFile = path.toFile();
        }

        return newFile;
    }
}