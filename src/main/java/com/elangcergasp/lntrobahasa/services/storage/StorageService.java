package com.elangcergasp.lntrobahasa.services.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    Path store(MultipartFile file);

    Path store(MultipartFile file, Path targetBasePath);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

    String safePath(String text);

    String safeFilename(String text);

    Path newFileToTarget(Path targetPath, String baseFilename);

    String[] splitFileNameExtension(String fileName);

    File createFile(String filename) throws IOException;

}