import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { KeyChoice } from './model/key-choice';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerPropertiesService {

  private defaultSearchEngines: Array<KeyChoice> = [
    { key: 'GOOGLE', title: 'Google' },
    { key: 'DUCKDUCKGO', title: 'DuckDuckGo' },
    { key: 'BING', title: 'Bing' },
    { key: 'YAHOO', title: 'Yahoo' },
    { key: 'DOGPILE', title: 'Dogpile' },
    { key: 'ECOSIA', title: 'Ecosia' }
  ];

  private defaultExtractionMethods: Array<KeyChoice> = [
    { key: 'MAANS', title: 'Maximum of Absolute Average of Normalized Scores' },
    { key: 'MFLR', title: 'Most Frequent Local Recommendation' }
  ];

  private defaultConstructionMethods: Array<KeyChoice> = [
    { key: 'CACHE_SOURCE', title: 'Cache, Source' },
    { key: 'CACHE', title: 'Cache' },
    { key: 'SOURCE', title: 'Source' }
  ];

  private defaultPOSTaggerModels: Array<KeyChoice> = [
    { key: 'BASIC', title: 'Basic' },
    { key: 'BIDIRECTIONAL', title: 'Bi-Directional' },
    { key: 'LEFT3WORDS', title: 'Left-3-Words' },
    { key: 'EXPERIMENT_1', title: 'Experimental - 1' },
    { key: 'EXPERIMENT_2', title: 'Experimental - 2' }
  ];

  constructor(private http: HttpClient) {
  }

  public getSearchEngines(): Observable<any> {
    return this.http.get('http://localhost:9090/properties/search_engines');
  }

  public getExtractionMethods(): Observable<any> {
    return this.http.get('http://localhost:9090/properties/extraction_methods');
  }

  public getConstructionMethods(): Observable<any> {
    return this.http.get('http://localhost:9090/properties/construction_methods');
  }

  public getPOSTaggerModels(): Observable<any> {
    return this.http.get('http://localhost:9090/properties/pos_taggers');
  }
}
