import { LNTROAlgorithmParams } from './lntro-algorithm-params';
import { LNTROOutput } from './lntro-output';

export class LNTROProcess {

    id: String;
    fetchDate: Date;
    description: String;
    algorithmParams: LNTROAlgorithmParams;
    output: LNTROOutput;
    ontologyOWLFile: String;
}
