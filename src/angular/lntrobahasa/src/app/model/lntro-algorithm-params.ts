export class LNTROAlgorithmParams {

    constructionSearchEngines: Array<String>;
    corpusCount: Number;
    posTagger: String;
    extractionMethods: Array<String>;
    evaluationSearchEngines: Array<String>;
}
