import { TaggedWord } from './tagged-word';
import { BahasaActivePassiveForm } from './bahasa-active-passive-form';

export class LNTROOutput {

    columns: Array<String>;
    data: Array<Array<String>>;
    rawCorpus: Map<String, Array<CorpusItem>>;
    corpus: Map<String, Array<String>>;
    annotatedCorpus: Map<String, Array<Array<TaggedWord>>>;
    termFrequencyMaps: Map<String, Map<String, Number>>;
    extractionScores: Map<String, Map<String, Number>>;
    topExtractionTerms: Map<String, Array<BahasaActivePassiveForm>>;
    evaluationDataSample: Array<Array<String>>;
    evaluationScores: Map<String, Map<String, EvaluationOutput>>;
    lntroTermResult: BahasaActivePassiveForm;

    _benchmark: Map<String, String>;
}

class CorpusItem {

    searchEngine: String;
    content: String;
}

class EvaluationOutput {

    extractedTerms: Array<String>;
    evaluatedTermsMap: Map<String, Map<String, Number>>;
}
