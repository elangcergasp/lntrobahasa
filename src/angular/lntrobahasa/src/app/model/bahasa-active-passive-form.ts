export class BahasaActivePassiveForm {

    word: String;
    stem: String;
    activeForm: String;
    passiveForm: String;
}
