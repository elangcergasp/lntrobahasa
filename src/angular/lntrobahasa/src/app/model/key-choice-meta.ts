export class KeyChoiceMeta {
    key: String;
    title: String;
    meta: Map<String, Object>;
}
