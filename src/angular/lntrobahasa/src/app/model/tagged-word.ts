import { POSTag } from './postag';

export class TaggedWord {

    posTag: POSTag;
    word: String;
}
