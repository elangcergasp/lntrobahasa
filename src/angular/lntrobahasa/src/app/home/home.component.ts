import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LNTROProcess } from '../model/lntro-process';
import { LNTROProcessService } from '../lntro-process.service';
import * as fileSaver from 'file-saver';
import { DownloaderService } from '../downloader.service';
import { faDownload, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit, OnInit, OnDestroy {
  lntroProcesses: Array < LNTROProcess > = [];

  ontologyBaseUrl: String = 'http://localhost:9090/download/ontology/';
  iconFaDetail = faSearch;
  iconFaDownload = faDownload;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private lntroProcessService: LNTROProcessService,
    private downloaderService: DownloaderService) {}

  ngOnInit() {
    this.dtOptions = {
      responsive: true,
      order: [ [ 0, 'desc' ] ],
      columns: [
        { width: "2em" },
        { width: "8em" },
        null,
        { class: 'none' },
        { width: "2em" }
      ]
    };
    this.getLNTROProcesses();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  getLNTROProcesses(): void {
    this.lntroProcessService.getProcesses()
      .subscribe((data: Array<LNTROProcess>) => {
        this.lntroProcesses = data;
        this.rerender();
      });
  }

  downloadFile(index: any) {
    const ontologyOWLFile = this.lntroProcesses[index].ontologyOWLFile;
    // console.log(this.ontologyBaseUrl.toString() + ontologyOWLFile);
    this.downloaderService
      .downloadFile(this.ontologyBaseUrl.toString() + ontologyOWLFile)
      .subscribe((response) => {
        // console.log(response);
        fileSaver.saveAs(response, ontologyOWLFile, 'application/xml');
      });
  }

}
