import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LntroComponent } from './lntro.component';

describe('LntroComponent', () => {
  let component: LntroComponent;
  let fixture: ComponentFixture<LntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
