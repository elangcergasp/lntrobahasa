import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LNTROProcessComponent } from './process.component';

describe('LNTROProcessComponent', () => {
  let component: LNTROProcessComponent;
  let fixture: ComponentFixture<LNTROProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LNTROProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LNTROProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
