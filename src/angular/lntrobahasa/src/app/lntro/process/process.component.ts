import { Component, OnDestroy, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as fileSaver from 'file-saver';
import { LNTROProcess } from '../../model/lntro-process';
import { LNTROProcessService } from '../../lntro-process.service';
import { ServerPropertiesService } from '../../server-properties.service';
import { KeyChoice } from '../../model/key-choice';
import { DownloaderService } from '../../downloader.service';
import { faDownload, faSpinner } from '@fortawesome/free-solid-svg-icons';
import * as posTags from '../../../../../../../data/tagged/posTags.json';

@Component({
  selector: 'app-lntro-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss']
})
export class LNTROProcessComponent implements OnInit, OnDestroy {
  serviceTimeout = 15000;

  lntroProcess: LNTROProcess = null;
  allSearchEngines: Array < KeyChoice > ;
  allPOSTaggerModels: Array < KeyChoice > ;
  allExtractionMethods: Array < KeyChoice > ;
  rawCorpusIterable: Array < MapEntry > = [];
  corpusIterable: Array < MapEntry > = [];
  annotatedCorpusIterable: Array < MapEntry > = [];
  termFrequencyMapsIterable: Array < MapEntry > = [];
  extractionScoresIterable: Array < MapEntry > = [];
  topExtractionTermsIterable: Array < MapEntry > = [];
  evaluationDataSampleIterable: Array < Array<String> > = [];
  evaluationScoresIterable: Array < MapEntry > = [];
  ontologyOWLFile: String = '';
  topExtractionTermsUniqueIterable: Array < MapEntry > = [];

  ontologyBaseUrl: String = 'http://localhost:9090/download/ontology/';

  columnNames: Array < String > = ['Entity 1', 'Entity 2'];
  iconFaDownload = faDownload;
  iconFaSpinner = faSpinner;
  posTags: any = posTags.default;
  posTagsIterable: Array < MapEntry > = this.toArrayMapEntries(posTags.default);

  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtInstanceMap: any = {};
  dtObjects: any = {};

  modalData: any = {
    rawCorpus: {
      data: null,
      rows: null
    },
    corpus: {
      data: null,
      rows: null
    },
    annotatedCorpus: {
      data: null,
      rows: null
    },
    termFrequencyMaps: {
      data: null,
      rows: null
    }
  };

  constructor(private route: ActivatedRoute,
    private modalService: NgbModal,
    private lntroProcessService: LNTROProcessService,
    private serverPropertiesService: ServerPropertiesService,
    private downloaderService: DownloaderService) {

    const getSearchEnginesLambda = () => {
      this.serverPropertiesService.getSearchEngines()
        .subscribe((data: Array < KeyChoice > ) => this.allSearchEngines = data,
          (error) => setTimeout(() => getSearchEnginesLambda(), this.serviceTimeout));
    };
    getSearchEnginesLambda();

    const getPOSTaggerModelsLambda = () => {
      this.serverPropertiesService.getPOSTaggerModels()
        .subscribe((data: Array < KeyChoice > ) => this.allPOSTaggerModels = data,
          (error) => setTimeout(() => getPOSTaggerModelsLambda(), this.serviceTimeout));
    };
    getPOSTaggerModelsLambda();

    const getExtractionMethodsLambda = () => {
      this.serverPropertiesService.getExtractionMethods()
        .subscribe((data: Array < KeyChoice > ) => this.allExtractionMethods = data,
          (error) => setTimeout(() => getExtractionMethodsLambda(), this.serviceTimeout));
    };
    getExtractionMethodsLambda();
  }

  ngOnInit() {
    //console.log(this.posTags);
    this.dtObjects = {
      corpus: {
        dtOptions: {
          columns: [
            null,
            null,
            null,
            null
          ]
        }
      },
      annotatedCorpus: {
        dtOptions: {
          columns: [
            null,
            null,
            null,
            null
          ]
        }
      },
      termFrequencyMaps: {
        dtOptions: {
          columns: [
            null,
            null,
            null,
            null
          ]
        }
      }
    };
    this.getLNTROProcess();
  }

  ngOnDestroy(): void {
  }

  getLNTROProcess(): void {
    // console.log("Fetching LNTRO Process Object...");
    const id = this.route.snapshot.paramMap.get('id');
    this.lntroProcessService.getProcess(id)
      .subscribe((data: LNTROProcess) => {
        // console.log(data);
        if (data == null || data.id == null) {
          setTimeout(() => this.getLNTROProcess(), this.serviceTimeout);
          return;
        }
        // console.log("LNTRO Process Fetched");

        this.lntroProcess = data;

        if (this.lntroProcess.output.columns) {
          this.columnNames = this.lntroProcess.output.columns;
        }

        this.rawCorpusIterable = this.toArrayMapEntries(this.lntroProcess.output.rawCorpus);
        this.corpusIterable = this.toArrayMapEntries(this.lntroProcess.output.corpus);
        this.annotatedCorpusIterable = this.toArrayMapEntries(this.lntroProcess.output.annotatedCorpus);
        this.termFrequencyMapsIterable = this.toArrayMapEntries(this.lntroProcess.output.termFrequencyMaps)
          .map((entry: MapEntry) => {
            const result: MapEntry = {
              key: entry.key,
              value: this.toArrayMapEntries(entry.value as Map < any, any > )
                .sort((a, b) => (b.value as number) - (a.value as number))
            };
            return result;
          });
        this.extractionScoresIterable = this.toArrayMapEntries(this.lntroProcess.output.extractionScores)
          .map((entry: MapEntry) => {
            const result: MapEntry = {
              key: entry.key,
              value: this.toArrayMapEntries(entry.value as Map < any, any > )
                .sort((a, b) => (b.value as number) - (a.value as number))
            };
            return result;
          });
        this.topExtractionTermsIterable = this.toArrayMapEntries(this.lntroProcess.output.topExtractionTerms)
          .map((entry: MapEntry) => {
            const result: MapEntry = {
              key: entry.key,
              value: this.toArrayMapEntries(entry.value as Map < any, any > )
            };
            return result;
          });
        if (this.lntroProcess.output.evaluationDataSample) {
          this.evaluationDataSampleIterable = this.lntroProcess.output.evaluationDataSample;
        }
        this.evaluationScoresIterable = this.toArrayMapEntries(this.lntroProcess.output.evaluationScores)
          .map((entry: MapEntry) => {
            const result: MapEntry = {
              key: entry.key,
              value: this.toArrayMapEntries(entry.value['evaluatedTermsMap'])
                .map((e: MapEntry) => {
                  const r: MapEntry = {
                    key: e.key,
                    value: e.value
                  };
                  return r;
                })
                // tslint:disable-next-line:max-line-length
                .sort((a, b) => (this.sumReduceMap(b.value as  Map<any, Number>) as number) - (this.sumReduceMap(a.value as  Map<any, Number>) as number))
            };
            return result;
          });

        this.ontologyOWLFile = this.lntroProcess.ontologyOWLFile;
        const topExtractionTermsTmpUnique = new Map;
        for (const x in this.lntroProcess.output.topExtractionTerms) {
          if (this.lntroProcess.output.topExtractionTerms.hasOwnProperty(x)) {
            for (const y in this.lntroProcess.output.topExtractionTerms[x]) {
              if (this.lntroProcess.output.topExtractionTerms[x].hasOwnProperty(y)) {
                const term = this.lntroProcess.output.topExtractionTerms[x][y].word;
                topExtractionTermsTmpUnique[term] = this.lntroProcess.output.topExtractionTerms[x][y];
              }
            }
          }
        }
        this.topExtractionTermsUniqueIterable = this.toArrayMapEntries(topExtractionTermsTmpUnique);

        setTimeout(() => {
          this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              // console.log(dtInstance);
              const classes = [
                'table-data',
                'table-corpus',
                'table-annotated',
                'table-tfm',
                'table-evaluation'
              ];
              classes.map((c) => {
                if (dtInstance.table().node().classList.contains(c)) {
                  this.dtInstanceMap[c] = dtInstance;
                }
              });
            });
          });
        }, 100);

        // setTimeout(() => console.clear(), 600000);

        // console.log(this.lntroProcess);
      }, (error) => {
        // console.log(error);
        setTimeout(() => this.getLNTROProcess(), this.serviceTimeout);
      });
  }

  downloadOntologyDefault() {
    const filename = this.ontologyOWLFile;
    const url = this.ontologyBaseUrl.toString() + this.ontologyOWLFile;
    // console.log(url);
    this.downloaderService
      .downloadFile(url)
      .subscribe((response) => {
        // console.log(response);
        fileSaver.saveAs(response, filename, 'application/xml');
      });
  }

  downloadOntology(term: String) {
    const filename = this.ontologyOWLFile.replace('.owl', '--' + term + '.owl');
    const url = this.ontologyBaseUrl.toString() + this.lntroProcess.id + '/' + term;
    // console.log(url);
    this.downloaderService
      .downloadFile(url)
      .subscribe((response) => {
        // console.log(response);
        fileSaver.saveAs(response, filename, 'application/xml');
      });
  }

  isDataTableDetailsShown(tableID, rowIndex) {
    if (this.dtInstanceMap[tableID]) {
      // console.log(Math.random());
      // console.log(this.dtInstanceMap[tableID].row(rowIndex));
      // console.log(rowIndex + " " + this.dtInstanceMap[tableID].row(rowIndex).child.isShown());
      return this.dtInstanceMap[tableID].row(rowIndex).child.isShown();
    } else {
      return false;
    }
  }

  /*
  posTagInfo(posTag: String) {
    return
  }
  */

  panelChangeHandler($event: NgbPanelChangeEvent) {
    // console.log($event);
    if ($event.nextState === false) {
      $event.preventDefault();

      // Hacks
      const el = document.getElementById($event.panelId);
      if (el.classList.contains('show')) {
        el.classList.remove('show');
        // console.log($event.panelId + " is now hidden");
      } else {
        el.classList.add('show');
        // console.log($event.panelId + " is now shown");
      }
    }
  }

  openRawCorpusModal(modal, data, rows) {
    this.modalData.rawCorpus.data = data;
    this.modalData.rawCorpus.rows = rows.sort((a, b) => {
      if (a.searchEngine == "GOOGLE") return -1;
      if (b.searchEngine == "GOOGLE") return 1;
      return a.searchEngine.localeCompare(b.searchEngine);
    });

    this.modalService.open(modal, { size: 'lg' }).result.then((result) => {

    }, (reason) => {

    });
  }

  openCorpusModal(modal, data, rows) {
    this.modalData.corpus.data = data;
    this.modalData.corpus.rows = rows;

    this.modalService.open(modal, { size: 'lg' }).result.then((result) => {

    }, (reason) => {

    });
  }

  openAnnotatedCorpusModal(modal, data, rows) {
    this.modalData.annotatedCorpus.data = data;
    this.modalData.annotatedCorpus.rows = rows;

    this.modalService.open(modal, { size: 'lg' }).result.then((result) => {

    }, (reason) => {

    });
  }

  openTermFrequencyMapsModal(modal, data, rows) {
    this.modalData.termFrequencyMaps.data = data;
    this.modalData.termFrequencyMaps.rows = rows;

    this.modalService.open(modal, { size: 'lg' }).result.then((result) => {

    }, (reason) => {

    });
  }

  makeHighlightBold(content, dataArray) {
    return dataArray.map(s => new RegExp(s, 'gi')).reduce((c, r) => c.replace(r, (s) => "<b>"+s+"</b>"), content);
  }

  get constructionSearchEngines(): Array < String > {
    return this.allSearchEngines
      .filter(el => this.lntroProcess.algorithmParams.constructionSearchEngines.indexOf(el.key) !== -1)
      .map(el => el.title);
  }

  get posTaggerModel(): Array < String > {
    return this.allPOSTaggerModels
      .filter(el => el.key === this.lntroProcess.algorithmParams.posTagger)
      .map(el => el.title);
  }

  get extractionMethods(): Array < String > {
    return this.allExtractionMethods
      .filter(el => this.lntroProcess.algorithmParams.extractionMethods.indexOf(el.key) !== -1)
      .map(el => el.title);
  }

  get evaluationSearchEngines(): Array < String > {
    return this.allSearchEngines
      .filter(el => this.lntroProcess.algorithmParams.evaluationSearchEngines.indexOf(el.key) !== -1)
      .map(el => el.title);
  }

  get evaluationSearchEnginesObjects(): Array < KeyChoice > {
    return this.allSearchEngines
      .filter(el => this.lntroProcess.algorithmParams.evaluationSearchEngines.indexOf(el.key) !== -1);
  }

  toArrayMapEntries(map: Map < any, any > ): Array < MapEntry > {
    const mapIterable: Array < MapEntry > = [];
    for (const x in map) {
      if (map.hasOwnProperty(x)) {
        mapIterable.push({
          key: x,
          value: map[x]
        });
      }
    }
    return mapIterable;
  }

  flatMapArray(array: Array < Array < any > >): Array < any > {
    const flatArray: Array < any > = [];
    for (const x in array) {
      if (array.hasOwnProperty(x)) {
        for (const y in array[x]) {
          if (array[x].hasOwnProperty(y)) {
            flatArray.push(array[x][y]);
          }
        }
      }
    }
    return flatArray;
  }

  flatMap(map: Map < String, Map < String, any > >): Map < String, any > {
    const flatMap: Map < String, any > = new Map;
    for (const x in map) {
      if (map.hasOwnProperty(x)) {
        for (const y in map[x]) {
          if (map[x].hasOwnProperty(y)) {
            flatMap[y] = map[x][y];
          }
        }
      }
    }
    return flatMap;
  }

  sumReduceMap(map: Map < any, Number >): Number {
    return Object.values(map).reduce((a, b) => a + b, 0);
  }
}

class MapEntry {
  key: String;
  value: Object;
}
