import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LNTROProcess } from '../../model/lntro-process';
import { LNTROProcessService } from '../../lntro-process.service';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.scss']
})
export class QueueComponent implements AfterViewInit, OnInit, OnDestroy {
  lntroQueue: Array < LNTROProcess > = [];
  iconFaDetail = faSearch;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private lntroProcessService: LNTROProcessService) { }

  ngOnInit() {
    this.dtOptions = {
      responsive: true,
      order: [ [ 0, 'asc' ] ],
      columns: [
        { width: "2em" },
        { width: "8em" },
        { width: "8em" },
        null,
        { class: 'none' },
        { width: "2em" }
      ]
    };
    this.getLNTROQueue();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  getLNTROQueue(): void {
    this.lntroProcessService.getQueue()
      .subscribe((data: Array<LNTROProcess>) => {
        this.lntroQueue = data;
        this.rerender();
      });
  }

}
