import { Component, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

import { ServerPropertiesService } from '../server-properties.service';
import { KeyChoice } from '../model/key-choice';
import { KeyChoiceMeta } from '../model/key-choice-meta';
import { HttpClient } from '@angular/common/http';
import { JustID } from '../model/just-id';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lntro',
  templateUrl: './lntro.component.html',
  styleUrls: ['./lntro.component.scss']
})
export class LNTROComponent {
  lntroForm: FormGroup;
  formFiles: Map<String, File> = new Map;

  constructionSearchEngines: Array<KeyChoiceMeta>;
  constructionMethods: Array<KeyChoice>;
  posTaggerModels: Array<KeyChoice>;
  extractionMethods: Array<KeyChoice>;
  evaluationSearchEngines: Array<KeyChoiceMeta>;

  isSubmittingForm = false;

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private router: Router,
              private serverPropertiesService: ServerPropertiesService) {

    this.createForm();

    this.serverPropertiesService.getSearchEngines()
      .subscribe(
        (data: Array<KeyChoiceMeta>) => {
          this.constructionSearchEngines = data;
          const formArrayConstruction = this.lntroForm.get('construction.searchEngines') as FormArray;
          this.constructionSearchEngines.map(el => formArrayConstruction.push(this.fb.control(true)));

          this.evaluationSearchEngines = data.filter(se => se.meta['countable']);
          const formArrayEvaluation = this.lntroForm.get('evaluation.searchEngines') as FormArray;
          this.evaluationSearchEngines.map(el => formArrayEvaluation.push(this.fb.control(true)));
        }
      );

    this.serverPropertiesService.getConstructionMethods()
      .subscribe(
        (data: Array<KeyChoice>) => {
          this.constructionMethods = data;
        }
      );

    this.serverPropertiesService.getPOSTaggerModels()
    .subscribe(
      (data: Array<KeyChoice>) => {
        this.posTaggerModels = data;
      }
    );

    this.serverPropertiesService.getExtractionMethods()
      .subscribe(
        (data: Array<KeyChoice>) => {
          this.extractionMethods = data;
          const formArray = this.lntroForm.get('extraction.methods') as FormArray;
          this.extractionMethods.map(el => formArray.push(this.fb.control(true)));
        }
      );
  }

  public createForm() {
    this.lntroForm = this.fb.group({
      input: this.fb.group({
        data: ['', Validators.required],
        separator: ['TAB', Validators.required],
        title: ['', Validators.required],
        description: ['', Validators.required]
      }),
      preprocessing: this.fb.group({
        stopwords: this.fb.group({
          extraction: ''
        })
      }),
      construction: this.fb.group({
        corpusCount: [100, Validators.required],
        corpusMethod: ['CACHE_SOURCE', Validators.required],
        searchEngines: this.fb.array([])
      }),
      annotation: this.fb.group({
        posTagger: 'BASIC'
      }),
      extraction: this.fb.group({
        methods: this.fb.array([])
      }),
      evaluation: this.fb.group({
        searchEngines: this.fb.array([])
      }),
    });
  }

  handleFileInput(key: String, files: FileList): void {
    const file = files.item(0);
    this.formFiles.set(key, file);
    this.lntroForm.get(key.toString()).setValue(file.name);
  }

  onSubmit() {
    if (!this.isSubmittingForm) {
      console.log('Submitting Form...');
      this.isSubmittingForm = true;

      const formData: FormData = new FormData();

      const title = this.lntroForm.get('input.title').value;
      formData.append('title', title);

      const description = this.lntroForm.get('input.description').value;
      formData.append('description', description);

      const data = this.formFiles.get('input.data');
      formData.append('data', data, data.name);

      const separator = this.lntroForm.get('input.separator').value;
      formData.append('separator', separator);

      // const corpusCount = this.lntroForm.get('construction.corpusCount').value;
      formData.append('corpusCount', '' + this.corpusCountAll);

      const corpusMethod = this.lntroForm.get('construction.corpusMethod').value;
      formData.append('corpusMethod', corpusMethod);

      const constructionSearchEnginesValues = this.lntroForm.get('construction.searchEngines').value;
      const constructionSearchEngineIDs: Array<String> = [];
      this.constructionSearchEngines.forEach((value, index) => {
        if (constructionSearchEnginesValues[index]) {
          constructionSearchEngineIDs.push(value.key);
          // formData.append('searchEngineIDs', value.key.toString());
        }
      });
      // @ts-ignore
      formData.append('constructionSearchEngineIDs', constructionSearchEngineIDs);

      const posTaggerID = this.lntroForm.get('annotation.posTagger').value;
      formData.append('posTaggerID', posTaggerID);

      const extractionMethodsValues = this.lntroForm.get('extraction.methods').value;
      const extractionMethods: Array<String> = [];
      this.extractionMethods.forEach((value, index) => {
        if (extractionMethodsValues[index]) {
          extractionMethods.push(value.key);
          // formData.append('extractionMethods', value.key.toString());
        }
      });
      // @ts-ignore
      formData.append('extractionMethods', extractionMethods);

      const evaluationSearchEnginesValues = this.lntroForm.get('evaluation.searchEngines').value;
      const evaluationSearchEngineIDs: Array<String> = [];
      this.evaluationSearchEngines.forEach((value, index) => {
        if (evaluationSearchEnginesValues[index]) {
          evaluationSearchEngineIDs.push(value.key);
          // formData.append('searchEngineIDs', value.key.toString());
        }
      });
      // @ts-ignore
      formData.append('evaluationSearchEngineIDs', evaluationSearchEngineIDs);

      // formData.forEach((val, key, parent) => console.log(key + ': ' + val));

      this.http.post('http://localhost:9090/lntro/submit', formData)
      .subscribe((response: JustID) => {
        console.log('Form Submitted! ID : ' + response.id);
        this.router.navigate(['lntro', response.id]);
      }, error => {
        console.log(error);
        this.isSubmittingForm = false;
      });
    }
  }

  get constructionSearchEnginesControls(): FormArray {
    return this.lntroForm.get('construction.searchEngines') as FormArray;
  }

  get extractionMethodsControls(): FormArray {
    return this.lntroForm.get('extraction.methods') as FormArray;
  }

  get evaluationSearchEnginesControls(): FormArray {
    return this.lntroForm.get('evaluation.searchEngines') as FormArray;
  }

  get corpusCountControl(): FormControl {
    return this.lntroForm.get('construction.corpusCount') as FormControl;
  }

  get constructionSearchEnginesControlsChecked() {
    return this.constructionSearchEnginesControls.controls
      .filter(s => s.value)
      .map(s => 1)
      .reduce((a, b) => a + b, 0);
  }

  get corpusCountAll(): Number {
    return Math.ceil(this.constructionSearchEnginesControls.controls.length > 0
    ? this.corpusCountControl.value * this.constructionSearchEnginesControlsChecked
    : this.corpusCountControl.value);
  }
}
