import { TestBed, inject } from '@angular/core/testing';

import { ServerPropertiesService } from './server-properties.service';

describe('ServerPropertiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerPropertiesService]
    });
  });

  it('should be created', inject([ServerPropertiesService], (service: ServerPropertiesService) => {
    expect(service).toBeTruthy();
  }));
});
