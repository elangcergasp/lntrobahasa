import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LNTROComponent } from './lntro/lntro.component';
import { LNTROProcessComponent } from './lntro/process/process.component';
import { QueueComponent } from './lntro/queue/queue.component';
import { SeederComponent } from './seeder/seeder.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'lntro', component: LNTROComponent },
  { path: 'lntro/queue', component: QueueComponent },
  { path: 'lntro/:id', component: LNTROProcessComponent },
  { path: 'seeder', component: SeederComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
