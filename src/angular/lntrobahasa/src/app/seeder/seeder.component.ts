import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ServerPropertiesService } from '../server-properties.service';
import { HttpClient } from '@angular/common/http';
import { KeyChoiceMeta } from '../model/key-choice-meta';

@Component({
  selector: 'app-seeder',
  templateUrl: './seeder.component.html',
  styleUrls: ['./seeder.component.scss']
})
export class SeederComponent implements OnInit {
  searchEngines: Object = {};
  errorBuffers: Object = {};

  form: FormGroup;
  isBufferEditable: false;
  uploadStatus: Object = {};
  uploadMessage: Object = {};
  currentProcessingDocuments: Object = {};

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private sanitizer: DomSanitizer,
              private serverPropertiesService: ServerPropertiesService) {

    this.createForm();

    this.serverPropertiesService.getSearchEngines()
      .subscribe(
        (data: Array<KeyChoiceMeta>) => {
          this.searchEngines = {};
          data.map(d => {
            const k: string = d.key as string;
            this.searchEngines[k] = d;

            const documentInput = this.form.get('document') as FormGroup;
            documentInput.addControl(k, this.fb.control(''));

            this.uploadStatus[k] = false;
            this.uploadMessage[k] = '';

            this.currentProcessingDocuments[k] = new CurrentProcessingDocument;
          });
        }
      );
  }

  createForm() {
  	this.form = this.fb.group({
      errorBuffer: this.fb.control(''),
      document: this.fb.group({})
    });
  }

  ngOnInit() {
  }

  getErrorBuffers() {
    console.log('Fetching Error Buffers...');

    this.http.get('http://localhost:9090/seeder/search_error_buffer_urls')
      .subscribe((response) => {
        console.log('Error Buffers Fetched!');
        this.errorBuffers = response;
        this.objectMap(this.errorBuffers, (k, v) => this.pushProcessDocument(k));
      }, error => {
        console.log(error);
      });
  }

  pushProcessDocument(searchEngine) {
    if (this.errorBuffers.hasOwnProperty(searchEngine)) {
      const randomData = this.randomProperty(this.errorBuffers[searchEngine]);
      console.log(randomData);
      const searchEngineTarget = this.errorBuffers[searchEngine][randomData];
      
      this.currentProcessingDocuments[searchEngine].data = randomData;
      this.currentProcessingDocuments[searchEngine].target = searchEngineTarget;
    } else {
      console.log(searchEngine);
      console.log(this.errorBuffers);
    }
  }

  popProcessDocument(searchEngine) {
    const data = this.currentProcessingDocuments[searchEngine].data;

    if (data) {
      this.currentProcessingDocuments[searchEngine].data = null;
      this.currentProcessingDocuments[searchEngine].target = null;
      this.currentProcessingDocuments[searchEngine].counter++;

      console.log(this.errorBuffers[searchEngine][data]);
      delete this.errorBuffers[searchEngine][data];
      console.log(this.errorBuffers[searchEngine][data]);
    }
  }

  popAndPushProcessDocument(searchEngine) {
    this.popProcessDocument(searchEngine);
    this.pushProcessDocument(searchEngine);
    this.form.get("document").get(searchEngine).setValue("");
  }

  documentLoadCompleteItem(searchEngine) {
    const documentString = this.form.get("document").get(searchEngine).value;

    const process = this.currentProcessingDocuments[searchEngine];

    const keyword: string = process.target.keyword.toString();
    const data: Array<String> = process.data;

    const formData: FormData = new FormData();
    formData.append('searchEngine', searchEngine);
    formData.append('keyword', keyword);
    // @ts-ignore
    formData.append('data', data);
    formData.append('document', documentString);

    console.log('Submitting Document Items ' + process.target.url + '...');

    this.uploadStatus[searchEngine] = true;
    this.http.post('http://localhost:9090/seeder/search_result_item', formData)
      .subscribe((response) => {
        console.log('Document Items ' + process.target.url + ' Submitted!');
        process.lastDocument = documentString;
        this.sendUploadMessage(searchEngine, "Upload Item OK!");
        //this.popAndPushProcessDocument(searchEngine);
      }, error => {
        console.log(error);
        this.sendUploadMessage(searchEngine, "Upload Item Error! " + error);
      }, () => {
        this.uploadStatus[searchEngine] = false;
      });
  }

  documentLoadCompleteStat(searchEngine) {
    const documentString = this.form.get("document").get(searchEngine).value;

    const process = this.currentProcessingDocuments[searchEngine];

    const keyword: string = process.target.keyword.toString();
    const data: Array<String> = process.data;

    const formData: FormData = new FormData();
    formData.append('searchEngine', searchEngine);
    formData.append('keyword', keyword);
    // @ts-ignore
    formData.append('data', data);
    formData.append('document', documentString);

    console.log('Submitting Document Stat ' + process.target.url + '...');

    this.uploadStatus[searchEngine] = true;
    this.http.post('http://localhost:9090/seeder/search_result_stat', formData)
      .subscribe((response) => {
        console.log('Document Stat ' + process.target.url + ' Submitted!');
        process.lastDocument = documentString;
        this.sendUploadMessage(searchEngine, "Upload Stat OK!");
        //this.popAndPushProcessDocument(searchEngine);
      }, error => {
        console.log(error);
        this.sendUploadMessage(searchEngine, "Upload Stat Error! " + error);
      }, () => {
        this.uploadStatus[searchEngine] = false;
      });
  }

  sendUploadMessage(searchEngine, message) {
    const timeout = 3000;
    this.uploadMessage[searchEngine] = message;
    setTimeout(() => this.uploadMessage[searchEngine] = '', timeout);
  }

  getProcessingDocumentUrl(searchEngine) {
    try {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.currentProcessingDocuments[searchEngine].target.url.toString());
    } catch (e) {
      return "";
    }
  }

  objectMap(obj, callback) {
    const newObj = {};
    for (const k in obj) {
      if (obj.hasOwnProperty(k)) {
        newObj[k] = callback(k, obj[k]);
      }
    }
    return newObj;
  }

  randomProperty(obj) {
    const keys = Object.keys(obj);
    return keys[ keys.length * Math.random() << 0];
  }

  objectKeys(obj) {
    return Object.keys(obj || {});
  }

  jsonParse(str) {
    return JSON.parse(str);
  }
}

class SearchEngineTarget {
  searchEngine: string;
  keyword: string;
  url: string;
}

class CurrentProcessingDocument {
  data: Array<string>;
  target: SearchEngineTarget;
  counter: number = 0;
  lastDocument: string;
}
