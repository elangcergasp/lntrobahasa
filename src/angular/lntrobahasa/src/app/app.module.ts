import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DataTablesModule } from 'angular-datatables';

import { ServerPropertiesService } from './server-properties.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LNTROComponent } from './lntro/lntro.component';
import { LNTROProcessComponent } from './lntro/process/process.component';
import { PosTagInfoDirective } from './pos-tag-info.directive';
import { SeederComponent } from './seeder/seeder.component';
import { VarDirective } from './var.directive';
import { QueueComponent } from './lntro/queue/queue.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LNTROComponent,
    LNTROProcessComponent,
    PosTagInfoDirective,
    SeederComponent,
    VarDirective,
    QueueComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FontAwesomeModule,
    DataTablesModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [
    ServerPropertiesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
