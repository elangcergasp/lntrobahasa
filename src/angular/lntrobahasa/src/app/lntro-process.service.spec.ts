import { TestBed, inject } from '@angular/core/testing';

import { LNTROProcessService } from './lntro-process.service';

describe('LNTROProcessService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LNTROProcessService]
    });
  });

  it('should be created', inject([LNTROProcessService], (service: LNTROProcessService) => {
    expect(service).toBeTruthy();
  }));
});
