import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LNTROProcessService {

  constructor(private http: HttpClient) { }

  public getProcess(ID: String): Observable<any> {
    return this.http.get('http://localhost:9090/lntro/process/' + ID);
  }

  public getProcesses(): Observable<any> {
    return this.http.get('http://localhost:9090/lntro/processes/');
  }

  public getQueue(): Observable<any> {
    return this.http.get('http://localhost:9090/lntro/queue/');
  }
}
